<?php
  /*File generated: 4.7.2016 - 05:05:01 UTC*/
  return [
    /* INFO */
    /**/
    "mission_enter_help" => "Osallistu kilpailuun",
    /**/
    "mission_help_label1" => "1. Lataa sovellus puhelimeesi",
    /**/
    "mission_help_text1" => "Saatavilla iOS ja Android puhelimille",
    /**/
    "mission_help_label2" => "2. Valitse tehtävä",
    /**/
    "mission_help_text2" => "Valitse tehtävä sovelluksessa mihin haluat osallistua videollasi.",
    /**/
    "mission_help_label3" => "3. Lähetä video",
    /**/
    "mission_help_text3" => "Kun olet kuvannut tai valinnut galleriasta videon lataa se meille ja olet valmis!",
    /**/
    "mission_help_button" => "Okei!",
  ];
?>
