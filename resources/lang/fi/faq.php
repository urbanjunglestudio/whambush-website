<?php
  /*File generated: 4.7.2016 - 05:04:58 UTC*/
  return [
    /* RULES */
    /*Same for all languages!!!!*/
    "SECTION_1_ICON" => "icon-rules",
    /**/
    "SECTION_1_TITLE" => "Säännöt",
    /**/
    "SECTION_1_1" => "Pätevä sisältö",
    /**/
    "SECTION_1_1_1" => "Hyväksytyksi esitykseksi nähdään sisältö, joka ei ole haitallista, ja on asianmukaisesti haasteeseen ladattu.",
    /**/
    "SECTION_1_2" => "Haastevideot",
    /**/
    "SECTION_1_2_1" => "Haasteeseen lähetettyä videota ei voi poistaa. Haasteeseen lähetetyn videon poisto voi antaa vääriä tuloksia sijoituksissa, ja aiheuttaa ongelmia voittajien palkitsemisessa.",
    /**/
    "SECTION_1_3" => "Miten voittajat valitaan",
    /**/
    "SECTION_1_3_1" => "Whambushin sijoitus-algoritmi valitsee päivittäisten haasteiden voittajat",
    /**/
    "SECTION_1_3_2" => "Ikonit valitsevat itse viikoittaisten haasteiden voittajat",
    /**/
    "SECTION_1_3_3" => "Jos et lunasta palkintoasi kuukauden sisällä, menetät sen. Kun Whambush-tiimistä on otettu sinuun yhteyttä, on sinulla 30 päivää aikaa lunastaa palkintosi tai lähettää meille postiosoitteesi. Jos sinuun ei ole otettu yhteyttä, ole ystävällinen ja lähetä viestiä tukeen.",
    /**/
    "SECTION_1_5" => "Väärennetyt tilit",
    /**/
    "SECTION_1_5_1" => "Banaaniesi määrä saattaa vaihdella kun siivoamme pois väärennettyjä tilejä. Jos epäilet jonkun huijaavan, ole ystävällinen ja raportoi tili/video tukeen.",
    /**/
    "SECTION_1_6" => "Vastuuvapaus",
    /**/
    "SECTION_1_6_1" => "Kaikki tapaukset ovat erilaisia, ja tuella on oikeus tehdä poikkeuksia säännöistä",

    /* PRIZES */
    /*Same for all languages!!!!*/
    "SECTION_2_ICON" => "icon-star",
    /**/
    "SECTION_2_TITLE" => "Palkinnot",
    /**/
    "SECTION_2_1" => "Miten palkinnot lunastetaan",
    /**/
    "SECTION_2_1_1" => "Palkinnot voivat olla fyysisiä tai virtuaalisia. Molemmissa tapauksissa sinuun ollaan yhteydessä sähköpostitse, jossa lähetämme virtuaalisen palkinnon, tai kysymme postiosoitettasi.",
    /**/
    "SECTION_2_2" => "En saanut palkintoani",
    /**/
    "SECTION_2_2_1" => "Jos olet tarkistanut sähköpostisi ja roskapostisi kahden viikon kuluttua kilpailun voittamisesta, ole ystävällinen ja ota yhteyttä tukeen, ja sisällytä viestiin käyttäjätunnuksesi ja voittamasi haasteen. Palkintoa ei voi vaihtaa, eikä ottaa rahana.",

    /* USER ACCOUNT */
    /*Same for all languages!!!!*/
    "SECTION_3_ICON" => "icon-profile",
    /**/
    "SECTION_3_TITLE" => "Käyttäjätili",
    /**/
    "SECTION_3_1" => "Aktivoi käyttäjätilisi",
    /**/
    "SECTION_3_1_1" => "Rekisteröitymisen jälkeen klikkaa aktivointi-linkkiä joka on lähetetty sähköpostitse. Jos et tee sitä 2 päivän sisällä, käyttäjätilisi deaktivoidaan.",
    /**/
    "SECTION_3_2" => "Unohtuiko salasanasi",
    /**/
    "SECTION_3_2_1" => "Kun sisäänkirjautumisesi epäonnistuu, valitse &quot;nollaa salasanasi&quot;, ja kirjoita sähköpostiosoitteesi",
    /**/
    "SECTION_3_3" => "Kuinka nollata salasana",
    /**/
    "SECTION_3_3_1" => "Mene asetuksiin ja etsi &quot;nollaa salasana&quot;-kenttä",

    /* CONTENT */
    /*Same for all languages!!!!*/
    "SECTION_4_ICON" => "icon-play",
    /**/
    "SECTION_4_TITLE" => "Sisältö",
    /**/
    "SECTION_4_1" => "Kuinka poistaa oma videosi",
    /**/
    "SECTION_4_1_1" => "Mene videoon jonka haluat poistaa, paina kolmea pistettä videon alla, ja klikkaa roskakori-ikonia",
    /**/
    "SECTION_4_2" => "Kuinka poistaa haastevideo",
    /**/
    "SECTION_4_2_1" => "Kilpailusääntöjen mukaan et voi poistaa haastevideota. Jos siihen kuitenkin löytyy hyvä syy, ole hyvä ja ota yhteyttä tukeen, niin he käyvät läpi vaatimuksen tapauksittain.",
    /**/
    "SECTION_4_3" => "Kuinka poistaa kommentteja",
    /**/
    "SECTION_4_3_1" => "iOS: Klikkaa ja pidä sormeasi kommenttisi päällä kunnes näet toimintopainikkeen",
    /**/
    "SECTION_4_4" => "Kuinka raportoida video",
    /**/
    "SECTION_4_4_1" => "Klikkaa kolmea pistettä videon alla ja valitse lippu-ikoni",

    /* MISSIONS */
    /*Same for all languages!!!!*/
    "SECTION_5_ICON" => "icon-missions",
    /**/
    "SECTION_5_TITLE" => "Haasteet",
    /**/
    "SECTION_5_1" => "Päivittäiset haasteet",
    /**/
    "SECTION_5_1_1" => "Whambush-gorilla jakaa päivittäiset haasteet maanantaista lauantaihin. Haasteet kestävät useimmiten 7 päivää, ja voittajat saavat sähköpostitse ilmoituksen. Voittajan video on nähtävissä &quot;vanhat tehtävät&quot;-syötteessä. Pyhäpäivät ja loma-ajat voivat aiheuttaa epäsäännöllisyyksiä haasteiden julkaisuihin ja niiden kestoihin.",
    /**/
    "SECTION_5_2" => "Viikoittaiset haasteet",
    /**/
    "SECTION_5_2_1" => "Näissä haasteissa on isommat palkinnot ja video, joka selittää tehtävän.   Voittajat julkaistaan videolla, ja heille ilmoitetaan myös sähköpostitse.",
    /**/
    "SECTION_5_3" => "Ulkomaalaisiin haasteisiin osallistuminen",
    /**/
    "SECTION_5_3_1" => "Jos osallistut toisen maan haasteeseen, mutta sinulla ei ole voimassa olevaa osoitetta kyseiseen maahan, palkinto lähetetään vain jos katat toimituskulut. Jos et, palkinto annetaan toiseksi tulleelle.",
    /**/
    "SECTION_5_4" => "Kuinka lähettää uudelleen video haasteeseen",
    /**/
    "SECTION_5_4_1" => "Jos olet ladannut videon, mutta et ole merkinnyt sitä haasteeseen, voit edelleen ladata sen uudelleen, osallistua haasteeseen, ja poistaa vanhan videon",

    /* APP */
    /*Same for all languages!!!!*/
    "SECTION_6_ICON" => "icon-banana",
    /**/
    "SECTION_6_TITLE" => "Sovellus",
    /**/
    "SECTION_6_1" => "Tuetut alustat",
    /**/
    "SECTION_6_1_1" => "iOS 7 ja uudemmat. Android 4.2 ja uudemmat.",
    /**/
    "SECTION_6_2" => "Raportoi ongelma",
    /**/
    "SECTION_6_2_1" => "Avaa sovellus, ja mene asetuksiin klikkaamalla profiili-ikonia. Etsi tuki-linkki asetuksista, jonka jälkeen sen pitäisi avata sähköpostisi vaadittujen laitetietojen kanssa.",
    /**/
    "SECTION_6_3" => "Etkö löytänyt etsimääsi?",
    /**/
    "SECTION_6_3_1" => "Ota yhteyttä tukeen",
  ];
?>
