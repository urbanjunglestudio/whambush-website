<?php
  /*File generated: 4.7.2016 - 05:05:02 UTC*/
  return [
    /* common */
    /**/
    "LOGIN" => "Kirjaudu sisään",
    /**/
    "LOGOUT" => "Kirjaudu ulos",
    /**/
    "WELCOME" => "Tervetuloa",
    /**/
    "USERNAME" => "Käyttäjätunnus",
    /**/
    "PASSWORD" => "Salasana",
    /**/
    "FORGOTPASS" => "Unohtuiko salasana?",
    /**/
    "REGISTER" => "Rekisteröidy uutena asiakkaana",
    /**/
    "LOGIN_FAILED" => "Käyttäjätunnus/salasana väärin tai tili ei ole aktivoitu, yritä uudelleen",
    /**/
    "PLEASE_LOGIN" => "Ole hyvä ja kirjaudu jatkaaksesi",
    /**/
    "CANCEL" => "Peruuta",
    /**/
    "FINISH" => "Valmis",
    /**/
    "NEXT" => "Seuraava",
    /**/
    "PREV" => "Edellinen",
    /**/
    "LOADING" => "Ladataan...",
    /**/
    "BACK" => "Takaisin",
    /**/
    "MISSION" => "Tehtävä",
    /**/
    "CLOSE" => "Sulje",
    /**/
    "SAVE" => "Tallenna",

    /* main page */
    /**/
    "NEWMISSION" => "Luo uusi tehtävä",
    /**/
    "CURRENTMISSIONS" => "Näytä aktiiviset tehtävät",
    /**/
    "OLDMISSIONS" => "Hallitse päättyneitä tehtäviä",
    /**/
    "HANDLEMISSIONS" => "Hallitse tehtäviä",
    /**/
    "CREATE" => "Luo uusi",
    /**/
    "CHECK" => "Näytä",
    /**/
    "HANDLE" => "Hallitse",
    /**/
    "HELP" => "Ongelmatapauksissa ota yhteyttä osoitteeseen tuki@whambush.com",
    /**/
    "HELPPDF" => "Lataa asiakaportaalin käyttöohje pdf-tiedostona",

    /* register */
    /**/
    "REGISTERTITLE" => "Rekisteröidy",
    /**/
    "NAME" => "Nimi",
    /**/
    "KEYCODE" => "Asiakaskoodi",
    /**/
    "KEYCODEINFO" => "Ole hyvä ja anna koodi, jonka sait Whambushin myyntiedustajalta.",
    /**/
    "EMAIL" => "Sähköposti",
    /**/
    "PASSWORD2" => "Salasana uudelleen",
    /**/
    "PROFILEPICTURE" => "Profiilikuva",
    /**/
    "DESCRIPTION" => "Profiilin kuvaus",
    /**/
    "COUNTRY" => "Maa",
    /**/
    "EDITPROFILEPIC" => "Skaalaa ja rajaa kuva",
    /**/
    "CHOOSEPIC" => "Valitse kuva",
    /**/
    "USERDETAILS" => "Käyttäjätiedot",
    /**/
    "USERDETAILSINFO" => "Ole hyvä ja täytä kaikki kentät",
    /**/
    "REGISTERDONE" => "Rekisteröinti valmis",
    /**/
    "ACTIVATIONMESSAGE" => "Ole hyvä ja aktivoi tilisi sähköpostista joka on lähetetty %%EMAIL%% osoitteeseen. Aktivoinnin jälkeen voit kirjautua palveuun.",
    /**/
    "MOREINFO" => "Lisäinfo",
    /**/
    "PREVIEW" => "Esikatselu",
    /**/
    "ICONNAME" => "Tehtävissä käytettävä nimi",
    /**/
    "CHOOSECOUNTRY" => "Valitse maa",

    /* errors */
    /**/
    "WRONGCODE" => "Koodi ei käy",
    /**/
    "ERRORUSERNAME1" => "Käyttäjätunnus tyhjä",
    /**/
    "ERRORUSERNAME2" => "Käyttäjätunnus on varattu",
    /**/
    "ERRORPASS1" => "Salasana liian lyhyt (vähintään 4 merkkiä)",
    /**/
    "ERRORPASS2" => "Salasanat eivät täsmää",
    /**/
    "ERROREMAIL1" => "Sähköposti virheellinen",
    /**/
    "ERROREMAIL2" => "Salasana jo rekisteröitynyt",
    /**/
    "ERRORGENERAL" => "Iso virhe, ota yhteyttä tukeen",
    /**/
    "ERRORPROFILEPIC" => "Profiilikuvan latauksessa tapahtui virhe. Yritä uudelleen tilin asetuksien kautta tai Whambush sovelluksella.",
    /**/
    "ERRORDESCRIPTION" => "Kuvauksen latauksessa tapahtui virhe. Yritä uudelleen tilin asetuksien kautta tai Whambush sovelluksella.",
    /**/
    "ERRORBROKENLINK" => "Yksi linkeistä ei ole oikein (kokeile muotoa: www.host.com tai http://host.com)",
    /**/
    "ERRORNOWINNER" => "&quot;Voitajan valintaa&quot; ei valittu",
    /**/
    "ERROREMPTYFIELDS" => "Pakollisia kenttiä tyhjinä",
    /**/
    "ERRORNODATES" => "Virhe ei aloituspäivää tai lopetuspäivää",
    /**/
    "ERRORENDDATE" => "Virhe lopetuspäivä ei voi olla ennen aloituspäivää",
    /**/
    "ERRORVIDEO" => "Ei videotiedosto tai tiedosto liian iso (max 150MB)",

    /* newmission */
    /**/
    "MISSIONVIDEO" => "Tehtävävideo",
    /**/
    "CHOOSEVIDEO" => "Valitse videotiedosto",
    /**/
    "MISSIONIMAGE" => "Tehtäväkuva",
    /**/
    "MISSIONOPTIONAL" => "*Ei pakollinen kenttä",
    /**/
    "MISSIONDETAILS" => "Tehtävän tiedot",
    /**/
    "MISSIONTITLE" => "Tehtävän otsikko",
    /**/
    "MISSIONDESCRIPTION" => "Tehtävän kuvaus",
    /**/
    "MISSIONWINSELECT" => "Valitse kuinka voittaja valitaan",
    /**/
    "MISSIONWINSELECTRANK" => "Järjestyksen/tykkäysten mukaan",
    /**/
    "MISSIONWINSELECTRAND" => "Satunnainen",
    /**/
    "MISSIONWINSELECTSELECT" => "Järjestäjä valitsee",
    /**/
    "MISSIONPRIZE" => "Tehtävän palkinto",
    /**/
    "MISSIONDETAILSCONT" => "Tehtävän tiedot jatkuu",
    /**/
    "MISSIONLINK" => "Markkinointilinkki*",
    /**/
    "MISSIONSTARTDATE" => "Paina valitseeksesi tehvävän aloituspäivän",
    /**/
    "MISSIONENDDATE" => "Paina valitseeksesi tehvävän lopetuspäivän",
    /**/
    "MISSIONPREVIEW" => "Tehtävän esikatselu",
    /**/
    "MISSIONPRIZETXTRANK" => "Paras video saa palkinnoksi %%PRIZE%%.",
    /**/
    "MISSIONPRIZETXTRAND" => "Kaikkien videoiden kesken arvotaan voittaja joka saa palkinnoksi %%PRIZE%%.",
    /**/
    "MISSIONPRIZETXTSELECT" => "%%USERNAME%% valitsee voittajan, jolle lähtee palkinnoksi %%PRIZE%%.",
    /**/
    "MISSIONDURATIONTXT" => "Aikaa osallistua %%DATE%% asti!",
    /**/
    "EDITMISSIONPIC" => "Skaalaa ja rajaa kuva",
    /**/
    "SUCCESSTITLE" => "Tehtävä luotu",
    /**/
    "SUCCESSMESSAGE" => "Tehtävä on luotu onnistunesti. Varmistus ja tehtäväntiedot on lähetetty %%EMAIL%% osoitteeseen.",
    /**/
    "NEWMISSIONMAILSUBJECT" => "Uusi Whambush tehtävä!",
    /**/
    "NEWMISSIONMAILMSG" => "Tässä tehtäväsi tiedot.\n\nLinkki tehtävään: %%MISSIONLINK%%\nVideon latauslinkki: %%VIDEODL%%\n\nKysymyksissä ja ongelmatapauksissa ota yhteyttä Whambush kontaktihenkilöösi tai tuki@whambush.com osoitteeseen.\n\nBR\nWhambush",

    /*  */

    /*  */
    /**/
    "EDITMISSION" => "Muokkaa",
    /**/
    "HANDLEMISSIONS" => "Hallitse tehtäviä",
    /**/
    "SHOWNEWMISSIONS" => "Näytä uudet tehtävät",
    /**/
    "SHOWACTIVEMISSIONS" => "Näytä aktiiviset tehtävät",
    /**/
    "SHOWOLDMISSIONS" => "Näytä vanhat tehtävät",
    /**/
    "STARTDATE" => "Aloituspäivä",
    /**/
    "MISSIONSHARELINK" => "Tehtävän linkki",
    /**/
    "ENDDATE" => "Lopetuspäivä",
    /**/
    "MISSIONWINNER" => "Voittaja",
    /**/
    "MISSIONSTATS" => "Tilastot",
    /**/
    "MISSIONUSERVIDEOS" => "Käyttäjien videot",
    /**/
    "VIDEOLINK" => "Video",
    /**/
    "ORDER" => "Järjestys",
    /**/
    "RANK" => "Paremmuusjärjestys",
    /**/
    "RANDOM" => "Satunnainen",
    /**/
    "SELECTWINNER" => "Vahvista voittaja",
    /**/
    "MISSIONEDITDONE" => "Tehtävä tallennettu",
    /**/
    "MISSIONWINNERDONE" => "Voittaja/t on nyt valittu ja sähköposti/t lähetetty. Kopio postista on lähetetty myös sinulle.",
    /**/
    "MISSIONWINNERCOMMENT" => "Onnea! Videosi voitti tämän tehtävän. Tarkkaile sähköpostiasi niin saadaan palkinto perille.",
    /**/
    "MISSIONWINNERTITLE" => "Voittaja valittu!",
    /**/
    "MISSIONWINNERDESCRIPTION" => "Voittaja:",
    /**/
    "MISSIONWINNERMAILTITLE" => "Olet Whambush tehtävänvoittaja",
    /**/
    "MISSIONHASWINNER" => "Tehtävän voittaja/t:",
  ];
?>
