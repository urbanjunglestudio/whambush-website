<?php
  /*File generated: 4.7.2016 - 05:05:00 UTC*/
  return [
    /* TEMPLATES */
    /**/
    "WINNER_MAIL_SUBJECT" => "Onnea Whambush voitosta!",
    /**/
    "WINNER_MAIL_TMPL_1" => "Super mahtavia uutisia %%USERNAME%%, sä oot voittanut Whambush-tehtävän &quot;%%MISSION%%&quot;!!\n \nKuten asiaan kuuluu, saat palkinnon loistavasta suorituksestasi.\nTällä kertaa, kiitoksena hyvästä työstä, wb-tilillesi ropisee yhteensä %%BANANAS%% banaania.\n \nJatka samaan malliin, kerää lisää banaaneja ja kuvaa hauskoja videoita!\n \nGorillaterveisin,\nWhambush-tiimi\n",
    /**/
    "WINNER_MAIL_TMPL_2" => "Super mahtavia uutisia %%USERNAME%%, sä oot voittanut Whambush-tehtävän &quot;%%MISSION%%&quot;!!\n \nKuten asiaan kuuluu, saat palkinnon loistavasta suorituksestasi.\nTällä kertaa, kiitoksena hyvästä työstä, saat palkinnoksi %%PRIZE%%.\n \nJotta saadaan palkinto liikkeelle, lähettäisitkö meille postiosoitteesi ja puhelinnumerosi. Vaikka ihan vain vastaamalla tähän viestiin!\n \nJatka samaan malliin, kerää lisää banaaneja ja kuvaa hauskoja videoita!\n \nGorillaterveisin,\nWhambush-tiimi\n",
    /**/
    "WINNER_MAIL_TMPL_3" => "Super mahtavia uutisia %%USERNAME%%, sä oot voittanut Whambush-tehtävän &quot;%%MISSION%%&quot;!!\n%%LINK%%\n \nKuten asiaan kuuluu, saat palkinnon loistavasta suorituksestasi.\n \nJotta saadaan palkinto liikkeelle, lähettäisitkö meille postiosoitteesi ja puhelinnumerosi. Vaikka ihan vain vastaamalla tähän viestiin!\n \nJatka samaan malliin, kerää lisää banaaneja ja kuvaa hauskoja videoita!\n \nGorillaterveisin,\n%%CREATOR%%\n",
  ];
?>
