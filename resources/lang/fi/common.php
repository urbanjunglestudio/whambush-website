<?php
  /*File generated: 4.7.2016 - 05:04:59 UTC*/
  return [
    /* COMMON */
    /**/
    "open" => "Avaa",
    /**/
    "close" => "Sulje",
    /**/
    "faq" => "UKK",
    /**/
    "feedback" => "Anna palautetta",
    /**/
    "toggle" => "Avaa/sulje navigaatio",
    /**/
    "terms" => "Palvelun käyttöehdot",
    /**/
    "selected_language" => "Suomi",
    /**/
    "tw_widget_id" => "475684343449264128",
    /**/
    "following" => "Seuratut",
    /**/
    "followers" => "Seuraajat",
    /**/
    "videos" => "Videot",
    /**/
    "bananas" => "Banaanit",
    /**/
    "missions" => "Haasteet",
    /**/
    "advertise" => "Mainostaja",
    /**/
    "contact" => "Ota yhteyttä",
    /**/
    "showmissionvideo" => "Näytä tehtävän video",
    /**/
    "ends" => "loppuu",
    /**/
    "moremissions" => "Lisää tehtäviä löytyy Whambush appista!",
    /**/
    "mission_ends" => "Aikaa jäljellä",
    /**/
    "open_in_app" => "Avaa Whambush sovelluksessa",
    /**/
    "not_soon" => "Ei ihan vähään aikaan",
    /**/
    "finland" => "Suomi",
    /**/
    "estonia" => "Viro",
    /**/
    "global" => "Global",
    /**/
    "notyetvideos" => "Ei vielä yhtään osallistujaa",
  ];
?>
