<?php
  /*File generated: 4.7.2016 - 05:05:01 UTC*/
  return [
    /* RULES */
    /**/
    "MISSIONPAGE" => "Tehtäväsivu",
    /**/
    "SUPPORT" => "tuki@whambush.com",
    /**/
    "PAGE_TITLE" => "Säännöt",
    /**/
    "SECTION_1_TITLE" => "Rekisteröinti:",
    /**/
    "SECTION_1_1" => "Sinun pitää olla rekisteröity Whambushin käyttäjä voidaksesi osallistua kilpailuihin. Huomioi että rekisteröityessäsi ja käyttäessäsi Whambush-sovellusta hyväksyt käyttäjänä kaikki säännöt ja käyttöehdot jotka löytyvät %%TERMS%%.",
    /**/
    "SECTION_2_TITLE" => "Kilpailunjärjestäjä:",
    /**/
    "SECTION_2_1" => "Kilpailun järjestäjä on %%USERNAME%%. Voit lukea lisää järjestäjästä klikkaamalla linkkiä kilpailun kuvauksen alta sivulla %%MISSIONPAGE%%",
    /**/
    "SECTION_2_2" => "Vaikka Whambush sovellus toimii niin iOS kuin Android laitteilla Apple tai Google ei ole mitenkään osallisena kilpailuiden järjestämisessä tai sponsoroinnissa. Lisä kysymyksiin Whambush kilpailuiden järjestelyistä vastataan Urban Jungle Studiosin toimesta osoitteessa info@whambush.com.",
    /*e*/
    "SECTION_2_2_IOS" => "Vaikka Whambush sovellus toimii iOS laitteilla Apple ei ole mitenkään osallisena kilpailuiden järjestämisessä tai sponsoroinnissa. Lisä kysymyksiin Whambush kilpailuiden järjestelyistä vastataan Urban Jungle Studiosin toimesta osoitteessa info@whambush.com.",
    /**/
    "SECTION_3_TITLE" => "Kilpailun tiedot:",
    /**/
    "SECTION_3_1" => "Kilpailun (1) pituus, (2) rajoitukset kilpailuun liittyen, (3) miten voittaja valitaan ja (4) kilpailun palkinto löytyvät aina kilpailun kuvauksesta sivulla %%MISSIONPAGE%%. Huomioi että tehtävävideo ei aina sisällä kaikkia yksityiskohtia kilpailuun liittyen - saadaksesi varmasti tarkat yksityiskohdat, lue huolellisesti kilpailun kuvaus sivulta %%MISSIONPAGE%%.",
    /**/
    "SECTION_4_TITLE" => "Hyväksytty osallistuminen:",
    /**/
    "SECTION_4_1" => "Kilpailuun osallistumisien pitää noudattaa %%TERMS%% ja niiden pitää olla asianmukaisesti esitettyjä jotta ne hyväksytään päteviksi osallistumisiksi.",
    /**/
    "SECTION_5_TITLE" => "Palvelun ehdot ja yksityisyyden suoja:",
    /**/
    "SECTION_5_1" => "Katso %%TERMS%%",
    /**/
    "SECTION_6_TITLE" => "Käyttäjän ikä:",
    /**/
    "SECTION_6_1" => "Palvelumme ei ole suunnattu alle 13-vuotiaille. Mikäli tietoosi tulee että lapsesi on käyttänyt palveluitamme henkilökohtaisten tietojen kanssa ilman Sinun suostumustasi, ole ystävällinen ja ota yhteyttä meihin %%SUPPORT%%. Emme tietoisesti kerää henkilötietoja alle 13-vuotiailta.",
    /**/
    "SECTION_7_TITLE" => "Nollatoleranssipolitiikkaa:",
    /**/
    "SECTION_7_1" => "Ole hyvä ja huomaa, että Whambushilla on nollatoleranssi videoita kohtaan, joissa esiintyy kiusaamista, rasismia, uskonnollisia tai sukupuoleen liittyviä suvaitsemattomuuksia sekä henkilökohtaisia suuntautumisia. Oman seurantamme lisäksi me Whambushilla luotamme Sinun käyttäjänä auttavan meitä ilmoittamalla loukkaavista videoista Whambush-sovelluksessa; me tarkistamme kaikki merkityt videot, ja poistamme ne joita pidämme nollatoleranssipolitiikan vastaisina, tai muulla tavalla eriää meidän ehdoista sekä palvelu-ja yksityisyyspolitiikasta. Lähetämme varoituksen sähköpostitse kaikille käyttäjille jotka lähettävät hyökkääviä videoita. Useampaa hyökkäävää videota lähettänyttä seuraa käyttäjän bannaus. Videoita voi raportoida Whambush-sovelluksen kautta, tai ottamalla yhteyttä %%SUPPORT%%. Huomaa että Whambush riippuu käyttäjien yhteisön asianmukaisesta käytöksestä - Merkintöjen väärinkäyttö videoissa jotka eivät osoittaudu hyökkääviksi voi aiheuttaa varoituksen, ja jopa käyttäjän bannaamisen Whambushista.",
    /**/
    "SECTION_8_TITLE" => "Kilpailun voittaja:",
    /**/
    "SECTION_8_1" => "Kilpailun voittajat valitaan jollakin seuraavalla tavalla; satunnainen, sijoitus, tai suosikki (käytetty menetelmä on määritelty kilpailun kuvauksessa).",
    /**/
    "SECTION_8_2" => "Satunnainen: Voittajavideo on satunnaisesti valittu kaikista lähetetyistä osallistumisista.",
    /**/
    "SECTION_8_3" => "Sijoitus: Video joka on sijoittunut ensimmäiselle paikalle Whambushin algoritmin mukaan kilpailun loppuessa katsotaan voittajaksi. Sijoitus-algoritmi käyttää muun muassa videoiden tykkäyksiä, katsojakertoja ja kommentteja.",
    /**/
    "SECTION_8_4" => "Suosikki: Kilpailun järjestäjä valitsee suosikkivideon kaikista lähetetyistä osallistumisista.",
    /**/
    "SECTION_9_TITLE" => "Voittajalle ilmoittaminen:",
    /**/
    "SECTION_9_1" => "Erilaiset yritykset ovat mukana kaikissa kilpailuissa, joten pyrimme varmistamaan että he ilmoittavat voittajan kohtuullisen (yleensä viikon) ajan sisällä kilpailun päätyttyä. Whambush lähettää sähköpostia voittajalle/voittajille ohjeiden kera joissa kerrotaan miten palkinto lunastetaan. Voittajalla on kuukausi aikaa lunastaa palkinto sähköpostin lähettämisen jälkeen. Lisää tietoa löydät %%FAQ%%.",
    /**/
    "SECTION_9_2" => "Huomaa, että kannustamme kilpailuiden järjestäjiä ilmoittamaan kilpailun voittajat omilla sosiaalisen median sivustoilla (esimerkiksi Facebook, Twitter, Snapchat); voittajat voidaan välillä myös ilmoittaa Whambushissa kilpailun järjestäjän tilillä tietyn kilpailun päätyttyä.",
    /**/
    "SECTION_10_TITLE" => "Palkinnot ja verot:",
    /**/
    "SECTION_10_1" => "Palkinnot kilpailuista jotka käyttävät (1) suosiota tai (2) voittajasuosikkia metodeina katsotaan markkinointikilpailuksi, ja ovat siten verotettavia tuloja voittajille, ja ne pitää ilmoittaa verottajalle. Palkinnot kilpailuista joissa käytetään satunnaisen voittajan valintaa katsotaan markkinointi-arvonnaksi, ja nämä palkinnot ovat verovapaita voittajille.",
  ];
?>
