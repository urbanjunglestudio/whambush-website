<?php
  /*File generated: 4.7.2016 - 05:04:58 UTC*/
  return [
    /* HOME */
    /**/
    "mission_title" => "Haasteet",
    /**/
    "mission_txt" => "Kuvaa tehtäviä ja voita palkintoja! Päivittäin vaihtuvia tehtäviä, ja viikoittainen ikonitehtävä mielenkiintoiselta artistilta, bändiltä tai julkkikselta.",
    /**/
    "prize_title" => "Palkinnot",
    /**/
    "prize_txt" => "Parhaat videot palkitaan, ja palkinnot vaihtelevat aina vaatteista karkkiin ja lahjakortteihin!",
    /**/
    "wbtv_title" => "Whambush TV",
    /**/
    "wbtv_txt" => "Eksklusiivia piensarjoja. Pääset muun muassa seuraamaan bändejä, artisteja, ja oppimaan taikatemppuja.",
    /**/
    "gowild" => "Go Wild, Go Whambush!",
    /**/
    "submit" => "Osallistu ilmaisiin videohaasteisiin voittaaksesi makeita palkintoja.",
    /**/
    "tweet" => "Viimeisimmät twiitit",
    /**/
    "follow" => "Seuraa meitä",
    /**/
    "download" => "Lataa Whambush-sovellus!",

    /* LINKS */
    /**/
    "ios_link" => "/ios",
    /**/
    "android_link" => "/android",
    /**/
    "facebook_link" => "/fi/facebook",
    /**/
    "instagram_link" => "/fi/instagram",
    /**/
    "twitter_link" => "/fi/twitter",
    /**/
    "youtube_link" => "/fi/youtube",
  ];
?>
