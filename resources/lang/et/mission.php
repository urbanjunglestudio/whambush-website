<?php
  /*File generated: 4.7.2016 - 05:05:01 UTC*/
  return [
    /* INFO */
    /**/
    "mission_enter_help" => "Osale ülesandes",
    /**/
    "mission_help_label1" => "1. Lae endale mobiilirakendus telefoni",
    /**/
    "mission_help_text1" => "Saadaval nii iOS&#039;il kui ka Androidil",
    /**/
    "mission_help_label2" => "2. Vali ülesanne",
    /**/
    "mission_help_text2" => "Mobiilirakenduses vali ülesanne milles soovid osaleda ja filmi oma video",
    /**/
    "mission_help_label3" => "3. Lae oma video üles",
    /**/
    "mission_help_text3" => "Kui oled oma videoga rahul siis lae video üles ning siis oledki valmis!",
    /**/
    "mission_help_button" => "Okei!",
  ];
?>
