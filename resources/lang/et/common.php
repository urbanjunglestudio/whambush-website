<?php
  /*File generated: 4.7.2016 - 05:04:59 UTC*/
  return [
    /* COMMON */
    /**/
    "open" => "Ava",
    /**/
    "close" => "Sulge",
    /**/
    "faq" => "FAQ",
    /**/
    "feedback" => "Anna tagasisidet",
    /**/
    "toggle" => "Ava või sulge navigeerimine",
    /**/
    "terms" => "Kasutustingimused",
    /**/
    "selected_language" => "Eesti",
    /**/
    "tw_widget_id" => "622068970397540354",
    /**/
    "following" => "Jälgib",
    /**/
    "followers" => "Jälgijad",
    /**/
    "videos" => "Videod",
    /**/
    "bananas" => "Banaanid",
    /**/
    "missions" => "Missioonid",
    /**/
    "advertise" => "Reklaamija",
    /**/
    "contact" => "Võtke meiega ühendust",
    /**/
    "showmissionvideo" => "Näita ülesande videot",
    /**/
    "ends" => "lõpeb",
    /**/
    "moremissions" => "Rohkem ülesandeid võib leida Whambush&#039;i rakendusest!",
    /**/
    "mission_ends" => "Aega jäänud",
    /**/
    "open_in_app" => "Ava Whambush&#039;i mobiilirakenduses",
    /**/
    "not_soon" => "Lähiajal veel mitte",
    /**/
    "finland" => "Soome",
    /**/
    "estonia" => "Eesti",
    /**/
    "global" => "Globaal",
    /**/
    "notyetvideos" => "Pole veel osalejaid",
  ];
?>
