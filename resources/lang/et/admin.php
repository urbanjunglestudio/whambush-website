<?php
  /*File generated: 4.7.2016 - 05:05:00 UTC*/
  return [
    /* TEMPLATES */
    /**/
    "WINNER_MAIL_SUBJECT" => "Palju õnne Whambush&#039;i võidu puhul!",
    /**/
    "WINNER_MAIL_TMPL_1" => "Suurepärane uudis %%USERNAME%%, Sa oled võitnud Whambush’i ülesande nimega &quot;%%MISSION%%&quot;!!\n \nTubli tehtud töö vajab tublisti tasustamist!\nSeekord saad sa %%BANANAS%% banaani lisatud oma kontole.\n \nJätka suurepärase tööga, kogu veel rohkem banaane ja filmi lahedaid videosid!\n \nParimate soovidega,\nWhambush’i meeskond\n",
    /**/
    "WINNER_MAIL_TMPL_2" => "Suurepärane uudis %%USERNAME%%, Sa oled võitnud Whambush’i ülesande nimega &quot;%%MISSION%%&quot;!!\n \nTubli tehtud töö vajab tublisti tasustamist!\nSeekord saad sa %%PRIZE%%.\nSelleks, et saata auhinda õigesse kohta, vajame meie Sinu aadressi ja telefoni numbri. Palun saada oma andmed vastates sellele e-kirjale ning saamegi auhinna panna Sulle teele!\nJätka suurepärase tööga, kogu veel rohkem banaane ja filmi lahedaid videosid!\n \nParimate soovidega,\nWhambush’i meeskond\n",
    /**/
    "WINNER_MAIL_TMPL_3" => "Suurepärane uudis %%USERNAME%%, Sa oled võitnud Whambush’i ülesande nimega &quot;%%MISSION%%&quot;!!\n \nTubli tehtud töö vajab tublisti tasustamist!\n\nSelleks, et saata auhinda õigesse kohta, vajame meie Sinu aadressi ja telefoni numbri. Palun saada oma andmed vastates sellele e-kirjale ning saamegi auhinna panna Sulle teele!\nJätka suurepärase tööga, kogu veel rohkem banaane ja filmi lahedaid videosid!\n \nParimate soovidega,\nWhambush’i meeskond\n",
  ];
?>
