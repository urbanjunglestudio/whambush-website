<?php
  /*File generated: 4.7.2016 - 05:04:58 UTC*/
  return [
    /* HOME */
    /**/
    "mission_title" => "Missioonid",
    /**/
    "mission_txt" => "Iga päev on uus video võistlus ning sellest osavõtmine on väga lihtne. Tšekka üle missiooni leht, vajuta missiooni nuppu ja filmi oma video.",
    /**/
    "prize_title" => "Auhinnad",
    /**/
    "prize_txt" => "Kõige populaarsem video võidab auhindu mis varieeruvad muusikast, kommideni, mängudest riieteni ja palju muud!",
    /**/
    "wbtv_title" => "Whambush TV",
    /**/
    "wbtv_txt" => "Sisaldab endas lahedaid kaadri taguseid videosid ning ka palju muud",
    /**/
    "gowild" => "Go Wild, Go Whambush!",
    /**/
    "submit" => "Osale tasuta video võistlustes, et võita lahedaid auhindu.",
    /**/
    "tweet" => "Viimased twiidid",
    /**/
    "follow" => "Jälgi meid",
    /**/
    "download" => "Lae alla Whambushi äpp!",

    /* LINKS */
    /**/
    "ios_link" => "/ios",
    /**/
    "android_link" => "/android",
    /**/
    "facebook_link" => "/et/facebook",
    /**/
    "instagram_link" => "/et/instagram",
    /**/
    "twitter_link" => "/et/twitter",
    /**/
    "youtube_link" => "/et/youtube",
  ];
?>
