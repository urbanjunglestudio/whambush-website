<?php
  /*File generated: 4.7.2016 - 05:04:58 UTC*/
  return [
    /* RULES */
    /*Same for all languages!!!!*/
    "SECTION_1_ICON" => "icon-rules",
    /**/
    "SECTION_1_TITLE" => "Reeglid",
    /**/
    "SECTION_1_1" => "Kehtiv video postitus",
    /**/
    "SECTION_1_1_1" => "Kehtivaks loetakse video postitust siis, kui video ei sisalda endas ohtlikku sisu ja on korrektselt postitatud missiooni alla",
    /**/
    "SECTION_1_2" => "Missiooni videod",
    /**/
    "SECTION_1_2_1" => "Missiooni jaoks postitatud videot ei saa eemaldada. Video eemaldamine võib anda lõpuks valed tulemused missiooni hindamisele ning võib tekitada probleeme auhindade jagamisel",
    /**/
    "SECTION_1_3" => "Kuidas on võitjad valitud",
    /**/
    "SECTION_1_3_1" => "Päevaste missioonide võitjad on valitud Whambush&#039;i algoritmi järgi",
    /**/
    "SECTION_1_3_2" => "Nädalaste missioonide võitjad on valitud ikoonide poolt",
    /**/
    "SECTION_1_3_3" => "Kui sa ei ole nõudnud oma auhinda 30 päeva jooksul siis sa jääd sellest ilma. Peale seda kui Whambush&#039;i meeskond on sinuga ühendust võtnud, on sul 30 päeva, et auhinda endale nõuda või saata meile aadress kuhu soovid saada auhinda. Kui sinuga pole ühendust võetud, palun võta tugiga ühendust",
    /**/
    "SECTION_1_5" => "Võltsitud kontod",
    /**/
    "SECTION_1_5_1" => "Sinu banaanide kogus võib varieeruda kui me oleme kustutanud võlts kontod. Kui sa kahtlustad kedagi sohki tegemas, palun teavita kas videot või kasutajat tugile",
    /**/
    "SECTION_1_6" => "Hoiatus",
    /**/
    "SECTION_1_6_1" => "Kõik juhud on erinevad ning tugi meeskonnal on õigus teha reeglites erandeid",

    /* PRIZES */
    /*Same for all languages!!!!*/
    "SECTION_2_ICON" => "icon-star",
    /**/
    "SECTION_2_TITLE" => "Auhinnad",
    /**/
    "SECTION_2_1" => "Kuidas saada auhinda kätte",
    /**/
    "SECTION_2_1_1" => "Auhinnad võivad olla füüsilised või virtuaalsed. Mõlemal juhul võetakse sinuga ühendust e-maili vahendusel. Meie saadame sulle virtuaalse auhinna või siis küsime aadressi kuhu on soov auhinda kätte saada",
    /**/
    "SECTION_2_2" => "Ma ei saanud oma auhinda kätte",
    /**/
    "SECTION_2_2_1" => "Kui sa oled üle vaadanud oma e-maili ja oma prügi kausta peale kahte nädalat võistluse võidust, palun võta ühendust tugi meeskonnaga ja lisa enda kasutajanimi ja missioon mille sa võitsid. Sa ei sa välja vahetada oma auhinda või saada rahalist kompensatsiooni",

    /* USER ACCOUNT */
    /*Same for all languages!!!!*/
    "SECTION_3_ICON" => "icon-profile",
    /**/
    "SECTION_3_TITLE" => "Kasutaja",
    /**/
    "SECTION_3_1" => "Aktiveeri oma kasutaja",
    /**/
    "SECTION_3_1_1" => "Peale registreerimist palun vajuta aktiveerimise lingile mis on saadetud sinu e-mailile.  Kui sa ei tee seda 2 päeva jooksul, on sinu kasutaja deaktiveeritud.",
    /**/
    "SECTION_3_2" => "Unustasid oma parooli",
    /**/
    "SECTION_3_2_1" => "Kui sinu sisselogimine ebaõnnestub, vajuta &#039;&#039;unustasid oma salasõna&#039;&#039; nuppu ja lisa oma e-maili aadress",
    /**/
    "SECTION_3_3" => "Kuidas muuta oma parooli",
    /**/
    "SECTION_3_3_1" => "Mine seadetesse ja otsi &#039;&#039;genereeri uus parool&#039;&#039; lahtrit",

    /* CONTENT */
    /*Same for all languages!!!!*/
    "SECTION_4_ICON" => "icon-play",
    /**/
    "SECTION_4_TITLE" => "Sisu",
    /**/
    "SECTION_4_1" => "Kuidas kustutada oma videot",
    /**/
    "SECTION_4_1_1" => "Mine oma videole mida sa kustutada soovid, puutuda kolme täppi video all ja vajuta prügikasti ikooni",
    /**/
    "SECTION_4_2" => "Kuidas kustutada missiooni videot",
    /**/
    "SECTION_4_2_1" => "Võistluse reeglite kohaselt ei tohiks kustutada missiooni videot. Kuid kui on väga hea põhjus selleks, siis palun võtke ühendust tugi meeskonnaga ja nemad vaatavad nõuded üle.",
    /**/
    "SECTION_4_3" => "Kuidas eemaladada kommentaare",
    /**/
    "SECTION_4_3_1" => "iOS: puuduta ja hoia oma näppu kommentaari peal kuni ilmuvad lisa valikud",
    /**/
    "SECTION_4_4" => "Kuidas raporteerida videot",
    /**/
    "SECTION_4_4_1" => "Vajuta kolm täppi video all ja vajuta lipu nuppu",

    /* MISSIONS */
    /*Same for all languages!!!!*/
    "SECTION_5_ICON" => "icon-missions",
    /**/
    "SECTION_5_TITLE" => "Missioonid",
    /**/
    "SECTION_5_1" => "Igapäevased missioonid",
    /**/
    "SECTION_5_1_1" => "Esmaspäevast-laupäevani saad Sina Whmabush&#039;i gorilla käest ülesandeid. Missioonid kestavad tavaliselt 7 päeva ja võitjad saavad e-maili teel teavituse ning võitjate videod on nähtavad vanade missioonide all.",
    /**/
    "SECTION_5_2" => "Nädalased missioonid",
    /**/
    "SECTION_5_2_1" => "Need missioonid sisaldavad endas suurimaid auhindu ja ka videot mis selgitab võistlust lahti. Võitjad on teavitatud videos ja saavad ka e-maili teel teavituse.",
    /**/
    "SECTION_5_3" => "Erinevate maade missioonides osalemine",
    /**/
    "SECTION_5_3_1" => "Kui sa osaled mõne teise maa missioonis kuid sul ei ole selles riigis legaalset aadressit kuhu auhinda saata, saadetakse auhind ainult siis kui maksad kinni saatmiskulud. Kui ei, siis auhind antakse välja teisele kohale.",
    /**/
    "SECTION_5_4" => "Kuidas laadida oma videot uuesti missiooniks",
    /**/
    "SECTION_5_4_1" => "Kui sa oled üleslaadinud video, kuid sa ei täginud seda missiooni alla siis lihtsalt lae see video uuesti üles ja postita see õige missiooni alla. Vana video palun kustuta ära",

    /* APP */
    /*Same for all languages!!!!*/
    "SECTION_6_ICON" => "icon-banana",
    /**/
    "SECTION_6_TITLE" => "Äpp",
    /**/
    "SECTION_6_1" => "Toetatud platvormid",
    /**/
    "SECTION_6_1_1" => "iOS 7 ja uuemad. Android 4.2 ja uuemad.",
    /**/
    "SECTION_6_2" => "Teavita veast",
    /**/
    "SECTION_6_2_1" => "Ava äpp ja mine seadetesse katsudes profiili ikooni. Leia tugi meeskonna link ja seadetes peaks see avama sinu e-maili koos sinu telefoni informatsiooniga.",
    /**/
    "SECTION_6_3" => "Ei leidnud seda mida otsisid?",
    /**/
    "SECTION_6_3_1" => "Võta ühendust tugi meeskonnaga",
  ];
?>
