<?php
  /*File generated: 4.7.2016 - 05:05:02 UTC*/
  return [
    /* common */
    /**/
    "LOGIN" => "Logi sisse",
    /**/
    "LOGOUT" => "Logi välja",
    /**/
    "WELCOME" => "Tere tulemast",
    /**/
    "USERNAME" => "Kasutajanimi",
    /**/
    "PASSWORD" => "Salasõna",
    /**/
    "FORGOTPASS" => "Unustasid salasõna?",
    /**/
    "REGISTER" => "Registreerimiseks vajuta siia",
    /**/
    "LOGIN_FAILED" => "Vale kasutajanimi/parool või kasutaja pole aktiveeritud, proovi uuesti",
    /**/
    "PLEASE_LOGIN" => "Jätkamiseks pead olema sisselogitud",
    /**/
    "CANCEL" => "Tühista",
    /**/
    "FINISH" => "Lõpeta",
    /**/
    "NEXT" => "Järgmine",
    /**/
    "PREV" => "Eelmine",
    /**/
    "LOADING" => "Laeb...",
    /**/
    "BACK" => "Tagasi",
    /**/
    "MISSION" => "Ülesanne",
    /**/
    "CLOSE" => "Sulge",
    /**/
    "SAVE" => "Salvesta",

    /* main page */
    /**/
    "NEWMISSION" => "Loo uus ülesanne",
    /**/
    "CURRENTMISSIONS" => "Näita aktiivseid ülesandeid",
    /**/
    "OLDMISSIONS" => "Halda vanu ülesandeid",
    /**/
    "HANDLEMISSIONS" => "Halda ülesandeid",
    /**/
    "CREATE" => "Loo uus",
    /**/
    "CHECK" => "Näita",
    /**/
    "HANDLE" => "Halda",
    /**/
    "HELP" => "Probleemide korral võtke meiega ühendust support@whambush.com",
    /**/
    "HELPPDF" => "Lae kliendiportaali abijuhend pdf formaadis",

    /* register */
    /**/
    "REGISTERTITLE" => "Registreeru",
    /**/
    "NAME" => "Nimi",
    /**/
    "KEYCODE" => "Kliendikood",
    /**/
    "KEYCODEINFO" => "Palun sisestage kood mille Whambushi müügiagent Teile saatis.",
    /**/
    "EMAIL" => "E-mail",
    /**/
    "PASSWORD2" => "Salasõna uuesti",
    /**/
    "PROFILEPICTURE" => "Profiilipilt",
    /**/
    "DESCRIPTION" => "Profiili kirjeldus",
    /**/
    "COUNTRY" => "Riik",
    /**/
    "EDITPROFILEPIC" => "Muuda pildi suurust",
    /**/
    "CHOOSEPIC" => "Vali pilt",
    /**/
    "USERDETAILS" => "Kasutaja informatsioon",
    /**/
    "USERDETAILSINFO" => "Täida kõik väljad",
    /**/
    "REGISTERDONE" => "Registreerimine valmis",
    /**/
    "ACTIVATIONMESSAGE" => "Palun aktiveeri oma kasutaja läbi e-maili mis oli saadetud %%EMAIL%%. Peale kasutaja aktiveerimist võid logida süsteemi.",
    /**/
    "MOREINFO" => "Lisainfo",
    /**/
    "PREVIEW" => "Eelvaade",
    /**/
    "ICONNAME" => "Ülesande nimi",
    /**/
    "CHOOSECOUNTRY" => "Vali riik",

    /* errors */
    /**/
    "WRONGCODE" => "Kood ei kehti",
    /**/
    "ERRORUSERNAME1" => "Kasutajanimi pole sisestatud",
    /**/
    "ERRORUSERNAME2" => "Kasutajanimi on juba kasutusel",
    /**/
    "ERRORPASS1" => "Salasõna on liiga lühike (vähemalt 4 tähemärki)",
    /**/
    "ERRORPASS2" => "Salasõnad ei ühti",
    /**/
    "ERROREMAIL1" => "E-mail ei kehti",
    /**/
    "ERROREMAIL2" => "E-mail on juba kasutusel",
    /**/
    "ERRORGENERAL" => "Üldine viga, palun võtke tehnilise toega ühendust",
    /**/
    "ERRORPROFILEPIC" => "Tekkis probleem pildi üleslaadimisel. Proovige uuesti kasutaja",
    /**/
    "ERRORDESCRIPTION" => "Tekkis probleem ülesande kirjelduse üleslaadimisega. Palun proovige uuesti kasutaja seadistustest või läbi Whambushi rakenduse.",
    /**/
    "ERRORBROKENLINK" => "Üks linkidest ei ole toimiv(proovi samamoodi: www.host.com või http://host.com)",
    /**/
    "ERRORNOWINNER" => "&quot;Võitja valitakse&quot; ei ole valitud",
    /**/
    "ERROREMPTYFIELDS" => "Kohustuslikud väljad on tühjad",
    /**/
    "ERRORNODATES" => "Viga! Ei ole alguse ega lõppkuupäeva",
    /**/
    "ERRORENDDATE" => "Viga! Lõppkuupäev ei saa olla alguskuupäevast varasem",
    /**/
    "ERRORVIDEO" => "Video fail on vigane või on video fail liiga suur (max 150MB)",

    /* newmission */
    /**/
    "MISSIONVIDEO" => "Ülesande video",
    /**/
    "CHOOSEVIDEO" => "Vali video fail",
    /**/
    "MISSIONIMAGE" => "Ülesande pilt",
    /**/
    "MISSIONOPTIONAL" => "*Väli ei ole kohustuslik",
    /**/
    "MISSIONDETAILS" => "Ülesande detailid",
    /**/
    "MISSIONTITLE" => "Ülesande pealkiri",
    /**/
    "MISSIONDESCRIPTION" => "Ülesande kirjeldus",
    /**/
    "MISSIONWINSELECT" => "Vali kuidas võitja valitakse",
    /**/
    "MISSIONWINSELECTRANK" => "Põhineb auaste/laikide alusel",
    /**/
    "MISSIONWINSELECTRAND" => "Juhuslik",
    /**/
    "MISSIONWINSELECTSELECT" => "Organiseerija valib",
    /**/
    "MISSIONPRIZE" => "Ülesande auhind",
    /**/
    "MISSIONDETAILSCONT" => "Ülesande informatsioon jätkub",
    /**/
    "MISSIONLINK" => "Turunduslink*",
    /**/
    "MISSIONSTARTDATE" => "Vajuta, et valida ülesande algus kuupäev",
    /**/
    "MISSIONENDDATE" => "Vajuta, et valida ülesande lõppkuupäev",
    /**/
    "MISSIONPREVIEW" => "Ülesande eelvaade",
    /**/
    "MISSIONPRIZETXTRANK" => "Parim video saab auhinnaks %%PRIZE%%.",
    /**/
    "MISSIONPRIZETXTRAND" => "Kõikide videote seast valime juhuslikult ühe võitja kes saab endale auhinnaks %%PRIZE%%.",
    /**/
    "MISSIONPRIZETXTSELECT" => "Võitja on valitud %%USERNAME%% poolt ning ta saab auhinnaks %%PRIZE%%.",
    /**/
    "MISSIONDURATIONTXT" => "Ülesandes saab osaleda kuni %%DATE%%!",
    /**/
    "EDITMISSIONPIC" => "Muuda pildi suurust",
    /**/
    "SUCCESSTITLE" => "Ülesanne loodud",
    /**/
    "SUCCESSMESSAGE" => "Ülesanne on nüüd edukalt loodud. Kinnitus ja kõik ülesande detailid on saadetud %%EMAIL%%.",
    /**/
    "NEWMISSIONMAILSUBJECT" => "Uus Whambushi ülesanne!",
    /**/
    "NEWMISSIONMAILMSG" => "Siin on ülesande detailid.\n\nÜlesande link:%%MISSIONLINK%%\nVideo allalaadimine:%%VIDEODL%%\n\nLisa abi saamiseks kontakteeruge Teie Whambushi kontaktisikuga või saatke e-mail support@whambush.com.\n\nBR\nWhambush",

    /*  */

    /*  */
    /**/
    "EDITMISSION" => "Redageeri",
    /**/
    "HANDLEMISSIONS" => "Halda ülesandeid",
    /**/
    "SHOWNEWMISSIONS" => "Näita uusi ülesandeid",
    /**/
    "SHOWACTIVEMISSIONS" => "Näita aktiivseid ülesandeid",
    /**/
    "SHOWOLDMISSIONS" => "Näita vanu ülesandeid",
    /**/
    "STARTDATE" => "Algus kuupäev",
    /**/
    "MISSIONSHARELINK" => "Ülesande link",
    /**/
    "ENDDATE" => "Lõppkuupäev",
    /**/
    "MISSIONWINNER" => "Võitja",
    /**/
    "MISSIONSTATS" => "Statistika",
    /**/
    "MISSIONUSERVIDEOS" => "Kasutaja videod",
    /**/
    "VIDEOLINK" => "Video",
    /**/
    "ORDER" => "Järjestus",
    /**/
    "RANK" => "Põhineb pingereal",
    /**/
    "RANDOM" => "Juhuslik",
    /**/
    "SELECTWINNER" => "Kinnita võitja",
    /**/
    "MISSIONEDITDONE" => "Ülesanne salvestatud",
    /**/
    "MISSIONWINNERDONE" => "Võitja/d on nüüd teavitatud. Võitja e-maili koopia on saadetud ka teile.",
    /**/
    "MISSIONWINNERCOMMENT" => "Õnnitlused! Olete võitnud whambushi ülesande. Vaadake oma e-mail, et me saaksime teile auhinna saata.",
    /**/
    "MISSIONWINNERTITLE" => "Võitja valitud!",
    /**/
    "MISSIONWINNERDESCRIPTION" => "Võitja:",
    /**/
    "MISSIONWINNERMAILTITLE" => "Oled Whambushi võitja",
    /**/
    "MISSIONHASWINNER" => "Ülesande võitja/d:",
  ];
?>
