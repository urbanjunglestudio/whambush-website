<?php
  /*File generated: 4.7.2016 - 05:04:58 UTC*/
  return [
    /* HOME */
    /**/
    "mission_title" => "Missioner",
    /**/
    "mission_txt" => "Filma missioner, och vinn priser! Dagligen varierande uppgifter, och veckovisa ikon missioner av intressanta artister, band eller kändisar",
    /**/
    "prize_title" => "Priser",
    /**/
    "prize_txt" => "Bästa videon belönas, och prisen varierar från kläder till godis och presentkort!",
    /**/
    "wbtv_title" => "Whambush TV",
    /**/
    "wbtv_txt" => "Eksklusiva serier. Här hittar du bland annat bander, artister och lär dig att göra magiska tricker.",
    /**/
    "gowild" => "Go Wild, Go Whambush!",
    /**/
    "submit" => "Delta i gratis uppmaningar för att vinna tuffa priser",
    /**/
    "tweet" => "Senaste tweets",
    /**/
    "follow" => "Följ oss",
    /**/
    "download" => "Ladda Whambush-appen!",

    /* LINKS */
    /**/
    "ios_link" => "/ios",
    /**/
    "android_link" => "/android",
    /**/
    "facebook_link" => "/en/facebook",
    /**/
    "instagram_link" => "/en/instagram",
    /**/
    "twitter_link" => "/en/twitter",
    /**/
    "youtube_link" => "/en/youtube",
  ];
?>
