<?php
  /*File generated: 4.7.2016 - 05:04:59 UTC*/
  return [
    /* COMMON */
    /**/
    "open" => "Öppna",
    /**/
    "close" => "Stäng",
    /**/
    "faq" => "FAQ",
    /**/
    "feedback" => "Ge feedback",
    /**/
    "toggle" => "Öppna/Stäng navigationen",
    /**/
    "terms" => "Användarvillkor",
    /**/
    "selected_language" => "Svenska",
    /**/
    "tw_widget_id" => "475684343449264128",
    /**/
    "following" => "Följda",
    /**/
    "followers" => "Följare",
    /**/
    "videos" => "Videon",
    /**/
    "bananas" => "Bananer",
    /**/
    "missions" => "Uppdrag",
    /**/
    "advertise" => "Annonsör",
    /**/
    "contact" => "Ta kontakt",
    /**/
    "showmissionvideo" => "Visa videon för uppdragen",
    /**/
    "ends" => "slutar",
    /**/
    "moremissions" => "Fler uppdrag hittas i Whambush-appen!",
    /**/
    "mission_ends" => "Tid kvar",
    /**/
    "open_in_app" => "Öppna i Whambush-appen",
    /**/
    "not_soon" => "Inte på ett bra tag",
  ];
?>
