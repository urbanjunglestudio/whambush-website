<?php
  /*File generated: 4.7.2016 - 05:04:58 UTC*/
  return [
    /* RULES */
    /*Same for all languages!!!!*/
    "SECTION_1_ICON" => "icon-rules",
    /**/
    "SECTION_1_TITLE" => "Reglerna",
    /**/
    "SECTION_1_1" => "Giltigt innehåll",
    /**/
    "SECTION_1_1_1" => "Som giltigt innehåll räknas material som inte är skadligt, och är lämpligt till missionen",
    /**/
    "SECTION_1_2" => "Mission videon",
    /**/
    "SECTION_1_2_1" => "Videon som skickats till missionen kan ej raderas. Raderande av videon i missionen kan orsaka fel resultat i rangordningen, och leda till problem i belöningen av vinnarna.",
    /**/
    "SECTION_1_3" => "Hur väljs vinnarna",
    /**/
    "SECTION_1_3_1" => "Dagliga vinnaren är valda av Whambush ranking-algoritm",
    /**/
    "SECTION_1_3_2" => "Ikonerna väljer själv för hand vinnaren av veckouppdrag",
    /**/
    "SECTION_1_3_3" => "Om du inte löser ut din pris inom 1 månad, mister du den. Efter att Whambush-team tagit kontakt i dig, har du 30 dagar tid att lösa ut priset, eller skicka din postadress åt oss. Om du inte har blivit kontaktad, var vänlig och skicka meddelande till stödet.",
    /**/
    "SECTION_1_5" => "Falska konton",
    /**/
    "SECTION_1_5_1" => "Mängden av dina bananer kan variera, då vi städar bort falska konton. Om du misstänker att någon fuskar, var vänlig och rapportera konton/videon till stödet.",
    /**/
    "SECTION_1_6" => "Ansvarsfriskrivning",
    /**/
    "SECTION_1_6_1" => "Alla fall är olika, och stödet har rättigheterna att göra undantag i reglerna",

    /* PRIZES */
    /*Same for all languages!!!!*/
    "SECTION_2_ICON" => "icon-star",
    /**/
    "SECTION_2_TITLE" => "Priser",
    /**/
    "SECTION_2_1" => "Hur löser man ut priserna",
    /**/
    "SECTION_2_1_1" => "Priserna kan vara fysiska eller virtuella. I båda fall blir du kontaktad via email. Vi skickar virtuella prisen åt dig, eller frågar efter din postadress.",
    /**/
    "SECTION_2_2" => "Jag fick inte mitt pris",
    /**/
    "SECTION_2_2_1" => "Om du har kollat din email och skräppost efter två veckor av vinsten, var vänlig och kontakta stödet och inkludera ditt användarnamn och missionen du vunnit i meddelandet. Priset kan inte bytas eller mottas som pengar.",

    /* USER ACCOUNT */
    /*Same for all languages!!!!*/
    "SECTION_3_ICON" => "icon-profile",
    /**/
    "SECTION_3_TITLE" => "Användarkonto",
    /**/
    "SECTION_3_1" => "Aktivera ditt användarkonto",
    /**/
    "SECTION_3_1_1" => "Efter registreringen, var vänlig och klicka på aktiveringslänken som skickats till dig via email. Om du inte gör det inom 2 dagar, deaktiveras ditt konto.",
    /**/
    "SECTION_3_2" => "Glömde du ditt lösenord",
    /**/
    "SECTION_3_2_1" => "När inloggandet misslyckas, klicka &quot;Glömt lösenordet&quot;, och fyll i ditt email",
    /**/
    "SECTION_3_3" => "Hur nollställs lösenordet",
    /**/
    "SECTION_3_3_1" => "Gå till inställningar och sök &quot;nollställning av lösenordet&quot;",

    /* CONTENT */
    /*Same for all languages!!!!*/
    "SECTION_4_ICON" => "icon-play",
    /**/
    "SECTION_4_TITLE" => "Innehåll",
    /**/
    "SECTION_4_1" => "Hur kan du radera din video",
    /**/
    "SECTION_4_1_1" => "Gå till videon du vill radera, klicka på de tre punkterna under videon, och välj skräpkorg-ikonen",
    /**/
    "SECTION_4_2" => "Hur kan man radera missionvideon",
    /**/
    "SECTION_4_2_1" => "Enligt tävlingsreglerna kan du inte radera missionsvideon. Om det ändå finns en bra orsak, var vänlig och kontakta stödet, så går de igenom anfordran från fall till fall.",
    /**/
    "SECTION_4_3" => "Hur kan man radera kommentarer",
    /**/
    "SECTION_4_3_1" => "iOS: Klicka och håll ditt finger på kommentaren, tills du ser funktionsknappen",
    /**/
    "SECTION_4_4" => "Hur kan man rapportera videon",
    /**/
    "SECTION_4_4_1" => "Klicka på de tre punkterna under videon, och välj flagg-ikonen",

    /* MISSIONS */
    /*Same for all languages!!!!*/
    "SECTION_5_ICON" => "icon-missions",
    /**/
    "SECTION_5_TITLE" => "Missionerna",
    /**/
    "SECTION_5_1" => "Dagliga missioner",
    /**/
    "SECTION_5_1_1" => "Whambush-gorillan delar ut dagliga missioner från måndag till lördag. Missionerna varar vanligtvis i 7 dagar, och vinnarna får meddelande via email. Vinnarnas videon hittas i de gamla missionerna. Högdagar och semester kan orsaka avvikelser i publikationer av missioner, och deras varaktigheter",
    /**/
    "SECTION_5_2" => "Veckovisa missioner",
    /**/
    "SECTION_5_2_1" => "I dessa missioner finns det större priser och en video, som förklarar uppgiften. Vinnarna annonseras på video, och de får också meddelande via email.",
    /**/
    "SECTION_5_3" => "Deltagande i utländska missioner",
    /**/
    "SECTION_5_3_1" => "Om du deltar i en utländsk mission, och har inte en giltig adress till landet, skickas priset bara om du betäcker fraktkostnaderna. Om inte, får den på andra platsen priset.",
    /**/
    "SECTION_5_4" => "Hur kan man skicka om videon till missionen",
    /**/
    "SECTION_5_4_1" => "Om du har laddat videon, men har inte taggat den till missionen, kan du skicka om den till missionen, och radera den gamla videon.",

    /* APP */
    /*Same for all languages!!!!*/
    "SECTION_6_ICON" => "icon-banana",
    /**/
    "SECTION_6_TITLE" => "App",
    /**/
    "SECTION_6_1" => "Stödjade platformer",
    /**/
    "SECTION_6_1_1" => "iOS 7 och nyare. Android 4.2 och nyare.",
    /**/
    "SECTION_6_2" => "Rapportera problem",
    /**/
    "SECTION_6_2_1" => "Öppna appen, gå till inställningar genom att klicka på profil ikonen. Leta efter stöd-länken från inställningarna, var efter den borde öppna din email med den information som krävs.",
    /**/
    "SECTION_6_3" => "Hittade du inte vad du letade efter?",
    /**/
    "SECTION_6_3_1" => "Kontakta stöden",
  ];
?>
