<?php
  /*File generated: 4.7.2016 - 05:04:58 UTC*/
  return [
    /* RULES */
    /*Same for all languages!!!!*/
    "SECTION_1_ICON" => "icon-rules",
    /**/
    "SECTION_1_TITLE" => "Rules",
    /**/
    "SECTION_1_1" => "Valid submission",
    /**/
    "SECTION_1_1_1" => "Viewed as a valid submission is content that is not harmful and is properly submitted to the mission",
    /**/
    "SECTION_1_2" => "Mission videos",
    /**/
    "SECTION_1_2_1" => "Video submitted to mission cannot be removed. Removing a mission video can give wrong results to the mission ranking and can cause problems awarding winners.",
    /**/
    "SECTION_1_3" => "How the winners are chosen",
    /**/
    "SECTION_1_3_1" => "Daily mission winners are chosen by the Whambush ranking algorithm",
    /**/
    "SECTION_1_3_2" => "Weekly mission winners are hand picked by the icon",
    /**/
    "SECTION_1_3_3" => "If you don&#039;t claim your prize within a month you will lose it. After you have been contacted by the Whambush team, you have 30 days to claim the prize or send us your shipping address. If you have not been contacted please send a message to support.",
    /**/
    "SECTION_1_5" => "Fake accounts",
    /**/
    "SECTION_1_5_1" => "Your amount of bananas might vary when we clean up fake accounts. If you suspect someone cheating please report the account/video to support.",
    /**/
    "SECTION_1_6" => "Disclaimer",
    /**/
    "SECTION_1_6_1" => "All cases are different and the support has the authority to make exceptions in the rules",

    /* PRIZES */
    /*Same for all languages!!!!*/
    "SECTION_2_ICON" => "icon-star",
    /**/
    "SECTION_2_TITLE" => "Prizes",
    /**/
    "SECTION_2_1" => "How are prizes claimed",
    /**/
    "SECTION_2_1_1" => "Prizes can be physical or virtual. In both cases you will be contacted via email. We will send you the virtual prize or ask for your shipping address.",
    /**/
    "SECTION_2_2" => "I didn&#039;t get my prize",
    /**/
    "SECTION_2_2_1" => "If you have checked your email and spam folder after two weeks from winning a competition, please contact support and include your username and the mission you have won. You cannot swap your prize or receive it as cash payment.",

    /* USER ACCOUNT */
    /*Same for all languages!!!!*/
    "SECTION_3_ICON" => "icon-profile",
    /**/
    "SECTION_3_TITLE" => "User account",
    /**/
    "SECTION_3_1" => "Activate your account",
    /**/
    "SECTION_3_1_1" => "After registering please click the activation link sent to you by email. If you don’t do it within 2 days your account will be deactivated.",
    /**/
    "SECTION_3_2" => "Forgot your password",
    /**/
    "SECTION_3_2_1" => "When your login fails tap &quot;forgot password&quot; and fill in your email",
    /**/
    "SECTION_3_3" => "How to reset password",
    /**/
    "SECTION_3_3_1" => "Go to settings and find the field &quot;reset password&quot;",

    /* CONTENT */
    /*Same for all languages!!!!*/
    "SECTION_4_ICON" => "icon-play",
    /**/
    "SECTION_4_TITLE" => "Content",
    /**/
    "SECTION_4_1" => "How to delete your video",
    /**/
    "SECTION_4_1_1" => "Go to the video you want to delete and tap the three dots below the video and hit the trashcan icon",
    /**/
    "SECTION_4_2" => "How to delete a mission video",
    /**/
    "SECTION_4_2_1" => "By the competition rules you can not delete the mission video. But if there is a good reason for it, then please contact support and they will review the claims case by case.",
    /**/
    "SECTION_4_3" => "How to remove comments",
    /**/
    "SECTION_4_3_1" => "iOS: tap and hold your finger on top of your comment until you see the action buttons",
    /**/
    "SECTION_4_4" => "How to report a video",
    /**/
    "SECTION_4_4_1" => "Tap three dots below the video and hit the flag icon",

    /* MISSIONS */
    /*Same for all languages!!!!*/
    "SECTION_5_ICON" => "icon-missions",
    /**/
    "SECTION_5_TITLE" => "Missions",
    /**/
    "SECTION_5_1" => "Daily missions",
    /**/
    "SECTION_5_1_1" => "From mo-sa you will receive daily missions from the Whambush Gorilla. The missions usually last for 7 days and winners will receive an email notification and winners video can be seen in old missions. Holidays and vacation periods can cause irregularities to mission postings and times how long missions are run.",
    /**/
    "SECTION_5_2" => "Weekly missions",
    /**/
    "SECTION_5_2_1" => "These missions have bigger prizes and a video explaining the challenge.   The winners are announced on a video and notified by email.",
    /**/
    "SECTION_5_3" => "Entering different country missions",
    /**/
    "SECTION_5_3_1" => "If you enter a competition in a different country but do not have a valid address in that country, the prize will only be shipped if you cover the shipping cost. If not it will be given to the 2nd position.",
    /**/
    "SECTION_5_4" => "How to resubmit video to mission",
    /**/
    "SECTION_5_4_1" => "If you have uploaded a video but have not tagged it to a mission then you can still re-upload it, submit it to the mission and delete the previous video",

    /* APP */
    /*Same for all languages!!!!*/
    "SECTION_6_ICON" => "icon-banana",
    /**/
    "SECTION_6_TITLE" => "App",
    /**/
    "SECTION_6_1" => "Supported platforms",
    /**/
    "SECTION_6_1_1" => "iOS 7 and up. Android 4.2 and up.",
    /**/
    "SECTION_6_2" => "Report a bug",
    /**/
    "SECTION_6_2_1" => "Open the app and go to settings by tapping the profile icon. Find the support link in the settings and it should open your email with the required device information.",
    /**/
    "SECTION_6_3" => "Didn&#039;t find what you were looking for?",
    /**/
    "SECTION_6_3_1" => "Contact support",
  ];
?>
