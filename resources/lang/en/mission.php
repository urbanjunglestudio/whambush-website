<?php
  /*File generated: 4.7.2016 - 05:05:01 UTC*/
  return [
    /* INFO */
    /**/
    "mission_enter_help" => "Enter the competition",
    /**/
    "mission_help_label1" => "1. Get the app to your phone",
    /**/
    "mission_help_text1" => "Available for iOS and Android",
    /**/
    "mission_help_label2" => "2. Select the mission",
    /**/
    "mission_help_text2" => "In the app, select the mission you want to participate in and film your video",
    /**/
    "mission_help_label3" => "3. Upload your video",
    /**/
    "mission_help_text3" => "Once you&#039;re satisfied with the video hit upload and your done!",
    /**/
    "mission_help_button" => "Got it!",
  ];
?>
