<?php
  /*File generated: 4.7.2016 - 05:05:00 UTC*/
  return [
    /* TEMPLATES */
    /**/
    "WINNER_MAIL_SUBJECT" => "Congratulations for winning at Whambush!",
    /**/
    "WINNER_MAIL_TMPL_1" => "Great news %%USERNAME%%, you have won Whambush mission called &quot;%%MISSION%%&quot;!!\n \nAs it goes you will receive a prize of job well done.\nThis time you will get %%BANANAS%% of bananas to your wb account.\n \nKeep up the good work, collect more bananas and shoot great videos!\n \nBest regards,\nTeam Whambush\n",
    /**/
    "WINNER_MAIL_TMPL_2" => "Great news %%USERNAME%%, you have won Whambush mission called &quot;%%MISSION%%&quot;!!\n \nAs it goes you will receive a prize of job well done.\nThis time you will get %%PRIZE%%.\n \nTo deliver the prize to correct place we would need your address. Please send it by replying this email and the prize will we heading your way in no time!\n \nKeep up the good work, collect more bananas and shoot great videos!\n \nBest regards,\nTeam Whambush\n",
    /**/
    "WINNER_MAIL_TMPL_3" => "Great news %%USERNAME%%, you have won Whambush mission called &quot;%%MISSION%%&quot;!!\n%%LINK%%\n \nAs it goes you will receive a prize of job well done.\n \nTo deliver the prize to correct place we would need your address. Please send it by replying this email and the prize will we heading your way in no time!\n \nKeep up the good work, collect more bananas and shoot great videos!\n \nBest regards,\n%%CREATOR%%\n",
  ];
?>
