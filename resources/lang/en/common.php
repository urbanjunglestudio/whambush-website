<?php
  /*File generated: 4.7.2016 - 05:04:59 UTC*/
  return [
    /* COMMON */
    /**/
    "open" => "Open",
    /**/
    "close" => "Close",
    /**/
    "faq" => "FAQ",
    /**/
    "feedback" => "Give feedback",
    /**/
    "toggle" => "Toggle navigation",
    /**/
    "terms" => "Terms of Service",
    /**/
    "selected_language" => "English",
    /**/
    "tw_widget_id" => "475684343449264128",
    /**/
    "following" => "Following",
    /**/
    "followers" => "Followers",
    /**/
    "videos" => "Videos",
    /**/
    "bananas" => "Bananas",
    /**/
    "missions" => "Missions",
    /**/
    "advertise" => "Advertiser",
    /**/
    "contact" => "Contact us",
    /**/
    "showmissionvideo" => "Show mission video",
    /**/
    "ends" => "ends",
    /**/
    "moremissions" => "More missions can be found in the Whambush app!",
    /**/
    "mission_ends" => "Time remaining",
    /**/
    "open_in_app" => "Open in Whambush app",
    /**/
    "not_soon" => "Not any time soon",
    /**/
    "finland" => "Finland",
    /**/
    "estonia" => "Estonia",
    /**/
    "global" => "Global",
    /**/
    "notyetvideos" => "No submissions yet",
  ];
?>
