<?php
  /*File generated: 4.7.2016 - 05:05:01 UTC*/
  return [
    /* RULES */
    /**/
    "MISSIONPAGE" => "Mission page",
    /**/
    "SUPPORT" => "support@whambush.com",
    /**/
    "PAGE_TITLE" => "Rules",
    /**/
    "SECTION_1_TITLE" => "REGISTRATION:",
    /**/
    "SECTION_1_1" => "You must be a registered Whambush user in order to submit a video entry to any of the competitions. Please note by registering to and using the Whambush platform service You the user expressly accept all terms and conditions contained in our %%TERMS%%.",
    /**/
    "SECTION_2_TITLE" => "COMPETITION ORGANIZER:",
    /**/
    "SECTION_2_1" => "The organizer of a competition is %%USERNAME%%. You can usually learn more about an organizer by clicking the links underneath a competition description located on the %%MISSIONPAGE%%.",
    /**/
    "SECTION_2_2" => "Although Whambush App can be used in iOS and Android devices, Apple or Google are expressly not involved or responsible in any way whatsoever in any of the competitions available through Whambush, including without limitation as a sponsor of any competition. In the event that you have any questions regarding this issue, please ask us at Urban Jungle Studios via email by writing to info@whambush.com.",
    /*e*/
    "SECTION_2_2_IOS" => "Although Whambush App can be used in iOS devices, Apple is expressly not involved or responsible in any way whatsoever in any of the competitions available through Whambush, including without limitation as a sponsor of any competition. In the event that you have any questions regarding this issue, please ask us at Urban Jungle Studios via email by writing to info@whambush.com.",
    /**/
    "SECTION_3_TITLE" => "COMPETITION DETAILS:",
    /**/
    "SECTION_3_1" => "The (1) length of each competition, (2) restrictions for a competition, (3) how a winner is selected and (4) competition prize are always contained in the competition description text found on the  %%MISSIONPAGE%%. Please note the mission video of a competition may not always contain any or all of that competition’s details - to be sure to get the accurate details, carefully read the competition description text found on the %%MISSIONPAGE%%.",
    /**/
    "SECTION_4_TITLE" => "VALID SUBMISSION:",
    /**/
    "SECTION_4_1" => "Competition submissions must comply with our %%TERMS%% and must be properly submitted in order to qualify as a valid submission.",
    /**/
    "SECTION_5_TITLE" => "TERMS OF USE AND SERVICE AND PRIVACY POLICY:",
    /**/
    "SECTION_5_1" => "Please see our %%TERMS%%.",
    /**/
    "SECTION_6_TITLE" => "USER AGE:",
    /**/
    "SECTION_6_1" => "Our Service is not directed to people under 13 years of age. If you become aware that your child has provided us with personal information without your consent, please contact us at %%SUPPORT%%. We do not knowingly collect personal information from children under 13.",
    /**/
    "SECTION_7_TITLE" => "ZERO TOLERANCE POLICY:",
    /**/
    "SECTION_7_1" => "Please note Whambush has a zero tolerance policy for videos that are offensive whether through bullying, racism, religious intolerance or against gender or personal orientation. In addition to our own monitoring, we at Whambush rely on You the user to help us out by flagging any offensive videos posted on the Whambush platform; we check out all flagged videos and take offline those videos we deem to be contrary to our zero tolerance policy or otherwise contrary to our published terms of us, service or privacy policy. We will send out a warning email to any user submitting offensive videos. Multiple offensive submissions will result in a user ban. Video submissions can be reported via Whambush app or by contacting %%SUPPORT%%. Please note Whambush depends on appropriate behavior from its user community - abuse of flagging videos that prove not to be offensive may result in a warning and even a user ban from the Whambush.",
    /**/
    "SECTION_8_TITLE" => "HOW COMPETITION WINNERS ARE CHOSEN:",
    /**/
    "SECTION_8_1" => "Competition winners are chosen by one of the following methods: random, ranking, or favorite  (the method used is specified in the competition description text).",
    /**/
    "SECTION_8_2" => "Random: A winning video entry is randomly chosen from the valid submissions",
    /**/
    "SECTION_8_3" => "Ranking: The video entry that is ranked as number one by Whambush ranking algorithm at the close of the competition is deemed the winner.  Ranking algorithm uses, among otherthings, video likes, views, and comments.",
    /**/
    "SECTION_8_4" => "Favorite: the competition organizer picks their favorite video from all video submissions.",
    /**/
    "SECTION_9_TITLE" => "WHEN AND WHERE WINNERS ARE NOTIFIED:",
    /**/
    "SECTION_9_1" => "Different companies are involved in each competition so we try to ensure that they announce the winners within a reasonable (usually a week) time after the end of their competitions. The Whambush will send out an email to the winner(s) with instructions on how to claim the prize. Winning user has one month time to claim the prize after mail has been sent to winner. More info at %%FAQ%%.",
    /**/
    "SECTION_9_2" => "Please note, we encourage competition organizers to announce the winners of their competition on their social media channels (this can mean FB, Instagram, Twitter, Snapchat); winners may sometimes be announced via video from the organizers Whambush account after the end of a particular competition.",
    /**/
    "SECTION_10_TITLE" => "PRIZES AND TAXES:",
    /**/
    "SECTION_10_1" => "Prizes from competitions that use the (1) popularity or (2) favorite winner methods are considered to be marketing competition and thus these prizes are taxable income for the winners and must be declared by them individually to the tax authorities. Prizes from competition that use the random winner method are considered to be marketing lotteries and these prizes are tax-free for the winners.",
  ];
?>
