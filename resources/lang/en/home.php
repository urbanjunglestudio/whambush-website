<?php
  /*File generated: 4.7.2016 - 05:04:58 UTC*/
  return [
    /* HOME */
    /**/
    "mission_title" => "Missions",
    /**/
    "mission_txt" => "Fulfill missions by shooting and uploading a video to win prizes! Missions range from weekly video challenges to daily missions",
    /**/
    "prize_title" => "Prizes",
    /**/
    "prize_txt" => "The best videos win. Prizes range from games and phones to candy, clothing and tickets",
    /**/
    "wbtv_title" => "Whambush TV",
    /**/
    "wbtv_txt" => "Features cool behind-the-scene footage from bands, artists and others",
    /**/
    "gowild" => "Go Wild, Go Whambush!",
    /**/
    "submit" => "Submit videos to free challenges for a chance to win cool prizes.",
    /**/
    "tweet" => "Latest tweets",
    /**/
    "follow" => "Follow us",
    /**/
    "download" => "Download the Whambush app!",

    /* LINKS */
    /**/
    "ios_link" => "/ios",
    /**/
    "android_link" => "/android",
    /**/
    "facebook_link" => "/en/facebook",
    /**/
    "instagram_link" => "/en/instagram",
    /**/
    "twitter_link" => "/en/twitter",
    /**/
    "youtube_link" => "/en/youtube",
  ];
?>
