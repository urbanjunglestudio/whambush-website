<?php
  /*File generated: 4.7.2016 - 05:05:02 UTC*/
  return [
    /* common */
    /**/
    "LOGIN" => "Login",
    /**/
    "LOGOUT" => "Logout",
    /**/
    "WELCOME" => "Welcome",
    /**/
    "USERNAME" => "Username",
    /**/
    "PASSWORD" => "Password",
    /**/
    "FORGOTPASS" => "Did you forgot password?",
    /**/
    "REGISTER" => "Press here to register",
    /**/
    "LOGIN_FAILED" => "Wrong username/password or account is not activated, try again",
    /**/
    "PLEASE_LOGIN" => "Please login to continue",
    /**/
    "CANCEL" => "Cancel",
    /**/
    "FINISH" => "Finish",
    /**/
    "NEXT" => "Next",
    /**/
    "PREV" => "Previous",
    /**/
    "LOADING" => "Loading...",
    /**/
    "BACK" => "Back",
    /**/
    "MISSION" => "Mission",
    /**/
    "CLOSE" => "Close",
    /**/
    "SAVE" => "Save",

    /* main page */
    /**/
    "NEWMISSION" => "Create new mission",
    /**/
    "CURRENTMISSIONS" => "Show current missions",
    /**/
    "OLDMISSIONS" => "Handle old missions",
    /**/
    "HANDLEMISSIONS" => "Handle missions",
    /**/
    "CREATE" => "Create new",
    /**/
    "CHECK" => "Show",
    /**/
    "HANDLE" => "Handle",
    /**/
    "HELP" => "For help contact support@whambush.com",
    /**/
    "HELPPDF" => "Download customer portal help in pdf format",

    /* register */
    /**/
    "REGISTERTITLE" => "Register",
    /**/
    "NAME" => "Name",
    /**/
    "KEYCODE" => "Customer code",
    /**/
    "KEYCODEINFO" => "Please enter code that was given to you by Whambush sales person.",
    /**/
    "EMAIL" => "Email",
    /**/
    "PASSWORD2" => "Password again",
    /**/
    "PROFILEPICTURE" => "Profile picture",
    /**/
    "DESCRIPTION" => "Profile description",
    /**/
    "COUNTRY" => "Country",
    /**/
    "EDITPROFILEPIC" => "Scale and crop picture",
    /**/
    "CHOOSEPIC" => "Choose picture",
    /**/
    "USERDETAILS" => "User details",
    /**/
    "USERDETAILSINFO" => "Fill out all fields",
    /**/
    "REGISTERDONE" => "Registration complete",
    /**/
    "ACTIVATIONMESSAGE" => "Please activate your account from the mail that has been sent to %%EMAIL%%. After activation you can login to the system.",
    /**/
    "MOREINFO" => "Details",
    /**/
    "PREVIEW" => "Preview",
    /**/
    "ICONNAME" => "Name to be used in missions",
    /**/
    "CHOOSECOUNTRY" => "Select country",

    /* errors */
    /**/
    "WRONGCODE" => "Code is not valid",
    /**/
    "ERRORUSERNAME1" => "Username empty",
    /**/
    "ERRORUSERNAME2" => "Username is taken",
    /**/
    "ERRORPASS1" => "Password too short (needs to be 4 or more)",
    /**/
    "ERRORPASS2" => "Passwords do not match",
    /**/
    "ERROREMAIL1" => "Email is not valid",
    /**/
    "ERROREMAIL2" => "Email already registered",
    /**/
    "ERRORGENERAL" => "General error, please contact support",
    /**/
    "ERRORPROFILEPIC" => "There was error uploading picture. Please try again via account settings or via Whambush app.",
    /**/
    "ERRORDESCRIPTION" => "There was error uploading description. Please try again via account settings or via Whambush app.",
    /**/
    "ERRORBROKENLINK" => "One of the links is not valid link (try similar than: www.host.com or http://host.com)",
    /**/
    "ERRORNOWINNER" => "&quot;How mission winner&quot; has not choosen",
    /**/
    "ERROREMPTYFIELDS" => "Mandatory fields are empty",
    /**/
    "ERRORNODATES" => "Error no mission start date or end date",
    /**/
    "ERRORENDDATE" => "Error mission end date cannot be before start date",
    /**/
    "ERRORVIDEO" => "Not valid video file or file too big (max 150MB)",

    /* newmission */
    /**/
    "MISSIONVIDEO" => "Mission video",
    /**/
    "CHOOSEVIDEO" => "Choose video file",
    /**/
    "MISSIONIMAGE" => "Mission image",
    /**/
    "MISSIONOPTIONAL" => "*Field is not mandatory",
    /**/
    "MISSIONDETAILS" => "Mission details",
    /**/
    "MISSIONTITLE" => "Mission title",
    /**/
    "MISSIONDESCRIPTION" => "Mission description",
    /**/
    "MISSIONWINSELECT" => "Select how winner is choosen",
    /**/
    "MISSIONWINSELECTRANK" => "Based on rank/likes",
    /**/
    "MISSIONWINSELECTRAND" => "Random",
    /**/
    "MISSIONWINSELECTSELECT" => "Organizer selects",
    /**/
    "MISSIONPRIZE" => "Mission prize",
    /**/
    "MISSIONDETAILSCONT" => "Mission details cont.",
    /**/
    "MISSIONLINK" => "Marketing link*",
    /**/
    "MISSIONSTARTDATE" => "Press to select mission start date",
    /**/
    "MISSIONENDDATE" => "Press to select mission end date",
    /**/
    "MISSIONPREVIEW" => "Mission preview",
    /**/
    "MISSIONPRIZETXTRANK" => "Best video will win %%PRIZE%% as a prize.",
    /**/
    "MISSIONPRIZETXTRAND" => "Among all videos we will select randomly one winner who gets %%PRIZE%% as a prize.",
    /**/
    "MISSIONPRIZETXTSELECT" => "Winner is selected by %%USERNAME%% and the winner gets %%PRIZE%% as a prize.",
    /**/
    "MISSIONDURATIONTXT" => "Time to participate until %%DATE%%!",
    /**/
    "EDITMISSIONPIC" => "Scale and crop picture",
    /**/
    "SUCCESSTITLE" => "Mission created!",
    /**/
    "SUCCESSMESSAGE" => "Mission is now created succesfully. Confirmation and all mission details have been sent to %%EMAIL%% address.",
    /**/
    "NEWMISSIONMAILSUBJECT" => "New Whambush missions!",
    /**/
    "NEWMISSIONMAILMSG" => "Here are the mission details.\n\nLink to mission: %%MISSIONLINK%%\nVideo download link: %%VIDEODL%%\n\nFor help please contact your Whambush contact person or support@whambush.com.\n\nBR\nWhambush",

    /*  */

    /*  */
    /**/
    "EDITMISSION" => "Edit",
    /**/
    "HANDLEMISSIONS" => "Handle missions",
    /**/
    "SHOWNEWMISSIONS" => "Show new missions",
    /**/
    "SHOWACTIVEMISSIONS" => "Show active missions",
    /**/
    "SHOWOLDMISSIONS" => "Show old missions",
    /**/
    "STARTDATE" => "Start date",
    /**/
    "MISSIONSHARELINK" => "Link to mission",
    /**/
    "ENDDATE" => "End date",
    /**/
    "MISSIONWINNER" => "Winner",
    /**/
    "MISSIONSTATS" => "Statistics",
    /**/
    "MISSIONUSERVIDEOS" => "User videos",
    /**/
    "VIDEOLINK" => "Video",
    /**/
    "ORDER" => "Order",
    /**/
    "RANK" => "Based on ranking",
    /**/
    "RANDOM" => "Random",
    /**/
    "SELECTWINNER" => "Confirm winner",
    /**/
    "MISSIONEDITDONE" => "Mission saved",
    /**/
    "MISSIONWINNERDONE" => "Winner/s has been now informed. Winner mail is also cc:d to your email.",
    /**/
    "MISSIONWINNERCOMMENT" => "Concratulations! Your video won this missions. Check your email so we can get the prize delivered to you.",
    /**/
    "MISSIONWINNERTITLE" => "Winner done!",
    /**/
    "MISSIONWINNERDESCRIPTION" => "Winner:",
    /**/
    "MISSIONWINNERMAILTITLE" => "You are Whambush mission winner",
    /**/
    "MISSIONHASWINNER" => "Mission winner/s:",
  ];
?>
