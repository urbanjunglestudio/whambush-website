@extends('newbase')

@section('extra_meta')
<meta name="apple-itunes-app" content="app-id=876370933">
@stop

@section('page_title')
<title>Whambush - Missions</title>
@stop

@section('extra_top')
<style>
.missionbox {width: <?php echo round(93/count($countries))."%";?>;}
@media(max-width: 760px) {
    .missionbox {width: 100%;}
}
</style>
@stop


@section('content')
<!-- Page Content -->
<div id="page-content-wrapper">
    <div class="container-fluid" style="max-width: none;">
        <div class="row">

<?php 
    $fi = $countries['FI'];
    unset($countries['FI']);
    $countries = array("FI" => $fi)+$countries; // move FI to first
    foreach ($countries as $country => $details) {
        
        echo '<div class="missionbox"><h1 class="mission_country" style="background-size: cover; background-image: url('.$details['flag_banner_url'].'); background-repeat: no-repeat;">'.trans('common.'.strtolower($details['name'])).'</h1>';
        foreach ($missions[$country] as $mission) {
            if (Config::get('api.version') == "v2.1") {
                echo '<div class="mission_details" onclick="location.href=\''.$mission['mission_url'].'\';">';
            } else {
                echo '<div class="mission_details">';
            }
            $icon_mission  = "";//($mission['mission_type'] === 1) ? "*": "";
            echo '<div class="mission_name"><h2>'.$mission['name'].$icon_mission.'</h2></div>';
            if (isset($mission['linked_video'])) {
                echo '<br><div class="mission_video"><a href="'.$mission['linked_video']['web_url'].'"><img src="'.$mission['mission_url'].'/image.jpg" style="width:100%;"></a></div><br>';
            }
            echo '<div class="mission_description">'.nl2br(urlize($mission['description'])).'</div>';
            
            if (!$mission['is_videos_ranked']) {
                if ($country == "FI" || $country == "EE") {
                    date_default_timezone_set("Europe/Helsinki");
                } else {
                    date_default_timezone_set("UTC");
                }
                $end_time = date("H:i \a\\t d.n.Y T",strtotime($mission['end_at']."UTC"));
                echo '<div class="mission_ends small">'.trans('common.ends').' '.$end_time.'</div> <img class="mission_flag" src="'.$details['flag_url'].'">';
            }
            echo '</div>';
        }
        echo '</div>'.PHP_EOL;
    }
?>
            <div class="col-xs-12">
                <p class="center">{{ trans('common.moremissions') }}</p>
                <?php 
                    if (strpos($_SERVER['REQUEST_URI'], "?old") === false) {
                        echo '<div style="text-align: right; padding-right: 30px;"><a href="/m?old&country=FI,EE">Go to mission archive</a></div>'; 
                    }
                ?>
            </div>
        </div>
    </div>
</div>
<!-- /#page-content-wrapper -->
@stop
