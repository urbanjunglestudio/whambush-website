<!DOCTYPE html>
<head>

    <title>Whambush Admin</title>

    <!-- Bootstrap core CSS -->
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">

    <!-- Fonts -->
    <link href='//fonts.googleapis.com/css?family=Roboto:400,100,300,700,400italic,700italic' rel='stylesheet' type='text/css'>

    <!-- Css -->
    <link href="{{ secure_asset('/css/default.css') }}" rel="stylesheet">
    <link href="{{ secure_asset('/css/admin.css') }}" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="//oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <script src="https://code.jquery.com/jquery-2.1.4.js"></script>
    <script src="{{ secure_asset('js/admin/jquery-ui.custom/jquery-ui.js') }}"></script>
    <link rel="stylesheet" href="{{ secure_asset('js/admin/jquery-ui.custom/jquery-ui.css') }}">

    @yield('extra_js')
    
    
    <!-- Favicons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ asset('/img/apple-touch-icon-144-precomposed.png') }}">
    <link rel="shortcut icon" href="{{ asset('/img/favicon.png') }}">

</head>

<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only"><?php echo trans('common.toggle'); ?></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <?php
        if (strpos(Config::get('api.base_url'),'api.whambush.net') !== false) {
            $isdev = "DEVELOPEMENT";
        } else {
            $isdev = "";
        }
      ?>
      <a class="navbar-brand" href="/wb_admin/"><img src="{{ asset('/img/logo.png') }}"/>{{ $isdev }}</a>
    </div>

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1"> 
      <ul class="nav navbar-nav navbar-right">
        
        <li>
            <a href="/wb_admin/logout">
                Logout
            </a>
        </li>

      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
<body class="admin-page">
    <div id="overlaybkg" hidden></div>
    
    <div id="overlay" hidden>
        <div id="progressbar" hidden><div id="progresstext" hidden></div></div>
        
        <div id='spinner'>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>
<div class="container">
    @yield('content')
    <script>
        $( "#progressbar" ).progressbar();
    </script>
</div>
</body>
</html>