<!DOCTYPE html>
<html lang="fi">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    <meta property="fb:admins" content="694877949" />
    
    @yield('extra_meta','
    <meta name="author" content="Urban Jungle Studios Oy">
    <meta name="description" content="Whambush on mobiilisovellus, joka on täynnä hauskoja videoita ja päättömiä tehtäviä">
    <meta property="og:author" content="Urban Jungle Studios Oy" />
    <meta property="og:title" content="Whambush" />
    <meta property="og:type" content="article" />
    <meta property="og:image" content="http://whambush.com/img/fb.jpg" />
    <meta property="og:description" content="Whambush on mobiilisovellus, joka on täynnä hauskoja videoita ja päättömiä tehtäviä" />
    ')

    @yield('page_title','
    <title>Whambush</title>
    ')
    <!-- Bootstrap core CSS -->
   <script src="https://code.jquery.com/jquery-2.1.4.js"></script>
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/default.css" rel="stylesheet">

    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,700,400italic,700italic' rel='stylesheet' type='text/css'>
    
    @yield('extra_top')

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <!-- Favicons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/img/apple-touch-icon-144-precomposed.png">
    <link rel="shortcut icon" href="/img/favicon.png">
 
    {!! env('HEAD_SCRIPT', '') !!}

<?php
    if (App::environment() == 'prod') {
        echo "
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','//connect.facebook.net/en_US/fbevents.js');
fbq('init', '165473937134470');
fbq('track', \"PageView\");
</script>
<noscript><img height=\"1\" width=\"1\" style=\"display:none\" src=\"https://www.facebook.com/tr?id=165473937134470&ev=PageView&noscript=1\"/></noscript>
<!-- End Facebook Pixel Code -->
       ";
    };
?>

</head>

<body>
<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only"><?php echo trans('common.toggle'); ?></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="{{ LaravelLocalization::getLocalizedURL(null,'/') }}"><img src="/img/logo.png"/></a>
    </div>

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1"> 
      <ul class="nav navbar-nav">
        <li>
            <a href="{{ LaravelLocalization::getLocalizedURL(null,'m/') }}">
                <?php echo trans('common.missions'); ?>
            </a>
        </li>
        
        <li>
            <a href="{{ LaravelLocalization::getLocalizedURL(null,'faq/') }}">
                <?php echo trans('common.faq'); ?>
            </a>
        </li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
          <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><?php echo trans('common.selected_language'); ?> <span class="caret"></span></a>
                <ul class="dropdown-menu" role="menu">
                   @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $localeName)
                        <li>
                            <a rel="alternate" hreflang="{{$localeCode}}" href="{{ LaravelLocalization::getLocalizedURL($localeCode) }}">
                                {{ $localeName }}
                            </a>
                        </li>
                    @endforeach
                </ul>
            </li>
    </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>


<div id="wrapper">
    <!-- Sidebar -->
    <div id="sidebar-wrapper">
        <ul class="sidebar-nav">
            <li>
                <a href="<?php echo trans('home.instagram_link'); ?>" target="_blanc"><img class="img-responsive" src="/img/whambush-ig.jpg"></a>
            </li>
            <li>
                <a href="<?php echo trans('home.facebook_link'); ?>" target="_blanc"><img class="img-responsive" src="/img/whambush-fb.jpg"></a>
            </li>
            <li>
                <a href="<?php echo trans('home.twitter_link'); ?>" target="_blanc"><img class="img-responsive" src="/img/whambush-tw.jpg"></a>
            </li>
            <li>
                <a href="<?php echo trans('home.ios_link'); ?>" target="_blanc"><img class="img-responsive" src="/img/sidebar-appstore.jpg"></a>
            </li>
            <li>
                <a href="<?php echo trans('home.android_link'); ?>" target="_blanc"><img class="img-responsive" src="/img/sidebar-googleplaystore.jpg"></a>
            </li>

        </ul>
    </div>
    <!-- /#sidebar-wrapper -->

    @yield('content')

</div>
<!-- /#wrapper -->

    <div class="socket">
        <p itemscope itemprop="affiliation">
            <a href="{{ LaravelLocalization::getLocalizedURL(null,'faq/') }}"><?php echo trans('common.faq'); ?></a>
            | <a href="/terms-of-service/"><?php echo trans('common.terms'); ?></a>
            <?php /*| <a href="/advertise/"><?php echo trans('common.advertise'); ?></a>*/ ?>
            | <a href="mailto:support@whambush.com"><?php echo trans('common.feedback'); ?></a>
            | &copy; 2014-<?php echo date("Y",time());?> Copyright <a href="http://ujs.fi">Urban Jungle Studios Oy</a>
        </p>
    </div>

    <!-- Placed at the end of the document so the pages load faster -->
    <script src="/js/jquery.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script type="text/javascript">
        $(function () {
          $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
    @yield('extra_bottom')
</body>

</html>
