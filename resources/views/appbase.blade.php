<!DOCTYPE html>
<html lang="fi">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    
    @yield('page_title','
    <title>Whambush</title>
    ')
    <!-- Bootstrap core CSS -->
    <script src="https://code.jquery.com/jquery-2.1.4.js"></script>
    <link href="{{ secure_asset('/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ secure_asset('/css/default.css') }}" rel="stylesheet">

    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,700,400italic,700italic' rel='stylesheet' type='text/css'>
    
    @yield('extra_top')

 
    {!! env('HEAD_SCRIPT', '') !!}

</head>

<body>

<div id="wrapper center">
    @yield('content')
</div>

    <!-- Placed at the end of the document so the pages load faster -->
    <script src="{{ secure_asset('/js/jquery.js') }}"></script>
    <script src="{{ secure_asset('/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript">
        $(function () {
          $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
    @yield('extra_bottom')
</body>

</html>
