@extends('customer_base')

@section('extra_js')

    <script src="{{ asset('js/admin/jquery-ui.custom/jquery-ui.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('js/admin/jquery-ui.custom/jquery-ui.css') }}">
    <?php 
        if (LaravelLocalization::getCurrentLocale() != "en") {
            echo "<script src=".asset('js/customer/datepicker-'.LaravelLocalization::getCurrentLocale().'.js')."></script>";
        }
    ?>    
    <script src="{{ asset('js/customer/sweetalert.min.js') }}"></script>
    <link href="{{ asset('/js/customer/sweetalert.css') }}" rel="stylesheet">
    <script src="{{ asset('js/customer/handle.js') }}"></script>
@stop


@section('content')
<a href="{{ LaravelLocalization::getLocalizedURL(null,'/customer') }}">⬅︎ {{ trans('customer.BACK') }}</a>
<div class="container admin-actions">

    <h1>{{ trans('customer.HANDLEMISSIONS') }}</h1>

    <div class="col-md-12">
        <div id="missionnew" class="missiontyperow" value='{{ $number_of_new_missions }}'>{{ trans('customer.SHOWNEWMISSIONS') }} ({{ $number_of_new_missions }})<img src="/img/down_arrow.png" class="downarrow"></div>
        <div id="missionnewcontent" hidden class="missionsinglerow row"></div>
        <div id="missionactive" class="missiontyperow" value='{{ $number_of_active_missions }}'>{{ trans('customer.SHOWACTIVEMISSIONS') }} ({{ $number_of_active_missions }})<img src="/img/down_arrow.png" class="downarrow"></div>
        <div id="missionactivecontent" hidden class="missionsinglerow row"></div>
        <div id="missionold" class="missiontyperow" value='{{ $number_of_old_missions }}'>{{ trans('customer.SHOWOLDMISSIONS') }} ({{ $number_of_old_missions }})<img src="/img/down_arrow.png" class="downarrow"></div>
        <div id="missionoldcontent" hidden class="missionsinglerow row"></div>
    </div>
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
</div>
<style>
    .ui-dialog { z-index: 9999 !important ;}
</style>
<div id="dialog" title="" hidden></div>
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
});
</script>
@stop