@extends('customer_base')

@section('extra_js')

    <script src="{{ asset('js/admin/jquery-ui.custom/jquery-ui.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('js/admin/jquery-ui.custom/jquery-ui.css') }}">
    <?php 
        if (LaravelLocalization::getCurrentLocale() != "en") {
            echo "<script src=".asset('js/customer/datepicker-'.LaravelLocalization::getCurrentLocale().'.js')."></script>";
        }
    ?>    
    <script src="{{ asset('js/customer/croppie.js') }}"></script>
    <link href="{{ asset('/js/customer/croppie.css') }}" rel="stylesheet">
    <script src="{{ asset('js/customer/jquery.steps.min.js') }}"></script>
    <link href="{{ asset('/js/customer/jquery.steps.css') }}" rel="stylesheet">
    <script src="{{ asset('js/customer/sweetalert.min.js') }}"></script>
    <link href="{{ asset('/js/customer/sweetalert.css') }}" rel="stylesheet">
    <script src="{{ asset('js/customer/upload.js') }}"></script>
    <script src="{{ asset('js/customer/mission.js') }}"></script>
@stop


@section('content')
<a href="{{ LaravelLocalization::getLocalizedURL(null,'/customer') }}">⬅︎ {{ trans('customer.BACK') }}</a>
<div class="container admin-actions">

<h1>{{ trans('customer.NEWMISSION') }}</h1>
{!! Form::open( array(
            'method' => 'post',
            'id' => 'customer_create_mission',
        ) ) 
!!}     
<style type="text/css">
    .wizard > .steps > ul > li {
        width: {{ 100/5 }}%;
    }
</style>   
<div id="mission-wizard">
    <h3>{{ trans('customer.MISSIONVIDEO') }}</h3>
    <section>
        <div class="col-sm-offset-3 col-sm-6">
            <div id="missionvideo" class="center">
                <button class="profilepicbtn btn btn-lg btn-default">
                    <span>{{ trans('customer.CHOOSEVIDEO') }}</span>
                    <input type="file" id="_file_"> 
                </button>              
            </div>
            <br>
            <div id="msg0" class="center" style="color: #CD554A"></div>   
        </div>
    </section>
    <h3>{{ trans('customer.MISSIONIMAGE') }}</h3>
    <section>
        <div class="col-sm-offset-3 col-sm-6">
            <div id="missionpic" class="center">
                <button class="profilepicbtn btn btn-lg btn-default">
                    <span>{{ trans('customer.CHOOSEPIC') }}</span>
                    <input type="file" id="upload_missionpic">
                </button>
                <br><img id="missionpicview" class="missimage" src="" style="width: 640px; height: 360px;" class="center">
                <input type="hidden" value="" id="missionimage">
            </div>
            <p class="center">{{ trans('customer.MISSIONOPTIONAL') }}</p>
        </div>
    </section>
    <h3>{{ trans('customer.MISSIONDETAILS') }}</h3>
    <section>
        <div class="col-sm-offset-3 col-sm-6">
        
            {!! Form::label('title', ' ') !!}<br>
            {!! Form::text( 'title', '', array(
                'class' => 'form-control input-lg',
                'placeholder' => trans('customer.MISSIONTITLE') 
                ) ) 
            !!}
            {!! Form::label('description', ' ') !!}<br>
            {!! Form::textarea( 'description', '', array(
            'class' => 'form-control input-lg',
            'placeholder' => trans('customer.MISSIONDESCRIPTION'),
            'style' => 'height: 200px;'
            ) ) 
            !!}
            
            {!! Form::label('winselect', ' ') !!}<br>
            {!! Form::select('winselect', array(
                    '-' => trans('customer.MISSIONWINSELECT'), 
                    'rank' => trans('customer.MISSIONWINSELECTRANK'),
                    'rand' => trans('customer.MISSIONWINSELECTRAND'),
                    'select' => trans('customer.MISSIONWINSELECTSELECT'),
                )) 
            !!} 

            {!! Form::label('prize', ' ') !!}<br>
            {!! Form::text( 'prize', '', array(
                'class' => 'form-control input-lg',
                'maxlength' => 32, 
                'placeholder' => trans('customer.MISSIONPRIZE') 
            ) ) 
            !!}
            <br>
            <div id="msg1" class="center" style="color: #CD554A"></div>   
            <br>
        </div>
   </section>
    <h3>{{ trans('customer.MISSIONDETAILSCONT') }}</h3>
    <section>
        <div class="col-sm-offset-3 col-sm-6">
            {!! Form::label('link1', ' ') !!}<br>
            {!! Form::text( 'link1', '', array(
                'class' => 'form-control input-lg',
                'placeholder' => trans('customer.MISSIONLINK') 
            ) ) 
            !!}
            {!! Form::label('link2', ' ') !!}
            {!! Form::text( 'link2', '', array(
                'class' => 'form-control input-lg',
                'placeholder' => trans('customer.MISSIONLINK') 
            ) ) 
            !!}
            {!! Form::label('link3', ' ') !!}
            {!! Form::text( 'link3', '', array(
                'class' => 'form-control input-lg',
                'placeholder' => trans('customer.MISSIONLINK') 
            ) ) 
            !!}

            {!! Form::label('mission_start_date', ' ') !!}<br>
            {!! Form::text('mission_start_date', '', array(
                'id' => 'datepicker',
                'placeholder' => trans('customer.MISSIONSTARTDATE'),
                'readonly' => true,
            )) !!} 
            {!! Form::label('mission_end_date', ' ') !!}<br>
            {!! Form::text('mission_end_date', '', array(
                'id' => 'datepicker_e',
                'placeholder' => trans('customer.MISSIONENDDATE'),
                'readonly' => true,
            )) !!} 
            <p class="center"><br>{{ trans('customer.MISSIONOPTIONAL') }}<br></p>
            <br>
            <div id="msg2" class="center" style="color: #CD554A"></div>   
            <br>
        </div>
   </section>
   </section>
    <h3>{{ trans('customer.MISSIONPREVIEW') }}</h3>
    <section>
        <div class="col-sm-6">
            <img src="{{ asset('img/video_thb.jpg') }}" id="videothb" style="width:100%;height:auto;">
            <div id="missiontime" class="mission-specs"></div>
        </div>
        <div class="col-sm-6">
            <h1 style="text-align: left;"><small><div id="missiontitle"></div></small></h1>
            <div id="missiondescription"></div>
            <div id="missiondescriptionraw" value='' hidden></div>
            <div id="missioncreator" value="{{ $username }}" hidden></div>
            <div id="missioncreatorid" value="{{ $userid }}" hidden></div>
            <div id="missioncountry" value="{{ $country }}" hidden></div>
            <div id="missionvideoid" value="" hidden></div>
        </div>
   </section>
</div>


        {!! Form::close() !!}
        <br>
        <p class="center">{{ trans('customer.HELP') }}</p>
    </div>
</div>

<div id="missionpic_dialog" title="{{ trans('customer.EDITMISSIONPIC') }}">
    <div id="upload-missionpic"></div>
    <button  id="crop_ok" class="btn btn-lg btn-default">OK</button>
</div>


<script type="text/javascript">
    $( "#missionpic_dialog" ).dialog({ autoOpen: false, width: 750 });
</script>

<script type="text/javascript">
$("#mission-wizard").steps({
        headerTag: "h3",
        bodyTag: "section",
        transitionEffect: "fade",
        autoFocus: true,
        enableKeyNavigation: false,
        enableCancelButton: true,
        onCanceled: function (event) { 
            location.reload();
        },
        onStepChanging: function (event, currentIndex, newIndex) { 
            return onStepChange(event, currentIndex, newIndex); 
        },
        onFinishing: function (event, currentIndex) { 
            return onWizardFinishing(event, currentIndex); 
        }, 
        labels: {
            cancel: "{{ trans('customer.CANCEL') }}",
            finish: "{{ trans('customer.FINISH') }}",
            next: "{{ trans('customer.NEXT') }}",
            previous: "{{ trans('customer.PREV') }}",
            loading: "{{ trans('customer.LOADING') }}"
        }
    });
</script>
@stop