@extends('customer_base')

@section('extra_js')
    <script src="{{ asset('js/customer/croppie.js') }}"></script>
    <link href="{{ asset('/js/customer/croppie.css') }}" rel="stylesheet">
    <script src="{{ asset('js/customer/jquery.steps.min.js') }}"></script>
    <script src="{{ asset('js/customer/register.js') }}"></script>
    <link href="{{ asset('/js/customer/jquery.steps.css') }}" rel="stylesheet">
    <script src="{{ asset('js/customer/sweetalert.min.js') }}"></script>
    <link href="{{ asset('/js/customer/sweetalert.css') }}" rel="stylesheet">
@stop

@section('content')
<div class="container admin-actions">

<h1>{{ trans('customer.REGISTERTITLE') }}</h1>
{!! Form::open( array(
           
            'method' => 'post',
            'id' => 'customer_register',
        ) ) 
!!}        
<div id="register-wizard">
    <h3>{{ trans('customer.KEYCODE') }}</h3>
    <section>
        <div class="col-sm-offset-3 col-sm-6" style="color: #cecece;">
            {{ trans('customer.KEYCODEINFO') }}<br>
            {!! Form::label('keycode', ' ') !!}<br>
            {!! Form::password( 'keycode',  array(
                'class' => 'form-control input-lg',
                'placeholder' => trans('customer.KEYCODE') 
                ) ) 
            !!}
            <br>
            <div id="msg0" style="color: #CD554A"></div>
        </div>
    </section>
    <h3>{{ trans('customer.USERDETAILS') }}</h3>
    <section>
        <div class="col-sm-offset-3 col-sm-6">
            {!! Form::label('username', ' ') !!}<br>
            {!! Form::text( 'username', '', array(
                'class' => 'form-control input-lg',
                'placeholder' => trans('customer.USERNAME') 
                ) ) 
            !!}
            {!! Form::label('email', ' ') !!}<br>
            {!! Form::text( 'email', '', array(
                'class' => 'form-control input-lg',
                'placeholder' => trans('customer.EMAIL') 
            ) ) 
            !!}
            <br><div id='countries' loc="{{ trans('customer.CHOOSECOUNTRY') }}"></div>
            {!! Form::label('password', ' ') !!}<br>
            {!! Form::password('password', array(
                'class' => 'form-control input-lg',
                'placeholder' => trans('customer.PASSWORD')
                ) ) 
            !!}
            {!! Form::label('password2', ' ') !!}<br>
            {!! Form::password('password2', array(
                'class' => 'form-control input-lg',
                'placeholder' => trans('customer.PASSWORD2')
                ) ) 
            !!} 
            <br>
            <div id="msg1" style="color: #CD554A"></div>   
        </div>
    </section>
    <h3>{{ trans('customer.PROFILEPICTURE') }}</h3>
    <section>
        <div class="col-sm-offset-3 col-sm-6">
            <div id="profilepic" class="center">
                <button class="profilepicbtn btn btn-lg btn-default">
                    <span>{{ trans('customer.CHOOSEPIC') }}</span>
                    <input type="file" id="upload_profilepic">
                </button>
                <div id="profilepicview" class="profimage"></div>
                <div class="profimage"><img src="/img/profileplace.png" class="center"></div>
                <input type="hidden" value="" id="profileimage">
            </div>
        </div>
    </section>
    <h3>{{ trans('customer.MOREINFO') }}</h3>
    <section>
        <div class="col-sm-offset-3 col-sm-6">
        
        {!! Form::label('description', ' ') !!}<br>
        {!! Form::textarea( 'description', '', array(
            'class' => 'form-control input-lg',
            'placeholder' => trans('customer.DESCRIPTION'),
            'maxlength' => 90, 
            ) ) 
        !!}
        </div>
   </section>
    <h3>{{ trans('customer.FINISH') }}</h3>
    <section>
        <div class="col-sm-offset-3 col-sm-6">
        <h2>{{ trans('customer.REGISTERDONE') }}</h2>     
        <div id='activemessage'></div>   
        </div>
   </section>
</div>

   
        {!! Form::close() !!}
        <br>
        <p class="center">{{ trans('customer.HELP') }}</p>
    </div>
</div>

<div id="profilepic_dialog" title="{{ trans('customer.EDITPROFILEPIC') }}">
   <div id="upload-profilepic"></div>
        <button  id="crop_ok" class="btn btn-lg btn-default">OK</button>
    </div>
<script type="text/javascript">
$( "#profilepic_dialog" ).dialog({ autoOpen: false, width: 800 });
</script>

<script type="text/javascript">
$("#register-wizard").steps({
        headerTag: "h3",
        bodyTag: "section",
        transitionEffect: "slideLeft",
        autoFocus: true,
        enableKeyNavigation: false,
        forceMoveForward: true,
        enableCancelButton: true,
        onCanceled: function (event) { 
            location.reload();
        },
        onStepChanging: function (event, currentIndex, newIndex) { 
            return onStepChange(event, currentIndex, newIndex); 
        },
        onFinishing: function (event, currentIndex) { 
            return onWizardFinishing(event, currentIndex); 
        }, 
        labels: {
            cancel: "{{ trans('customer.CANCEL') }}",
            finish: "{{ trans('customer.FINISH') }}",
            next: "{{ trans('customer.NEXT') }}",
            previous: "{{ trans('customer.PREV') }}",
            loading: "{{ trans('customer.LOADING') }}"
        }
    });
</script>
@stop