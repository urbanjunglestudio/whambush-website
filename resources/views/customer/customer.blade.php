@extends('customer_base')


@section('content')
<div class="container admin-actions">

<?php 
    if (isset($admin) && $admin) {
        $width = 98;
    } else {
        $width = 100/2 - 2;
    }
?>

@if ($logged) 
    <h1>{{ $msg }}</h1>

    @if (!(isset($admin) && $admin))
    <div class="faqbox" data-toggle="modal" style="padding: 20px 5px; width: {{$width}}%">
        <span class="icon-missions"></span>
        <h3>{{ trans('customer.NEWMISSION') }}</h3>
        <a href="{{ LaravelLocalization::getLocalizedURL(null,'/customer/missions/new') }}" class="btn btn-default">{{ trans('customer.CREATE') }}</a>
    </div>
    @endif
    <div class="faqbox" data-toggle="modal"  style="padding: 20px 5px; width: {{$width}}%">
        <span class="icon-latest"></span>
        <h3>{{ trans('customer.HANDLEMISSIONS') }}</h3>
        <a href="{{ LaravelLocalization::getLocalizedURL(null,'/customer/missions/handle') }}" class="btn btn-default">{{ trans('customer.HANDLE') }}</a>
    </div>
    <?php /*<div class="faqbox" data-toggle="modal"  style="padding:20px 5px; width: {{$width}}%">
        <span class="icon-trophy"></span>
        <h3>{{ trans('customer.OLDMISSIONS') }}</h3>
        <a href="{{ LaravelLocalization::getLocalizedURL(null,'/customer/missions/old') }}" class="btn btn-default">{{ trans('customer.HANDLE') }}</a>
    </div>*/?>
    <br><p class="center">{{ trans('customer.HELP') }}</p>
    <br><p class="center"><a href="/wb_portal_help_FI.pdf" target="_blank">{{ trans('customer.HELPPDF') }}</a></p>

@else
    <h1>{{ $msg }}</h1>
    <div class="col-sm-offset-3 col-sm-6">
        {!! Form::open( array(
            'route' => 'customer_login',
            'method' => 'post',
            'id' => 'try_login',
        ) ) !!}

        {!! Form::label('username', ' ') !!}<br>
        {!! Form::text( 'username', '', array(
            'class' => 'form-control input-lg',
            'placeholder' => trans('customer.USERNAME') 
            ) ) 
        !!}
        {!! Form::label('password', ' ') !!}<br>
        {!! Form::password('password', array(
            'class' => 'form-control input-lg',
            'placeholder' => trans('customer.PASSWORD')
            ) ) 
        !!}
        <br>
        
        @if (isset($failed) && $failed)
        <p class="center"><a href="https://whambush.com/account/reset_password/" style="color: #CD554A;">{{ trans('customer.FORGOTPASS') }}</a></p>
        @endif

        <br>
        <center>
        {!! Form::submit( trans('customer.LOGIN'), array(
            'id' => '_mission_create',
            'class' => 'btn btn-lg btn-default',
        ) ) !!}
        </center>
        {!! Form::close() !!}
        <br><br>
        
        <p class="center"><a href="{{ LaravelLocalization::getLocalizedURL(null,'/customer/register') }}">{{ trans('customer.REGISTER') }}</a></p>
    </div>
@endif
</div>

@stop