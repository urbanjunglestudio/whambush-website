@extends('admin_base')


@section('content')
<div class="container admin-actions">



@if ($logged == 1) 
    <h1>{{ $msg }}</h1>

    <div class="faqbox" data-toggle="modal">
        <span class="icon-missions"></span>
        <h3>Create new mission</h3>
        <a href="/wb_admin/mission/" class="btn btn-default">Create</a>
    </div>
    <div class="faqbox" data-toggle="modal">
        <span class="icon-profile"></span>
        <h3>Handle mission winners</h3>
        <a href="/wb_admin/winners/" class="btn btn-default">Handle</a>
    </div>
    <div class="faqbox" data-toggle="modal">
        <span class="icon-play"></span>
        <h3>Upload video</h3>
        <a href="/wb_admin/video/" class="btn btn-default">Upload</a>
    </div>

        <p class="center">Works only with <strong>Safari</strong> and <strong>Chrome</strong>!</p>

@else
<h1>{{ $msg }}</h1>
<div class="col-sm-offset-3 col-sm-6">
    {!! Form::open( array(
        'route' => 'admin_login',
        'method' => 'post',
        'id' => 'try_login',
    ) ) !!}

    {!! Form::label('username', ' ') !!}<br>
    {!! Form::text( 'username', '', array(
    'class' => 'form-control input-lg',
    'placeholder' => 'Username'
    ) ) !!}
    {!! Form::label('password', ' ') !!}<br>
    {!! Form::password( 'password', array(
    'class' => 'form-control input-lg',
    'placeholder' => 'Password'
    ) ) !!}

    <br>
    <br>
    {!! Form::submit( 'Login', array(
        'id' => '_mission_create',
        'class' => 'btn btn-lg btn-default',
    ) ) !!}
     
    {!! Form::close() !!}

@endif
    </div>
</div>
@stop