@extends('admin_base')

@section('extra_js')
<script src=<?php echo asset('js/admin/upload.js')/*."?".mt_rand()*/; ?>></script>
<script src=<?php echo asset('js/admin/video.js')/*."?".mt_rand()*/; ?>></script>
<script src="{{ asset('js/admin/jquery.ptTimeSelect.js') }}"></script>
<link rel="stylesheet" href="{{ asset('js/admin/jquery.ptTimeSelect.css') }}">
<?php /*<meta name="csrf-token" content="{{ csrf_token() }}" />*/?>
@stop

@section('content')
<h1>Create New Video</h1>
<div class="well well-lg">

{!! Form::open( array(
    'route' => 'admin_video_post',
    'method' => 'post',
    'id' => 'new_video',
    'files' => true,
) ) !!}

 <div id="_video_type">
{!! Form::label('_video_type', 'Select video type:') !!}<br>
{!! Form::radio( '_video_type', 'normal', false, array('id' => '_video_normal')) !!} Normal |
{!! Form::radio( '_video_type', 'wbtv', false, array('id' => '_video_tv')) !!} WBTV 
</div>

<div id="_video_file" hidden>
{!! Form::label('_video_file', 'Select video file: ') !!}
{!! Form::file( '_video_file',  array('id' => '_file_')) !!}<br>
</div>

<div id="_video_tv_channel"  hidden>
{!! Form::label('_video_tv_channel', 'Channel: ') !!}
<div id="_video_tv_channel_div"></div>
</div>  

<div id="_video_title"  hidden>
{!! Form::label('_video_title', 'Title: ') !!}
{!! Form::text( '_video_title', '', array(
    'class' => 'form-control',
    'placeholder' => 'Title',
) ) !!}
</div>  

<div id="_video_description"  hidden>
{!! Form::label('_video_description', 'Description:') !!} <span> (optional)</span><br>
{!! Form::textarea( '_video_description', '', array(
    'class' => 'form-control',
    'placeholder' => 'Description',
) ) !!}
</div>
<div id="_video_tags"  hidden>
{!! Form::label('_video_tags', 'Tags: ') !!}<span> (*separate with comma ",") (optional)</span>
{!! Form::text( '_video_tags', '', array(
    'class' => 'form-control',
    'placeholder' => 'Tags',
) ) !!}
</div>  

<div id="_video_creator"  hidden>
{!! Form::label('_video_creator', 'Video owner:') !!}
{!! Form::radio( '_video_creator', 'current', true, array('id' => '_video_creator_wb')) !!} {{ session('username') }} |
{!! Form::radio( '_video_creator', 'other', false, array('id' => '_video_creator_other')) !!} Other 
{!! Form::text( '_video_creator_txt', '', array(
    'class' => 'form-control',
    'placeholder' => 'Username',
    'id' => '_video_creator_username',
    'hidden' => true,
    'autocomplete' => 'off',
) ) !!}
{!! Form::text('_other_user_id','0',array('class' => 'form-control', 'hidden' => true,'id' => '_other_user_id' )) !!}
<div id="_video_creator_username_list"></div>
</div>

<div id="_video_country"  hidden>
{!! Form::label('_video_country', 'Country:') !!}
{!! Form::radio( '_video_country', 'default', true) !!} Default |
{!! Form::radio( '_video_country', 'FI', false) !!} Finland |
{!! Form::radio( '_video_country', 'EE', false) !!} Estonia |
{!! Form::radio( '_video_country', 'ZZ', false) !!} Global 
</div>

<div id="_video_start_timedate" hidden>
{!! Form::label('_video_start_timedate', 'Publish time/date:') !!}
{!! Form::checkbox( '_video_start_timedate', 'now', true, array('id' => '_video_publish')) !!} Now <br>
{!! Form::text('_video_start_time', '', array(
    'id' => 'timepicker',
    'class' => 'tinybox',
    'placeholder' => 'Click to set time',
    'hidden' => true,
)) !!} 
{!! Form::text('_video_start_date', '', array(
    'id' => 'datepicker',
    'class' => 'tinybox',
    'placeholder' => 'Click to set date',
    'hidden' => true,
)) !!} 
{!! Form::text('_parsed_time','',array('hidden' => true,'id' => '_parsed_time' )) !!}

</div>


{!! Form::text('_form_errors','',array('hidden' => true,'id' => '_form_errors' )) !!}
<div id="_rsp_msg"></div>
<br>
{!! Form::submit( 'Create video', array(
    'id' => '_video_create',
    'class' => 'btn-default btn',
    'hidden' => true,
) ) !!}
 
{!! Form::close() !!}
<br>
<button id="_video_reload" class="btn-default" hidden>Create another</button>
<br>
</div>
<script>
        $( "#datepicker" ).datepicker( { dateFormat:"d MM, yy",firstDay: 1} );
        $( '#timepicker' ).ptTimeSelect();
</script>

@stop