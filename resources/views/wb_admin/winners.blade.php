@extends('admin_base')

@section('extra_js')
<script src="{{ asset('js/admin/jquery-ui.custom/jquery-ui.js') }}"></script>
<link rel="stylesheet" href="{{ asset('js/admin/jquery-ui.custom/jquery-ui.css') }}">
<script src=<?php echo asset('js/admin/winners.js'); echo "?rand=".mt_rand();?>></script>
@stop


@section('content')
<h1>Mission winners</h1>
<div class="tiny well well-lg">
    <div id="_mission_rsp_msg"></div>

    {!! Form::open( array(
        'route' => 'admin_winners_post',
        'method' => 'post',
        'id' => '_get_winners',
    ) ) !!}

     <div id="_winner_type">
    {!! Form::label('_mission_type', 'Select mission type:') !!}
    {!! Form::radio( '_mission_type', 'normal', true, array('id' => '_mission_normal')) !!} Normal |
    {!! Form::radio( '_mission_type', 'icon', false, array('id' => '_mission_icon')) !!} Icon
    </div>
    <hr>
    <div id="_mission_country">
    {!! Form::label('_mission_country', 'Country:') !!}
    {!! Form::radio( '_mission_country', 'FI', true) !!} Finland |
    {!! Form::radio( '_mission_country', 'EE', false) !!} Estonia |
    {!! Form::radio( '_mission_country', 'ZZ', false) !!} Global 
    </div>
    <hr>
    <div id="_number_of_missions" class="styled-select">
    {!! Form::label('_number_of_missions', 'Max number of missions to print out:') !!}
    <?php
        $thelist = array();
        for ($i=1; $i < 100; $i++) { 
            $thelist[$i] = $i;
        }
        echo Form::select('_number_of_missions',$thelist,'7');
    ?>
    <label>&nbsp-&nbsp</label>
    {!! Form::label('_number_of_entries', 'Number of entries / mission:') !!}
    <?php
        $thelist = array('default' => 'default', 'all' => 'all');
        for ($i=1; $i < 50; $i++) { 
            $thelist[$i] = $i;
        }
        echo Form::select('_number_of_entries',$thelist,'default');
    ?> 
    <span class="tiny">*(default = normal:1, icon:10)</span>
    </div>
    <hr>

    <div id="_checkbox">
    {!! Form::label('_download_links', 'Create download links:') !!}
    {!! Form::checkbox('_download_links', 1,false) !!}
    {!! Form::label('_download_links', '"Icon friendly output":') !!}
    {!! Form::checkbox('_icon_friendly', 1,false) !!}
    {!! Form::label('_download_links', '"Mail friendly output":') !!}
    {!! Form::checkbox('_mail_friendly', 1,false) !!}
    </div>
    <br>
    {!! Form::submit( 'Get winners', array(
        'id' => '_mission_create',
        'class' => 'btn-default btn',
    ) ) !!}
     
    {!! Form::close() !!}
<div id="_content" hidden></div>
</div>

<div id="dialog" title="Mission info"></div>


<div id="banana_pop_up" title="Give bananas">
    {!! Form::open( array(
        'route' => 'admin_bananas_post',
        'method' => 'post',
        'id' => '_give_bananas',
    ) ) !!}

    {!! Form::label('_number_of_bananas', 'Bananas:') !!}
    {!! Form::number('_number_of_bananas') !!}

    <div id="_banana_form_id" hidden></div><br><br>

    {!! Form::submit( 'Give', array(
        'id' => '_bananas_give',
        'class' => 'btn-primary',
    ) ) !!}

    {!! Form::close() !!}
</div>

<div id="email_pop_up" title="Send mail" hidden>
    {!! Form::open( array(
        'route' => 'admin_mail_post',
        'method' => 'post',
        'id' => '_send_mail',
    ) ) !!}

    {!! Form::label('_mail_subject', 'Subject:') !!}
    <?php echo Form::text('_mail_subject') ?>
    {!! Form::label('_mail_message', 'Message:') !!}
    <?php echo Form::textarea('_mail_message') ?>
    <div id="_mail_message_local" hidden></div>
    <div id="_mail_form_id" hidden></div><br><br>

    {!! Form::submit( 'Send', array(
        'id' => '_mail_send',
        'class' => 'btn-default btn',
    ) ) !!}

    {!! Form::close() !!}
</div>


<script>
$( "#dialog" ).dialog({ autoOpen: false, width: 600 });
$( "#banana_pop_up" ).dialog({ autoOpen: false, width: 600 });
$( "#email_pop_up" ).dialog({ autoOpen: false, width: 800 });
</script>

@stop