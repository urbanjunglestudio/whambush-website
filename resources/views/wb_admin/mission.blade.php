@extends('admin_base')

@section('extra_js')
<script src="{{ asset('js/admin/jquery-ui.custom/jquery-ui.js') }}"></script>
<link rel="stylesheet" href="{{ asset('js/admin/jquery-ui.custom/jquery-ui.css') }}">
<script src="{{ asset('js/admin/upload.js') }}"></script>
<script src="{{ asset('js/admin/mission.js') }}"></script>
@stop

@section('content')
<h1>Create New Mission</h1>

<div class="well well-lg">


{!! Form::open( array(
    'route' => 'admin_mission_create',
    'method' => 'post',
    'id' => 'new_mission',
    'files' => true,
) ) !!}

 <div id="_mission_type">
{!! Form::label('_mission_type', 'Select mission type:') !!}<br>
{!! Form::radio( '_mission_type', 'normal', false, array('id' => '_mission_normal')) !!} Normal <br>
{!! Form::radio( '_mission_type', 'icon', false, array('id' => '_mission_icon')) !!} Icon <br>
</div>

<div id="_mission_video_file" hidden>
{!! Form::label('_video_file', 'Select video file: ') !!} <div class="small">*Video will have same title and details as the mission</div>
{!! Form::file( '_video_file',  array('id' => '_file_')) !!}<br>
{!! Form::text('_mission_video_id','0',array('hidden' => true,'id' => '_mission_video_id' )) !!}
</div>

<div id="_mission_icon_name" hidden>
{!! Form::label('_mission_icon_name', 'Icon name:') !!}<br>
{!! Form::text( '_mission_icon_name', '', array(
    'placeholder' => 'Name',
) ) !!}
</div>  

<div id="_mission_title" hidden>
{!! Form::label('_mission_title', 'Title:') !!}<br>
{!! Form::text( '_mission_title', '', array(
    'placeholder' => 'Title',
) ) !!}
</div>  

<div id="_mission_description" hidden>
{!! Form::label('_mission_description', 'Description:') !!}<br>
{!! Form::textarea( '_mission_description', '', array(
    'placeholder' => 'Description',
) ) !!}
</div>

<div id="_mission_creator" hidden>
{!! Form::label('_mission_creator', 'User:') !!}
{!! Form::radio( '_mission_creator', '1', true, array('id' => '_mission_creator_wb')) !!} whambush |
{!! Form::radio( '_mission_creator', 'other', false, array('id' => '_mission_creator_other')) !!} Other 
{!! Form::text( '_mission_creator_txt', '', array(
    'placeholder' => 'Username',
    'id' => '_mission_creator_username',
    'hidden' => true,
    'autocomplete' => 'off',
) ) !!}
{!! Form::text('_other_user_id','0',array('hidden' => true,'id' => '_other_user_id' )) !!}
<div id="_mission_creator_username_list"></div>
</div>

<div id="_mission_country" hidden>
{!! Form::label('_mission_country', 'Country:') !!}
{!! Form::radio( '_mission_country', 'FI', false) !!} Finland |
{!! Form::radio( '_mission_country', 'EE', false) !!} Estonia |
{!! Form::radio( '_mission_country', 'ZZ', false) !!} Global 
</div>

<div id="_mission_start_date" hidden>
{!! Form::label('_mission_start_date', 'Start date:') !!}
{!! Form::text('_mission_start_date', '', array(
    'id' => 'datepicker',
    'placeholder' => 'Click to set date',
)) !!} 
</div>

<div id="_mission_duration" hidden>
{!! Form::label('_mission_duration_radio', 'Duration (days):') !!}
{!! Form::radio( '_mission_duration', '1', false) !!} 1 |
{!! Form::radio( '_mission_duration', '2', false) !!} 2 |
{!! Form::radio( '_mission_duration', '7', true) !!} 7 |
{!! Form::radio( '_mission_duration', '14', false) !!} 14 
</div>

{!! Form::text('_form_errors','',array('hidden' => true,'id' => '_form_errors' )) !!}
<div id="_rsp_msg"></div>
<br>
{!! Form::submit( 'Create mission', array(
    'id' => '_mission_create',
    'class' => 'btn-default btn',
    'hidden' => true
) ) !!}
 
{!! Form::close() !!}
<br>
<button id="_mission_reload" class="btn-default" hidden>Create another</button>
<br>
</div>
<script>
    $(function() {
        $( "#datepicker" ).datepicker( {dateFormat:"DD, d MM, yy",firstDay: 1, minDate: new Date()} );
    });
</script>

@stop