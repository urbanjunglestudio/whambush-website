@extends('appbase')


@section('extra_bottom')
<!--<script src="{{ asset('js/mission.js') }}"></script>-->
@stop


@section('page_title')
<title>{{ $mission_name }} - {{ strtolower(trans('rules.PAGE_TITLE')) }} - Whambush</title>
@stop

@section('content')
<!-- Page Content -->
<div id="page-content-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="single-mission-header">
                    <div class="container-fluid">
                        <div class="row rules" style="margin-left:0px;margin-right:0px;">
<?php echo $content; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>  
</div>
<!-- /#page-content-wrapper -->

@stop
