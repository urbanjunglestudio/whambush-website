@extends('newbase')

@section('extra_meta')

    <meta property="og:title" content="{{ $mission['name'] }}"/>
    <meta property="og:description" content="{{ $mission['description'] }}" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="{{ Request::url() }}" />
    <meta property="og:image" content="{{ $mission_image }}?share" />
    <meta property="og:image:secure_url" content="{{ $mission_image }}?share" />
    <meta property="og:image:width" content="1138" />
    <meta property="og:image:height" content="640" />    
    
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@whambush">
    <meta name="twitter:creator" content="@whambush">
    <meta name="twitter:title" content="{{ $mission['name'] }}">
    <meta name="twitter:description" content="{{ $mission['description'] }}">
    <meta name="twitter:image" content="{{ $mission_image }}?share">
    
    <meta name="apple-itunes-app" content="app-id=876370933, app-argument=whambush://?mission={{ $mission['id'] }}">   
@stop

@section('extra_bottom')
<script src="/js/noframework.waypoints.min.js"></script>
<script src="/js/browser-deeplink.js" type="text/javascript"></script>
<script src="/js/mission.js"></script>
<script src="/js/app.js"></script>
@stop

@section('extra_top')
   <link rel="stylesheet" type="text/css" href="//releases.flowplayer.org/5.5.2/skin/minimalist.css">
   <style>
   .is-splash.flowplayer .fp-ui,.is-paused.flowplayer .fp-ui { background:url("{{ asset('img/play_button.png') }}") center no-repeat; }
   .flowplayer .fp-progress { background-color: rgba(172, 194, 52, 1)}
   </style>
   <script src="//releases.flowplayer.org/5.5.2/commercial/flowplayer.min.js"></script>
@stop

@section('page_title')
<title>{{ $mission_title }} by {{ $mission['added_by']['username'] }} @Whambush</title>
@stop

@section('content')
<!-- Page Content -->
<div id="page-content-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="single-mission-header">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-6">
                    <?php
                        if ($video != false && $video['is_processed']) {
                            echo '
                                <div class="videoWrapper">
                                    <div class="flowplayer no-time no-volume" data-embed="false" data-key="$366401312122019" data-logo="//whambush.com/img/favicon.png" data-analytics="'.Config::get('wb.GA_id').'">
                                        <video poster='.$mission_image.'>
                                            <source type="video/mp4" src='.$video['external_url'].'>
                                        </video>
                                    </div>
                                </div>           
                                        ';

                        
                        } else {
                                        echo '
                                <img class="img-responsive" src="'.$mission_image.'">
                                            ';
                        }
                    ?>  

                        <div class="get-app">
                            <a href="#get-app" data-toggle="modal" data-target="#get-app">
                                {{ trans('mission.mission_enter_help') }} »
                            </a>
                        </div>
                        <div class="video-bar">
                            <div class="user-info">
                                <a href=<?php echo LaravelLocalization::getLocalizedURL(null,'/u/'.$mission['added_by']['username']); ?>><img src={{ $added_by_profile_picture }}>
                                <span class="user-name">{{ $mission['added_by']['username'] }}</a></span>
                            </div>
                        </div>
                                <ul class="mission-specs"> 
                                    <li><span class='icon-play'></span> {{ $number_of_videos }}</li>
                                    <li><span class={{ $banana_css }}></span> {{ abs($banana) }}</li>
                                    <li id="timerrow"><div id="mission_ends_at" value={{ $mission_end_time }} local="{!! trans('common.mission_ends') !!}" notsoon="{{ trans('common.not_soon') }}"></div></li>
                                    <li>
                                        <?php
                                            $url = parse_url(asset(''), PHP_URL_HOST)."/m/".$slug;
                                        ?>
                                        <a href="#" id="applink" url={{ $url }} hidden>{!! trans('common.open_in_app') !!}</a>
                                    </li>
                                </ul>               
                            </div>
                            <div class="col-sm-6 single_mission_bg" <?php /*style="background-image: url('{{ $mission['mission_image_2']}}');"*/ ?>>
                                <div class="single-mission-info">
                                    <h1><SMALL>{{ $mission_title }}</SMALL></h1>
                                    <p>{!! $mission_description !!}</p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row videogrid">
            <div id="mission_videos" missionid={{ $mission['id'] }} value={{ $number_of_videos }}>
                <?php 
                if ($number_of_videos === 0) {
                    echo "<center><h2>".trans('common.notyetvideos')."</h2></center>";
                }
                ?>
            </div>
        </div>
        
        <div class="waypoint" id="load_more_block">
        <?php 
        if ($number_of_videos > 0) {
            echo '    
            <div id="spinner">
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
            </div>';
        }
        ?>
        </div>
    </div>

 
</div>
<!-- /#page-content-wrapper -->

</div>

<!-- Get app modal-->
<div class="modal fade" id="get-app">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">{{ trans('mission.mission_enter_help') }}</h4>
      </div>
      <div class="modal-body">
        <legend class="success">{{ trans('mission.mission_help_label1') }}</legend>
        <p>
            <p>{{ trans('mission.mission_help_text1') }}</p>
            <a href="https://whambush.com/ios" class="btn btn-default">App Store</a>
            <a href="https://whambush.com/android" class="btn btn-default">Google Play Store</a>

        </p>
        <br>
        <legend class="success">{{ trans('mission.mission_help_label2') }}</legend>
        <p>{{ trans('mission.mission_help_text2') }}</p>
        <br>

        <legend class="success">{{ trans('mission.mission_help_label3') }}</legend>
        <p>{{ trans('mission.mission_help_text3') }}</p>
        <br>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('mission.mission_help_button') }}</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@stop
