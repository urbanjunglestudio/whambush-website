@extends('newbase')

@section('content')

<div class="container content">
    <h1>Terms of Service</h1>
    <ol>
        <li>Community guidelines
            <ol>
                <li>Your use of the Whambush website (Site) and any Whambush products, channels, software, applications, data feeds and services, including the Whambush video player (WB Player) and application (WB App) provided to you on or from or through the Site (collectively the Service) is subject to the terms of a legal agreement between you and Whambush.</li>
            </ol>
        </li>
        <li>Your legal agreement with Whambush is made up of the terms and conditions set out in this document, Whambush's Privacy Policy and Whambush's Community guidelines (all collectively called the Terms).</li>
        <li>The Terms from a legally binding agreement between you and Whambush in relation to your use of the Service. Please read them carefully.</li>
        <li>The Terms apply to all those who use the Service, including contributors of Content, on the Service. Content includes without limitation text, software, graphics, photos, sounds, music, videos, AV combinations and other materials you might view, access or contribute on the Service.</li>
        <li>Accepting the terms
            <ol>
                <li>In order to use the Service, you must first agree to the Terms. You may not use the Service without accepting the Terms.</li>
                <li>You accept the terms simply by using the Service. You understand and agree the Whambush will treat your use of the Service as acceptance of the Terms from that point onwards.</li>
                <li>You may not use the Service and may not accept the Terms if (1) you are not of legal age to form a binding contract with Whambush or (2) you are a person who is either barred or otherwise legally prohibited from receiving or using the Service under the laws of the country you reside in or access and use the Service.</li>
                <li>You should print out a copy of the Terms for your records.</li>
            </ol>
        </li>
        <li>Changes: Whambush reserves the right to change the Terms from time to time at its sole discretion. Therefore you must read the Terms regularly to check any such changes to the Terms. If you do not agree to the changes you must stop using the Service. Your continued use of the Service after changes have been made to them with constitute your acceptance of the modified Terms.</li>
        <li>Accounts: in order to access some features of the Service you will have to register a Whambush account. You should keep your Whambush account password secure and confidential.
            <ol>
                <li>You must inform Whambush immediately of any unauthorized use of your account immediately.</li>
                <li>You agree that you are solely responsible (to Whambush and others) for all activity that occurs under your Whambush account.</li>
            </ol>
        </li>
        <li>General restrictions of usage: Whambush grants you permission to access and use the Service, including without limitation the following restrictions herein. You agree that your failure to adhere to these conditions shall constitute a breach of these Terms on your part:
            <ol>
                <li>You agree not to distribute any part or parts of the Site or Service, including without limitation any Content, without Whambush's prior written authorization, ￼unless made available through functionality offered by Service (example Whambush's video player);</li>
                <li>You agree not to alter or modify any part of the Site or Service;</li>
                <li>You agree not to access Content through any technology other than the Whambush video player;</li>
                <li>You agree not to circumvent or disable any security features of the Service;</li>
                <li>You agree not to use Service for commercial uses;</li>
                <li>Prohibited commercial uses do not include uploading an original video onto the Service;</li>
                <li>You agree not to launch any automated system (including spam uses) that accesses the Service or its users via Service;</li>
                <li>You agree not to harvest any personal data of any user of the Site or Service;</li>
                <li>You agree not to use the Site or Service in relation to solicitation of a commercial system towards the Service or Site;</li>
                <li>You agree not to solicit, for commercial purposes, any users of the Site or Service;</li>
                <li>You agree not to access the Content for any reason other than your own non-commercial use solely intended through the normal functionality of the Service, and solely for streaming. Streaming means a contemporaneous digital transmission of the material by Whambush via the internet to a user operated internet enabled device in such a manner that the data is intended for real time viewing and not intended to be copied and downloaded for later use and redistribution.</li>
                <li>You shall not copy, reproduce, distribute, transmit, broadcast, display, sell, license or otherwise exploit any Content without the prior written consent of Whambush or the respective licensors of the Content.</li>
            </ol>
        </li>
        <li>Copyrights: Whambush policy is to respect the copyrights of others by not using any unauthorized material, Content, pictures, including without limitation any actions or materials that infringes on an existing copyright. Whambush reserves the right to suspend or terminate the account(s) of any copyright offender(s).</li>
        <li>Confidentiality: Whambush makes no guarantee of confidentiality with content. </li>
        <li>By uploading content on Whambush, you give Whambush a license to use and display the Content.</li>
        <li>You understand that you are solely responsible for your own content and the consequences of uploading it. Whambush does not endorse any Content or opinion or advice expressed therein, and Whambush expressly disclaims any and all liability in connection with any Content.</li>
        <li>You represent and warrant that you have and will have while Service is being utilized all necessary licenses, rights, consents and permissions to use your Content for the purposes of the Service, and as contemplated by these Terms.</li>
        <li>You agree not to upload any Content containing material that might be unlawful.</li>
        <li>You agree that Content you upload to Service will not contain any third party copyright material, or material that is subject to other third party proprietary right (including privacy or publicity), unless you have a formal license or permission from the rightful owner(s), or you are otherwise legally entitled to upload the material in question and grant Whambush a license as stated above.</li>
        <li>￼You understand that while using Service, you may be exposed to Content that is factually inaccurate, offensive, indecent or otherwise objectionable to you. You agree to waive, and you do hereby waive, any legal or equitable rights or remedies you have or may have against Whambush.</li>
        <li>Uploading Content means you grant Whambush a license: every time you upload Content to Service, you grant (1) to Whambush a perpetual, worldwide, non- exclusive, royalty-free, transferable license (with right to sub-license) to use, reproduce, distribute, prepare derivative works of, display and perform that Content in connection with the provision of Service and Whambush's business, including without limitation for promoting and distributing part or all of the Service (and derivative works thereof) in any media formats and through any media channel; and (2) to each user of the Service a similar license to access your Content to the extent permitted by the functionality of the Service.</li>
        <li>With the exception of Content you own and Upload, all Content on Whambush is subject to copyright, trademark rights and other intellectual property rights of Whambush and Whambush's licensors. Such Content may not be copied, downloaded, reproduced, broadcast, transmitted, sold, or otherwise exploited with the prior written consent of Whambush and it's licensors. Whambush and its licensors reserve all rights not expressly granted in and to their Content. Notwithstanding the following, you hereby grant all necessary rights and royalty-free licenses and otherwise acknowledge and agree that your Content (whether in part or in whole) may be posted, displayed and shown on the Whambush App, Whambush Player, Whambush website and service and on third party sites and services, including but not limited to television (broadcast and through the internet) with no payments or otherwise remuneration due to You from Whambush or any other third party.</li>
        <li>Links from Whambush: Service may include hyperlinks to other websites that are not owned or controlled by Whambush. Whambush assumes no responsibility for the content, practices and privacy policies of any third party websites.
            <ol>
                <li>You acknowledge and agree that Whambush is not responsible for any third party websites. Whambush does not endorse any advertising, products or other materials from other websites.</li>
                <li>You acknowledge and agree Whambush is not liable for any loss or damage you may incur as the result of availability of external websites or resources.</li>
            </ol>
        </li>
        <li>Ending your relationship with Whambush: the terms herein will apply until terminated by either you or Whambush as set out herein.
            <ol>
                <li>If you want to terminate your legal relationship with Whambush, you may do so by (1) notifying Whambush at any time and (2) closing your Whambush account. Your notice should be sent in writing to Whambush's address which is set out at the beginning of these terms.</li>
                <li>Whambush may terminate its legal agreement with you at any time if:
                    <ol>
                        <li>You have breached any provisions of the Terms (or have acted in a manner which Whambush determines as compliance with the Terms; or</li>
                        <li>Whambush is required to do so by law; or</li>
                        <li>Whambush no longer provides Service to the country where you are resident in; or</li>
                        <li>the provision of Service is no longer commercially viable in Whambush's discretion.</li>
                    </ol>
                </li>
                <li>When these Terms come to an end, all of the legal rights, obligations and liabilities between You and Whambush will be deemed terminated.</li>
            </ol>
        </li>
    </ol>
</div>
<br><br>
<br><br>
@stop
