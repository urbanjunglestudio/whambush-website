@extends('newbase')

@section('content')

        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <?php
                            $sections = 1;
                            while(strcmp(trans('faq.SECTION_'.$sections.'_TITLE'), 'faq.SECTION_'.$sections.'_TITLE') != 0) {
                                echo PHP_EOL.'                        <div class="faqbox" data-toggle="modal" data-target="#SECTION_'.$sections.'">
                            <span class="'.trans('faq.SECTION_'.$sections.'_ICON').'"></span>
                            <h3>'.trans('faq.SECTION_'.$sections.'_TITLE').'</h3>
                            <a href="#" class="btn btn-default">'.trans('common.open').'</a>
                        </div>'.PHP_EOL;
                                $sections++;
                            }

                        ?>
                    </div>
                 </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->


<?php
    for ($i = 1; $i < $sections; $i++) {
        echo PHP_EOL.'        <!-- Modals -->
    <div class="modal fade" id="SECTION_'.$i.'" tabindex="-1" role="dialog" aria-labelledby="faquser" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>'.PHP_EOL;
        $subsec = 1;
        while(strcmp(trans('faq.SECTION_'.$i.'_'.$subsec), 'faq.SECTION_'.$i.'_'.$subsec) != 0) {
            echo '            <h3>'.trans('faq.SECTION_'.$i.'_'.$subsec).'</h3>'.PHP_EOL;
            $subsubsec = 1;
            while(strcmp(trans('faq.SECTION_'.$i.'_'.$subsec.'_'.$subsubsec), 'faq.SECTION_'.$i.'_'.$subsec.'_'.$subsubsec) != 0) {
                echo '            <p>'.trans('faq.SECTION_'.$i.'_'.$subsec.'_'.$subsubsec).'</p>'.PHP_EOL;
                $subsubsec++;
            }
            $subsec++;
         } 
          echo '          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">'.trans('common.close').'</button>
          </div>
        </div>
      </div>
    </div>'.PHP_EOL;
    }
?>

@stop
