@extends('page')

@section('extra_bottom')
<script src="/js/noframework.waypoints.min.js"></script>
<script src="/js/home.js"></script>
@stop

@section('content')


<!-- Start Hero Section
================================================== -->
<section id="hero" class="section active-section">
    <div class="container">
        <div class="row">
            <div class="col-md-5 hidden-xs hidden-sm hidden-md">
                <img src="{{ asset('/img/whambush-app.png') }}">
            </div>
            <div class="col-md-7">
                <div class="lp-element animated fadeInUp">
                    <h1 itemscope itemprop="name" style="margin-top:80px;"><?php echo trans('home.gowild'); ?></h1>
                    <p class="lead" itemscope itemprop="description"><?php echo trans('home.submit'); ?></p>
                    <p><a href="<?php echo trans('home.ios_link'); ?>" class="btn btn-default btn-lg" role="button">App Store</a>  <a style="margin-left:7px;" class="btn btn-default btn-lg" role="button" href="<?php echo trans('home.android_link'); ?>">Google Play Store</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==================================================
End Hero -->
<a href="{{ asset('/m') }}" style="color : white">
<section id="homemissions" class="section" style="max-height: 256px; overflow: hidden; gradient(to top, rgba(1, 1, 1, 1) , rgba(0, 0, 0, 0)); ">
    <div id='spinner'>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
    </div>
</section>
</a>
<!-- Start Features
================================================== -->
<section id="features" class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-4">
                <div class="service-block">
                    <a href="/m"><h3><?php echo trans('home.mission_title'); ?></h3>
                    <p style="color: white;"><?php echo trans('home.mission_txt'); ?></p></a>
                </div>
            </div>
            <div class="col-md-4 col-sm-4">
                <div class="service-block">
                    <h3><?php echo trans('home.prize_title'); ?></h3>
                    <p><?php echo trans('home.prize_txt'); ?></p>
                </div>
            </div>
            <div class="col-md-4 col-sm-4">
                <div class="service-block">
                    <h3><?php echo trans('home.wbtv_title'); ?></h3>
                    <p><?php echo trans('home.wbtv_txt'); ?></p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==================================================
End Features -->

<section id="footermain" class="section_footer">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-4">
                <legend><?php echo trans('home.tweet'); ?></legend>
                <a class="twitter-timeline" href="https://twitter.com/whambush" data-widget-id="<?php echo trans('common.tw_widget_id'); ?>" data-tweet-limit="1" data-theme="dark" data-chrome="noheader transparent">Tweets by @whambush</a>
                <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
            </div>

            <div class="col-md-4 col-sm-4">
                <legend><?php echo trans('home.follow'); ?></legend>
                <ul class="sosialicons">
                    @foreach ([
                        'facebook' => trans('home.facebook_link'),
                        'twitter' => trans('home.twitter_link'),
                        'youtube' => trans('home.youtube_link'),
                        'instagram' => trans('home.instagram_link')
                    ] as $service => $link)
                    <li>
                        <a href="{{ $link }}" class="{{ $service }}_link" target="_blank">
                            <span><img src="{{ asset("/img/{$service}.png") }}" style="margin-top:-20px;" width="48" height="48"></span>
                        </a>
                    </li>
                    @endforeach
                </ul>
                <br>
            </div>

            <div class="col-md-4 col-sm-4">
                <legend><?php echo trans('home.download'); ?></legend>
                <div class="row">
                    <div class="col-xs-6"><a href="<?php echo trans('home.ios_link'); ?>"><img class="img-responsive" src="{{ asset('/img/appstore.png') }}"></a></div>
                    <div class="col-xs-6"><a href="<?php echo trans('home.android_link'); ?>"><img class="img-responsive" src="{{ asset('/img/playstore.png') }}"></a></div>
                </div>
            </div>

        </div>
    </div>
</section>

@stop
