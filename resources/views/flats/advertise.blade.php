@extends('newbase')

@section('content')

        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-7">
                        <h1>Create a video competiton for your brand</h1>
                    </div>
                    <div class="col-md-5">
                        <div class="well well-lg">
                            <h3>Kickstart you video competition</h3>
                            <form>
                                  <div class="form-group">
                                    <label >Company</label>
                                    <input type="text" class="form-control" placeholder="Company">
                                  </div>
                                  <div class="form-group">
                                    <label >Mission title</label>
                                    <input type="text" class="form-control" placeholder="Mission title">
                                  </div>
                                  <div class="form-group">
                                    <label >Mission description</label>
                                    <textarea type="text" class="form-control" placeholder="Mission description"></textarea>
                                  </div>
                                  <div class="form-group">
                                    <label >Start date</label>
                                    <input type="text" class="form-control" placeholder="Start date">
                                  </div>
                                  <div class="form-group">
                                    <label >Lenght (days)</label>
                                    <input type="text" class="form-control" placeholder="Start date">
                                  </div>
                                  <div class="form-group">
                                    <button class="btn btn-default">Submit</button>
                                  </div>
                            </form>
                        </div>
                    </div>
                 </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->


@stop
