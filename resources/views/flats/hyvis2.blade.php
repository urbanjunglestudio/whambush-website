<!DOCTYPE html>
<html lang="fi">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=0">
    <title>Vantaan Hyvis</title>
   <meta name="description" content="Hyviksen eli Vantaan lasten ja nuorten hyvinvointisuunnitelman tämän vuoden teema on hyvän ja huolen puheeksi otto. Haastamme lapset sekä nuoret mukaan jakamaan hyvää osallistumalla">
    <meta name="author" content="Urban Jungle Studios Oy">
    <meta property="og:title" content="Whambush" />
    <meta property="og:type" content="article" />
    <meta property="og:image" content=asset('/img/hyvis/fb.png') />
    <meta property="og:description" content="Hyviksen eli Vantaan lasten ja nuorten hyvinvointisuunnitelman tämän vuoden teema on hyvän ja huolen puheeksi otto. Haastamme lapset sekä nuoret mukaan jakamaan hyvää osallistumalla" />
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/hyvis.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    {!! env('HEAD_SCRIPT', '') !!}
<?php
    if (App::environment() == 'prod') {
        echo "
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','//connect.facebook.net/en_US/fbevents.js');
fbq('init', '165473937134470');
fbq('track', \"PageView\");
</script>
<noscript><img height=\"1\" width=\"1\" style=\"display:none\" src=\"https://www.facebook.com/tr?id=165473937134470&ev=PageView&noscript=1\"/></noscript>
<!-- End Facebook Pixel Code -->
       ";
    };
?>

  </head>
  <body>
  <section class="hero hero-hyvis2">

    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <a href="https://whambush.com/fi"><img src="img/hyvis/wb-logob.png" style="max-width:170px; height:auto;"></a>
        </div>

        <div class="col-sm-7">
          <h1>Vantaan Hyvis</h1>
          <p class="herotxt">Vantaan lasten ja nuorten hyvinvointisuunnitelma eli HYVIS haluaa muistuttaa kaikkia Lapsen oikeuksista. Whambushin kanssa toteutettavassa kampanjassa nostetaan esille lasten oikeuksia ja muita lapsille ja nuorille tärkeitä asioita. Katso haaste, tee oma video ja osallistu kampanjaan!
<br/><a href="http://www.vantaanhyvis.fi" class="btn btn-primary" target="_blanc">www.vantaanhyvis.fi</a></p>
          <h2 style="margin-top:0px;">Pistä hyvä kiertämään ja tee oma Hyvis-video!</h2>
          <p class="herotxt">YK:n lapsen oikeuksien sopimus (LOS) on lapsia koskeva ihmisoikeussopimus. Sopimuksen tärkein tavoite on perusoikeuksien: terveyden, koulutuksen, tasa-arvon ja turvan takaaminen kaikille lapsille. Lapsen oikeuksien päivää vietetään joka vuosi 20. marraskuuta.
          <br/><a href="http://www.pelastakaalapset.fi/jarjesto/lapsen-oikeudet/" class="btn btn-primary" target="_blanc">Pelastakaa lapset</a>
          <a href="http://www.unicef.fi/lapsen-oikeudet/" class="btn btn-primary" target="_blanc">Unicef</a>
          <br></p>
           <h2>Osallistu</h2>
          <p>
          Tutustu alla oleviin viikkotehtäviin, lataa <b><a href="https://whambush.com/fi">Whambush</a></b>-sovellus puhelimeesi ja osallistu sovelluksella* Hyvis-kampanjaan omalla videollasi! 
          Videoita voi käydä tykkäämässä sovelluksella ja parhaat videot palkitaan viikoittain upealla Hyvis-palkintosetillä. Palkintosetissä on mm. <b><a href="http://blackeyemarket.com" target="_blanc">Black Eye</a></b> linssi kännykkään, <b><a href="http://www.superpark.fi" target="_blanc">Superpark</a></b>-pääsylippu, kaksi <b><a href="http://kotipizza.fi" target="_blanc">Kotipizzan</a></b> lahjakorttia, <b><a href="http://www.brainblasterz.com">Brain Blasterz</a></b>-karkkia sekä Finnkinon leffalippuja plus yllätyksiä! 
          Lisäksi kampanjan lopussa arvotaan kaikkien osallistuneiden kesken yllätyspalkinto!<br>Seuraa Whambushin <a href="https://facebook.com/Whambush" target="_blanc">facebook</a>- ja <a href="https://instagram.com/Whambush" target="_blanc">instagram</a>tilejä niin saat tietää viimeisimmät uutiset kampanjasta.
          </p>

          <a href="https://whambush.com/android" class="btn btn-primary" target="_blanc">Androidille</a> <a href="https://whambush.com/ios" class="btn btn-primary" target="_blanc">Iphonelle</a>
          <br>
          <p style="font-size:12px; margin-top:20px;">*Tehtäviin voi osallistua myös lähettämällä video osoitteeseen <b>hyvis(ät)whambush.com</b></p>
        </div>
        <div class="col-sm-5 visible-lg">
          <img class="wave" src="img/hyvis/hyvis_hand.png">
          <img class="hyvis-guy" src="img/hyvis/hyvis_hahmo.png">
        </div>

      </div>
    </div>

  </section>

   <section class="about" id="about">

    <div class="container">


      <div class="row missiontxt" >

        <div class="col-sm-4">
          <a href="https://whambush.com/v/lapsen-oikeudet/" target="_blanc"><img src="img/hyvis/hyviskuva.png">
          <h2>Lasten Oikeudet<br>16.11.</h2></a>
          <p></p>
        </div>

        <div class="col-sm-4">
          <a href="https://whambush.com/v/hyvis-vapaa-aika"  target="_blanc"><img src="img/hyvis/iaj.png">
          <h2>Terveys<br>23.11.</h2></a>
          <p></p>
        </div>

        <div class="col-sm-4">
          <a href="https://whambush.com/m/hyvis-kavereita" target="_blanc"><img src="img/hyvis/kaitsu.png">
          <h2>Osallistuminen<br>30.11.</h2></a>
          <p>
</p>
        </div>

      </div>
      <div class="row missiontxt">
        <div class="col-sm-2">
        </div>

        <div class="col-sm-4">
          <a href="https://whambush.com/m/hyvis-enkiusaa" target="_blanc"><img src="img/hyvis/superpark.png">
          <h2>Turva<br>7.12.</h2></a>
          <p></p>
        </div>

        <div class="col-sm-4">
          <a href="https://whambush.com/m/parasta-koulussa" target="_blanc"><img src="img/hyvis/dmies.png">
          <h2>Koulutus<br>14.12.</h2></a>
          <p></p>
        </div>

      </div>

            
    </div>

  </section>



  <section class="logos">

    <div class="container">
      <div class="row" >
        <div class="col-sm-12">
          <h2>Menossa mukana</h2>
        </div>
      </div>



<div class="row vcenter" >

        <div class="col-sm-2">
        </div>
  <div class="col-sm-3">
          <a href="https://whambush.com/fi" target="blanc"><img class="img-responsive" src="img/hyvis/wb-logob.jpg"></a>
        </div>
        


<div class="col-sm-3">
          <a href="http://www.vantaanhyvis.fi/" target="blanc"><img class="img-responsive" src="img/hyvis/hyvis-logo.png"></a>
        </div>
        <div class="col-sm-2">
          <a href="http://vantaa.fi" target="blanc"><img class="img-responsive" src="img/hyvis/vantaa-logo.png"></a>
        </div>



      </div>
      <div class="row vcenter" >

        <div class="col-sm-2 ">
        </div>
         <div class="col-sm-1 ">
        </div>
        <div class="col-sm-2">
          <a href="http://kotipizza.fi" target="blanc"><img class="img-responsive" src="img/hyvis/kotipizza-logo.jpg"></a>
        </div>

<div class="col-sm-2">
          <a href="http://www.brainblasterz.com" target="blanc"><img class="img-responsive" src="img/hyvis/bb.png"></a>
        </div>

<div class="col-sm-2">
          <a href="http://www.superpark.fi" target="blanc"><img class="img-responsive" src="img/hyvis/superpark.jpg"></a>
        </div>

<div class="col-sm-2">
        </div>
<div class="col-sm-1">
        </div>

  </div>
       <div class="row">

        <div class="col-sm-4 col-sm-offset-4">
          <div class="col-xs-3">
            <a href="https://www.facebook.com/Whambush" class="fblink" target="_blank">
                                <span><img src="img/hyvis/facebook.png" style="margin-top:70px;" class="img-responsive"></span>
                            </a>
          </div>


          <div class="col-xs-3">
            <a href="https://twitter.com/whambush" class="twlink" target="_blank">
                                <span><img src="img/hyvis/twitter.png" style="margin-top:70px;" class="img-responsive"></span>
                            </a>
          </div>

          <div class="col-xs-3">
            <a href="https://www.youtube.com/user/whambush/videos" class="ytlink" target="_blank">
                                <span><img src="img/hyvis/youtube.png" style="margin-top:70px;" class="img-responsive"></span>
                            </a>
          </div>

          <div class="col-xs-3">
             <a href="https://instagram.com/whambush" class="iglink" target="_blank">
                                <span><img src="img/hyvis/instagram.png" style="margin-top:70px;" class="img-responsive"></span>
                            </a>
          </div>
        </div>
         

        </div>
      </div>
    </div>

    <br><br><a href="/hyvis-k15"  class="btn btn-primary" >Kevään 2015 Hyvis-kampanja</a>
  </section>
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>