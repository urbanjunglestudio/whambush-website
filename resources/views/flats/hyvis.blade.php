<!DOCTYPE html>
<html lang="fi">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=0">
    <title>Whambush + Hyvis</title>
   <meta name="description" content="Hyviksen eli Vantaan lasten ja nuorten hyvinvointisuunnitelman tämän vuoden teema on hyvän ja huolen puheeksi otto. Haastamme lapset sekä nuoret mukaan jakamaan hyvää osallistumalla">
    <meta name="author" content="Urban Jungle Studios Oy">
    <meta property="og:title" content="Whambush" />
    <meta property="og:type" content="article" />
    <meta property="og:image" content=asset('/img/hyvis/fb.png') />
    <meta property="og:description" content="Hyviksen eli Vantaan lasten ja nuorten hyvinvointisuunnitelman tämän vuoden teema on hyvän ja huolen puheeksi otto. Haastamme lapset sekä nuoret mukaan jakamaan hyvää osallistumalla" />
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/hyvis.css" rel="stylesheet">

    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
    
    {!! env('HEAD_SCRIPT', '') !!}

  </head>
  <body>
  <section class="hero">

    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <a href="https://whambush.com"><img src="img/hyvis/wb_logo.png" style="max-width:170px; height:auto;"></a>
        </div>

        <div class="col-sm-7">
          <h1>Whambush + Hyvis</h1>
          <p class="herotxt">Hyviksen eli Vantaan lasten ja nuorten hyvinvointisuunnitelman tämän vuoden teema on hyvän ja huolen puheeksi otto. Haastamme lapset sekä nuoret mukaan jakamaan hyvää osallistumalla <a href="https://whambush.com" target="_blanc">Whambush-mobiilipalvelun</a> kanssa toteutettuun sosiaalisen median kampanjaan.</p>
          <a href="http://www.vantaanhyvis.fi/" class="btn btn-primary" target="_blanc">Vantaanhyvis.fi</a> 
          <h2>Pistä hyvä kiertämään ja tee oma Hyvis-video!</h2>
          <p>Tutustu alla oleviin viikkotehtäviin, lataa Whambush-sovellus ja osallistu Hyvis-kampanjaan omalla videollasi! Videoita voi käydä tykkäämässä ja parhaat videot palkitaan viikoittain upeilla palkinnoilla. Lisäksi kampanjan lopussa arvotaan kaikkien osallistuneiden kesken Jopon 50v juhlamalli! <a href="http://www.jopo.fi" target="_blanc">www.jopo.fi</a><br></p>

          <a href="https://whambush.com/android" class="btn btn-primary" target="_blanc">Androidille</a> <a href="https://whambush.com/ios" class="btn btn-primary" target="_blanc">Iphonelle</a>
          <br>
          <p style="font-size:12px; margin-top:20px;">*Tehtäviin voi osallistua myös lähettämällä video osoitteeseen <b>hyvis(ät)whambush.com</b></p>
        </div>

        <div class="col-sm-5 visible-lg">
          <img class="wave" src="img/hyvis/hyvis_hand.png">
          <img class="hyvis-guy" src="img/hyvis/hyvis_hahmo.png">
        </div>

      </div>
    </div>

  </section>

   <section class="about" id="about">

    <div class="container">


      <div class="row missiontxt" >

        <div class="col-sm-4">
          <img src="img/hyvis/hyviskuva.png">
          <a href="https://whambush.com/v/hyvis-tee-hyvaa/" target="_blanc"><h2>20.4. Tee Hyvää - Hyvis ja Whambush Gorilla</h2></a>
          <p>Ole hyvä toiselle. Esitä ja kuvaa kuinka olemalla hyvä toiselle asiat paranee.</p>
        </div>

        <div class="col-sm-4">
          <img src="img/hyvis/icemfb.png">
          <a href="https://whambush.com/v/hyvis-vappu/" target="_blanc"><h2>27.4. Vappu - Ice Hearts ja My First Band</h2></a>
          <p>Toisen Hyvis-haasteen heittää Mikko My First Band:stä ja Ville Icehearts:sta! Kuvatkaa hauskimmat vappujuhlinnat kavereiden kanssa. </p>
        </div>

        <div class="col-sm-4">
          <img src="img/hyvis/johtaja.png">
          <a href="https://whambush.com/v/hyvis-kiitos-aideille/" target="_blanc"><h2>4.5. Kiitos äideille - vantaan kaupunginjohtaja</h2></a>
          <p>Tehtävänä on kuvata kiitoksesi äideille ja lähettää se meille. Muistakaa, Äitienpäivä on tulevana sunnuntaina 10.5.
</p>
        </div>

      </div>
      <div class="row missiontxt">
        <div class="col-sm-4">
          <img src="img/hyvis/vaikuttaja.png">
          <a href="https://whambush.com/v/hyvis-nuori-vaikuttaja/" target="_blanc"><h2>11.5. Nuori Vaikuttaja</h2></a>
          <p>Tehtävänä on kuvata miten sinun mielestäsi nuorten ääni saadaa kuuluviin.</p>
        </div>

        <div class="col-sm-4">
          <img src="img/hyvis/hymyile.png">
          <a href="https://whambush.com/v/hyvis-hymyile/"><h2>18.5. Hymyile</h2></a>
          <p>Tehtävänä on ihan vaan hymyillä. Kuvaa hymysi videolle ja lähetä se meille.</p>
        </div>

        <div class="col-sm-4">
          <img src="img/hyvis/juno.png">
          <a href="https://whambush.com/v/hyvis-kesaa-kohti/"><h2>25.5. Kesää kohti</h2></a>
          <p>Näytä mitä kaikkea liikuntaa kesällä voi harrastaa, niin sisällä kuin ulkona. Kuvaa video, lähetä ja voita!</p>
        </div>

      </div>

            <div class="row">
        <div class="col-sm-6 col-sm-offset-3">
          <img class="img-responsive" src="img/hyvis/jopo.png">
          <h2>Lisäksi kampanjan lopussa arvotaan kaikkien osallistuneiden kesken Jopon 50v juhlamalli!</h2>
        </div>

      </div>
    </div>

  </section>



  <section class="logos">

    <div class="container">
      <div class="row" >
        <div class="col-sm-12">
          <h2>Menossa mukana</h2>
        </div>
      </div>

      <div class="row vcenter" >

        <div class="col-sm-2 ">
          <a href="http://sendy.soikea.com/karkkiklubi/" target="blanc"><img class="img-responsive" src="img/hyvis/halva1-logo.jpg"></a>
        </div>
        <div class="col-sm-2">
          <a href="http://kotipizza.fi" target="blanc"><img class="img-responsive" src="img/hyvis/kotipizza-logo.jpg"></a>
        </div>

<div class="col-sm-2">
          <a href="http://www.otava.fi/" target="blanc"><img class="img-responsive" src="img/hyvis/otava-logo.jpg"></a>
        </div>

<div class="col-sm-2">
          <a href="https://instagram.com/tuplapatukka/" target="blanc"><img class="img-responsive" src="img/hyvis/tupla-logo.jpg"></a>
        </div>

<div class="col-sm-2">
          <a href="http://www.icehearts.fi/" target="blanc"><img class="img-responsive" src="img/hyvis/icehearts-logo.png"></a>
        </div>

<div class="col-sm-2">
          <a href="http://vantaa.fi" target="blanc"><img class="img-responsive" src="img/hyvis/vantaa-logo.png"></a>
        </div>
  </div>

<div class="row vcenter" >

        <div class="col-sm-2">
          <p> </p>
        </div>
  <div class="col-sm-3">
          <a href="https://whambush.com" target="blanc"><img class="img-responsive" src="img/hyvis/wb-logob.jpg"></a>
        </div>
        

<div class="col-sm-2">
          <a href="http://www.vantaanhyvis.fi/" target="blanc"><img class="img-responsive" src="img/hyvis/hyvis-logo.png"></a>
        </div>

<div class="col-sm-3">
          <a href="http://jopo.fi" target="blanc"><img class="img-responsive" src="img/hyvis/helkama-logo.jpg"></a>
        </div>


      </div>

       <div class="row">

        <div class="col-sm-4 col-sm-offset-4">
          <div class="col-xs-3">
            <a href="https://www.facebook.com/Whambush" class="fblink" target="_blank">
                                <span><img src="img/hyvis/facebook.png" style="margin-top:70px;" class="img-responsive"></span>
                            </a>
          </div>


          <div class="col-xs-3">
            <a href="https://twitter.com/whambush" class="twlink" target="_blank">
                                <span><img src="img/hyvis/twitter.png" style="margin-top:70px;" class="img-responsive"></span>
                            </a>
          </div>

          <div class="col-xs-3">
            <a href="https://www.youtube.com/user/whambush/videos" class="ytlink" target="_blank">
                                <span><img src="img/hyvis/youtube.png" style="margin-top:70px;" class="img-responsive"></span>
                            </a>
          </div>

          <div class="col-xs-3">
             <a href="https://instagram.com/whambush" class="iglink" target="_blank">
                                <span><img src="img/hyvis/instagram.png" style="margin-top:70px;" class="img-responsive"></span>
                            </a>
          </div>
        </div>
         

        </div>
      </div>
    </div>

  </section>
 
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>