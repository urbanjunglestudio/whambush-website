@extends('newbase')

@section('extra_meta')

    <meta property="og:title" content="{{ $video['name'] }}"/>
    <meta property="og:description" content="{{ $video['description'] }}" />
    <meta property="og:url" content="{{ Request::url() }}" />
    <meta property="og:image" content="{{ Request::url() }}/image.jpg" />
    <meta property="og:image:secure_url" content="{{ str_replace('http:', 'https:', Request::url().'/image.jpg') }}" />
    <meta property="og:image" content="https://whambush.com/img/fb.jpg" />
    <meta property="og:image:secure_url" content="https://whambush.com/img/fb.jpg" />
    
    <meta name="twitter:card" content="player">
    <meta name="twitter:site" content="@whambush">
    <meta name="twitter:title" content="{{ $video['name'] }}">
    <meta name="twitter:description" content="{{ $video['description'] }}">
    <meta name="twitter:image" content="{{ Request::url() }}/image.jpg">
    <meta name="twitter:player" content="">
    <meta name="twitter:player:width" content="750">
    <meta name="twitter:player:height" content="422">
    <meta name="twitter:player:stream" content="{{ $video['external_url'] }}">
    <meta name="twitter:player:stream:content_type" content="video/mp4">
    
    <meta name="apple-itunes-app" content="app-id=876370933,  app-argument=whambush://?video={{ $video['id']}}">
    
@stop

@section('page_title')
<title>Whambush - {{$video['name']}}</title>
@stop

@section('extra_top')
   <link rel="stylesheet" type="text/css" href="//releases.flowplayer.org/5.5.2/skin/minimalist.css">
   <style>
   .is-splash.flowplayer .fp-ui,.is-paused.flowplayer .fp-ui { background:url("{{ asset('img/play_button.png') }}") center no-repeat; background-size:11%;}
   .flowplayer .fp-progress { background-color: rgba(172, 194, 52, 1)}
   </style>
   <script src="//releases.flowplayer.org/5.5.2/commercial/flowplayer.min.js"></script>
@stop

@section('extra_bottom')
<script src="{{ asset('js/vendor/emoji_image_replace.js') }}"></script>
<script src="{{ asset('js/app.js') }}"></script>
@stop

@section('content')


        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-9">
                        
                        <div class="videoWrapper">
                                <div class="flowplayer no-time no-volume" data-embed="false" data-key="$366401312122019" data-logo="//whambush.com/img/favicon.png" data-analytics="{{ Config::get('wb.GA_id') }}">
                                    <video poster={{ $video['thumbnail_url'] }}>
                                        <source type="video/mp4" src={{ $video['external_url'] }}>
                                    </video>
                                </div>
                        </div>
                        <div class="get-app">
                            <a href="#get-app" data-toggle="modal" data-target="#get-app">
                                {{ trans('mission.mission_enter_help') }} »
                            </a>
                        </div>
                        <?php
                            $url = parse_url(asset(''), PHP_URL_HOST)."/v/".$slug;
                        ?>
                        <a href="#" id="applink"class="mission-specs" url={{ $url }} hidden><center>{!! trans('common.open_in_app') !!}</center></a>
                        <div class="video-bar">
                            <div class="user-info">
                                <img src={{ $added_by_profile_picture }}>
                                <span class="user-name"><a href=<?php echo LaravelLocalization::getLocalizedURL(null,'/u/'.$video['added_by']['username']); ?>>{{ $video['added_by']['username'] }}</a></span>
                            </div>
                            <div class={{ $shit_css }}>{{ abs($shit) }}</div>
                            <div class={{ $banana_css }}>{{ abs($banana) }}</div>
                        </div>
                        <div class="main-content well well-lg">
                            <h1>{{ $video['name'] }}</h1>
                            <p><?php echo $video['description_html']; ?></p>
                        </div>
<?php 
    if (count($comments) > 0) {
       $count =  count($comments) > 3 ? 3: count($comments);
       for ($i = 0; $i < $count ; $i++) { 
            if ($comments[$i]['user']['profile_picture'] != "") {
                $profilepic = $comments[$i]['user']['profile_picture'];
            } else {
                $profilepic = asset('/img/minion.png');
            }
            echo '                                
                        <div class="main-content comments well well-md">
                                <div class="row">
                                    <div class="col-xs-3 col-sm-2">
                                        <img src="'.$profilepic.'" width="61" height="61" class="img-circle">
                                    </div>
                                    <div class="col-xs-9 col-sm-10">
                                        <p>'.$comments[$i]['comment'].'</p>
                                        <span><a href="/u/'.$comments[$i]['user']['username'].'">'.$comments[$i]['user']['username'].'</a></span>
                                    </div>
                                </div>
                        </div>'.PHP_EOL; 
       }
    } 
 ?>    
                    </div>


                   
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->

<!-- Get app modal-->
<div class="modal fade" id="get-app">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">{{ trans('mission.mission_enter_help') }}</h4>
      </div>
      <div class="modal-body">
        <legend class="success">{{ trans('mission.mission_help_label1') }}</legend>
        <p>
            <p>{{ trans('mission.mission_help_text1') }}</p>
            <a href="https://whambush.com/ios" class="btn btn-default">App Store</a>
            <a href="https://whambush.com/android" class="btn btn-default">Google Play Store</a>

        </p>
        <br>
        <legend class="success">{{ trans('mission.mission_help_label2') }}</legend>
        <p>{{ trans('mission.mission_help_text2') }}</p>
        <br>

        <legend class="success">{{ trans('mission.mission_help_label3') }}</legend>
        <p>{{ trans('mission.mission_help_text3') }}</p>
        <br>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('mission.mission_help_button') }}</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@stop
