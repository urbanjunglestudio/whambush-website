@extends('newbase')

@section('extra_meta')

    <meta property="og:author" content="Whambush"/>
    <meta property="og:title" content="{{ $user['username'] }}"/>
    <meta property="og:description" content="{{ $user['description'] }}" />
    <meta property="og:url" content="{{ Request::url() }}" />
    <meta property="og:image" content="{{ $profile_picture }}" />
    <meta property="og:image:secure_url" content="{{ str_replace('http', 'https', $profile_picture) }}" />
    <meta property="og:image" content="http://whambush.com/img/fb.jpg" />
    <meta property="og:image:secure_url" content="https://whambush.com/img/fb.jpg" />
     
    <meta name="twitter:site" content="@whambush">
    <meta name="twitter:title" content="{{ $user['username'] }}">
    <meta name="twitter:description" content="{{ $user['description'] }}">
    <meta name="twitter:image" content="{{ $profile_picture }}">
    
    <meta name="apple-itunes-app" content="app-id=876370933,  app-argument=whambush://?user={{ $user['id']}}">
    
@stop

@section('extra_top')
<link rel="stylesheet" href="//releases.flowplayer.org/5.4.6/skin/minimalist.css">
@stop

@section('page_title')
<title>Whambush - {{$user['username']}}</title>
@stop

@section('content')
        <!-- Page Content -->
        <div id="page-content-wrapper">
<div class="container-fluid videogrid">
    <div class="row">
        <div class="col-md-12">
            <div class="userprofile-cover well well-lg">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-4 col-sm-3 col-md-3 col-lg-2">
                            <img class="img-responsive img-circle" src="{{$profile_picture}}">
                            <?php 
                                if (strlen($user['country']['flag_url']) > 0 ) {
                                    $flag_url = $user['country']['flag_url'];
                                } else {
                                    $flag_url = "/img/profile_badge_global.png";
                                }
                            ?>
                            <img class="img-responsive img-circle userflag" src="{{ $flag_url }}">
                        </div>
                        <div class="col-xs-8 col-sm-9">
                            <h1>
                                <?php
echo preg_replace_callback("/([a-z]+)/",function($m) {return strtoupper('<small>'.$m[0].'</small>');},$user['username']);
                                ?>
                            </h1>
                            <p>{!! $user['description_html'] !!}</p>
                        </div>
                    </div>
                </div>
                <ul class="user-specs"> 
                    <li data-toggle="tooltip" data-placement="top" title="<?php echo trans('common.videos'); ?>" alt="<?php echo trans('common.videos'); ?>"><span class='icon-play'></span> {{ $user['num_videos'] }}</li>
                    <li data-toggle="tooltip" data-placement="top" title="<?php echo trans('common.bananas'); ?>" alt="<?php echo trans('common.bananas'); ?>"><span class='icon-banana'></span> {{ $user['userscore'] }}</li>
                    <li data-toggle="tooltip" data-placement="top" title="<?php echo trans('common.following'); ?>" alt="<?php echo trans('common.following'); ?>"><span class='icon-following'></span> {{ $user['num_followings'] }}</li>
                    <li data-toggle="tooltip" data-placement="top" title="<?php echo trans('common.followers'); ?>" alt="<?php echo trans('common.followers'); ?>"><span class='icon-followers'></span> {{ $user['num_followers'] }}</li>
                </ul>
            </div>
        </div>
    <?php 
        //create videos
        if (count($videos) > 0) {
            for ($i=0; $i < count($videos); $i++) { 
                if ($videos[$i]['is_processed']) {
                    $score = $videos[$i]['like_count'] - $videos[$i]['dislike_count'].PHP_EOL;
                    if ($score < 0) {
                        $scoreType = "shit";
                    } else {
                        $scoreType = "banana";
                    }
                    $video_url = parse_url($videos[$i]['web_url']);
                    echo "<div class='col-sm-6 col-md-4'>".PHP_EOL;
                    echo "  <div class='video-thumb'>
                                <div class='video-thumb-content' style='background-image: url(\"".$videos[$i]['thumbnail_url']."\");'>
                                    <a href='".LaravelLocalization::getLocalizedURL(null,$video_url['path'])."'><div class='container-fluid video-thumb-overlay'>
                                        <div class='col-xs-3'>
                                            <span class='icon-".$scoreType."'></span>
                                            <p>".abs($score)."</p>
                                        </div>
                                        <div class='col-xs-6'>
                                           <span class='icon-play_video'></span>
                                        </div>
                                        <div class='col-xs-3'>
                                            <span class='icon-comment'></span>
                                            <p>".$videos[$i]['comment_count']."</p>
                                        </div>
                                    </div></a>
                                </div>
                            </div>".PHP_EOL;
                    if ($videos[$i]['mission_id'] > 0) {
                        echo "  <div class='video-grid-desc'><h4>WB: ".$videos[$i]['name']."</h4></div>".PHP_EOL;
                    } else {
                        echo "  <div class='video-grid-desc'><h4>".$videos[$i]['name']."</h4></div>".PHP_EOL;
                    }
                    echo "</div>".PHP_EOL;
                }
            }    
        }
    ?>
    </div>
</div>


 </div>
<!-- /#page-content-wrapper -->


@stop
