<?php

class FlatpageTest extends TestCase {

    public function testHomePage()
    {
        $response = $this->call('GET', '/');
        $this->assertEquals(200, $response->getStatusCode());
    }

    // public function testFaqPage()
    // {
    //     $response = $this->call('GET', '/faq');
    //     $this->assertEquals(200, $response->getStatusCode());
    // }
}
