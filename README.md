# Whambush Website

## Install Docker
[Instructions at Docker.com](https://docs.docker.com/engine/installation/). For OSX use **Docker for Mac** (not the toolbox).

If you need to install ```docker-compose``` (not part of the docker in some Linux builds) see [instructions here](https://docs.docker.com/compose/install/).

## Start the containers
Run command in ```laradoc/``` folder.

```
docker-compose up nginx php-fpm
```
Once the docker has build the container you need to run the composer setup with (in ```laradoc/``` folder)

```
docker-compose exec workspace composer install
```

Run: ```cp env.example .env``` to create local environment config (fill in the details also)

Check that ```storage/``` folder is writable (if not run ```chmod -R 777 storage/```)

Once that is finnished the website is accessible at [http://localhost](http://localhost)


## Coding convention
- For PHP, read this guide: http://www.php-fig.org/psr/psr-2/
- Use ONLY spaces, no tab is allowed

## Where is what

### If you want to add a static / flat page to the site:
1. Add your route to: `/app/Http/routes.php`, there are some example there already
2. Add your blade view with the same name as the controller method to `/resources/views/flats` folder
Example: if your route is: `Route::get('/terms-of-service', 'FlatController@tos');` then your view name should be `tos.blade.php`

## How to deploy

Don't :D Ping Hieu or Jari

## Updating the language files

Websites golden language files are in Google Drive (do not edit the language files in this repository as those will be overwritten). After edits at Google Drive you can import your updates to web following way.


1. Get localisation_parser from http://github.com/jvk75/localisation_parser
2. Download zip and unzip the package to some good location
3. Go to `whambush-website/resourses/lang/` folder
4. Execute `php <path to parser folder>/parse.php -laravel -linkfile web_language_files_at_google_drive.php`. You can add `-force` to the command to generate files to languages that are not fully translated (web will default to english if string is missing).
5. Commit changes to github

