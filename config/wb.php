<?php

return [
    'head_script' => env('HEAD_SCRIPT', ''),
    'GA_id' => env('GOOGLE_ANALYTICS_ID', ''),
];
