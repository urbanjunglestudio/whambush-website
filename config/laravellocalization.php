<?php

return [
    'supportedLocales' => [
        'en' => 'English',
        'fi' => 'Suomi',
        'et' => 'Eesti',
        'sv' => 'Svenska',
    ],
];
