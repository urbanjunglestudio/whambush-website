<?php

return [
    'base_url' => env('API_URL', 'https://api.whambush.com/'),
    'admin_username' => env('API_ADMIN_USERNAME', ''),
    'username' => env('API_USERNAME', ''),
    'admin_password' => env('API_ADMIN_PASSWORD', ''),
    'password' => env('API_PASSWORD', ''),
    'guest_id' => env('API_GUEST_ID', '9314'),
    'version' =>  env('API_VERSION', 'v2.1'),
    'info_email' =>  env('INFO_EMAIL', ''),
    'path' => [
        'token' => 'login/',
        'videos' => 'videos/',
        'video' => 'videos/slug/{slug}/',
        'comments' => 'comments/video/{video_id}',
        'comment' => 'comments/{what}/{id}/',
        'search' => 'search/{what}/',
        'bananas' => 'bananas/',
        'missions' => 'missions/',
        'mission' => 'missions/slug/{slug}/',
        'missionWithId' => 'missions/{mission_id}/',
        'user' => 'users/{id}/',
        'countries' => 'countries/',
        'channels' => 'channels/',
        'register' => 'users/',
    ],
    'sg_api_key' => env('SG_API_KEY', ''),
];
