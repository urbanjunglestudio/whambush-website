<?php
use lib_autolink;

function urlize($text, $maxTextLength = 50, $nofollow = true) {
    $rel = "";
    if ($nofollow) {
        $rel = 'rel="nofollow"';
    }
    return autolink($text, $maxTextLength, 'target="_blank" '.$rel);
}

function getLanguageWithCountry($country) {
    switch ($country) {
        case "FI":
            return "fi";
            break;
        case "EE":
            return "et";
            break;
        default:
            return "en";
            break;
    }
}

?>
