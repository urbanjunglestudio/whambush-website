from __future__ import with_statement
from fabric.api import (
    task, local, hosts, env, cd, run, warn_only, sudo
)


# ------------------------------ Devops ------------------------------ #
@task(alias='is')
def init_server():
    '''
    Deploy the whole infrastructure to a new server
    '''
    local(
        'ansible-playbook devops/server.yml -i devops/hosts --ask-vault-pass'
    )
    local('ansible-playbook devops/app.yml -i devops/hosts --ask-vault-pass')


@task(alias='ss')
def sync_server_settings():
    '''
    Sync the latest templates to the server app
    '''
    local(
        'ansible-playbook devops/app.yml -i devops/hosts --ask-vault-pass '
        '--tags templated_settings'
    )


def _deploy(type):
    env.user = 'root'
    with cd('/srv/whambush_website_{}/src'.format(type)):
        # pull latest source
        run('git pull')
        # install deps
        run('docker-compose exec laradoc "composer install"')


@task(alias='ds')
@hosts('whambush.net')
def deploy_stag():
    '''
    Deploy latest changes to staging server
    '''
    _deploy('stag')


@task(alias='dp')
@hosts('whambush.com')
def deploy_prod():
    '''
    Deploy latest changes to PRODUCTION server
    '''
    _deploy('prod')
