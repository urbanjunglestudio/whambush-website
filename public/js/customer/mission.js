var videofile;
var startdate = "";
var enddate = "";
var progressbar;

jQuery( document ).ready( function( $ ) {
	
	function pictureUpload() {
		var $uploadCrop;

		function readFile(input) {
 			if (input.files && input.files[0]) {
	            var reader = new FileReader();
	            reader.onload = function (e) {
	            	$uploadCrop.croppie('bind', {
	            		url: e.target.result
	            	});
	            	$('.upload-missionpic').addClass('ready');
	            }
	            reader.readAsDataURL(input.files[0]);
	            $( "#missionpic_dialog" ).dialog( "open" );
	        }
	        else {
		        $.get( '/translate/' + $('#locale').attr('value') + '/customer.ERRORNOTSUPPORTED', { token: $('input[name="_token"]').attr('value') }, function(result){
		        	swal(result);
		    	});
		    }
		}

		$uploadCrop = $('#upload-missionpic').croppie({
			viewport: {
				width: 640,
				height: 360,
				type: 'square'
			},
			boundary: {
				width: 640,
				height: 360
			}
		});

		$('#upload_missionpic').on('change', function () { 
			readFile(this); 
		});

		$('#crop_ok').on('click', function (ev) {
			$uploadCrop.croppie('result', {
				type: 'canvas',
				size: 'viewport'
			}).then(function (resp) {
				//$( "#profilepicbtn" ).hide();
				//$( "#missionpicview" ).html('<br><img src="'+resp+'" style="width: 640px; height: 360px;" class="center">');
				$( "#missionpicview" ).attr('src',resp);
				
				$( "#missionimage" ).val(resp);
				$( "#missionpic_dialog" ).dialog( "close" );
			});
		});
	}
	
	$('#_file_').on('change', function () { 
    	videofile = this.files[0];
    	$("#msg0").css('color','#FFFFF');
    	$("#msg0").html(this.files[0].name);
    	checkVideo();
    	return true;
    });
		    
    $( "#datepicker" ).datepicker({
    	dateFormat:"DD, d MM, yy",
    	firstDay: 1, 
    	minDate: 1
    });

    $( "#datepicker_e" ).datepicker({
    	dateFormat:"DD, d MM, yy",
    	firstDay: 1, 
    	minDate: 2
    });
    
    progressbar = $( "#progressbar" ).progressbar();

	pictureUpload();

    $( "#datepicker" ).on('change',function(){
	    startdate = $( "#datepicker" ).datepicker( "getDate" );
	    var mindate = new Date(startdate);
	    mindate.setDate(mindate.getDate()+1);
	    $( "#datepicker_e" ).datepicker( "option", "minDate", mindate );
	    enddate = $( "#datepicker_e" ).datepicker( "getDate" );
    });

    $( "#datepicker_e" ).on('change',function(){
	    enddate = $( "#datepicker_e" ).datepicker( "getDate" );
    });

	//$( "#mission-wizard-t-4" ).on('click',createPreview());

} );

function onStepChange(event, currentIndex, newIndex)
{

    $("#overlay").show();

	if (currentIndex == 0) {
		return checkVideo();
	} else if (currentIndex == 1 && newIndex == 2) {
		return  checkImage();
	} else if (currentIndex == 2 && newIndex == 3) {
		return checkFields(1);
	} else if (currentIndex == 3 && newIndex == 4) {
		if (checkFields(2)) {
			createPreview();
			return true;
		} else {
			return false;			
		}
	}
    $("#overlay").hide();
	return true;
}

function onWizardFinishing(event, currentIndex)
{
    $("#overlay").show();
	uploadVideo();
	//$("#missionvideoid").attr('value','1');
	//createMission();
	return true;
}

function checkVideo(){

	if (typeof videofile !== 'undefined') {
		if( videofile.type.match('video.*') && videofile.size < 400000000 ) {
    	$("#overlay").hide();
			return true;
		} 
	}
	$.get( '/translate/' + $('#locale').attr('value') + '/customer.ERRORVIDEO', { token: $('input[name="_token"]').attr('value') }, function(result){
		$("#msg1").css('color','#CD554A');
    	$("#msg0").html(result);
    });
    
    $("#overlay").hide();

	return false;
}

function checkImage(){
    $("#overlay").hide();
	return true;
}

function checkFields(page)
{
	var error = false;
	var errormsg = 'ERRORGENERAL';
	if (page === 1) {
		var title = $( "body" ).find( 'input[name=title]' ).val();
		var description = $( "body" ).find( 'textarea[name=description]' ).val();
		var winner = $( "body" ).find( '#winselect' ).val();
		var prize = $( "body" ).find( 'input[name=prize]' ).val();
		if (title.length == 0 && description.length == 0 && prize.length == 0){
			error = true;
			errormsg = 'ERROREMPTYFIELDS';
		} else if (winner == "-") {
			error = true;
			errormsg = 'ERRORNOWINNER';
		}
	} else if (page === 2){
		var link1 = $( "body" ).find( 'input[name=link1]' ).val();
		var link2 = $( "body" ).find( 'input[name=link2]' ).val();
		var link3 = $( "body" ).find( 'input[name=link3]' ).val();
		var start = startdate;
	    var end   = enddate;
		if (!isUrl(link1) || !isUrl(link2) || !isUrl(link3)) { 
			errormsg = 'ERRORBROKENLINK';
			error = true;
		} else if (start.length === 0 || end.length === 0) {
			errormsg = 'ERRORNODATES';
			error = true;			
		} else {
			var sd = new Date(start);
			var ed = new Date(end);
			if (sd.getTime() > ed.getTime()) {
				errormsg = 'ERRORENDDATE';
				error = true;			
			}
		}
	}

    $("#overlay").hide();

	if (error) {
		$.get( '/translate/' + $('#locale').attr('value') + '/customer.'+errormsg, { token: $('input[name="_token"]').attr('value') }, function(result){
	    	$("#msg"+page).html(result);
    	});	
		return false;
	}
	$("#msg"+page).html('');
	return true;
}

function createPreview()
{
	var end_date = new Date(enddate);
	end_date.setHours(15);
	$.get( window.location.href+'/description', { 
		token: $('input[name="_token"]').attr('value'), 
		title: $( "body" ).find( 'input[name=title]' ).val(),
		description: $( "body" ).find( 'textarea[name=description]' ).val(),
		winner: $( "body" ).find( '#winselect' ).val(),
		prize: $( "body" ).find( 'input[name=prize]' ).val(),
		link1: $( "body" ).find( 'input[name=link1]' ).val(),
		link2: $( "body" ).find( 'input[name=link2]' ).val(),
		link3: $( "body" ).find( 'input[name=link3]' ).val(),
		end: end_date.getTime(),
	}, function(result){
		$('#missiontitle').html(result.title);
		$('#missiondescription').html(result.description);
		$('#missiondescriptionraw').html(result.description_raw);
		$('#missiontime').html('Start: '+$('#datepicker').val()+"<br>End: "+$('#datepicker_e').val());
		var img = $( "#missionimage" ).val();
		if (img.length > 0) {
			$('#videothb').attr('src',$( "#missionimage" ).val());
		};
	    $("#overlay").hide();
	},'json');	
}

function createMission()
{
	
	var token = $('input[name="_token"]').attr('value');
	var start_date = new Date(startdate);
	start_date.setHours(15);
	start_date.setMinutes(00);
	var end_date = new Date(enddate);
	end_date.setHours(15);
	end_date.setMinutes(00);

	var form = new FormData();
	form.append("token", $( this ).find( 'input[name=_token]' ).val());
    form.append("mission_title",  $('#missiontitle').html());
    form.append("mission_description",  $('#missiondescriptionraw').html());
    form.append("mission_country",  $('#missioncountry').attr('value'));
    form.append("mission_start_date",  start_date.getTime());
    form.append("mission_end_date",  end_date.getTime());
    form.append("mission_linked_file_id",  $("#missionvideoid").attr('value'));
    form.append("mission_image_filedata", $( "#missionimage" ).val());
    $.ajax({ 
        url: window.location.href,
        type: "POST", 
        headers: {'X-CSRF-TOKEN': token},            
        data: form, 
        contentType: false,       
        cache: false,             
        processData:false,
        dataType: 'json', 
        async: false,	
		success: function( response ) {
    		$("#overlay").hide();
    		swal({   
    			title: response.title,   
    			text: response.msg,   
    			imageUrl: "/img/apple-touch-icon-144-precomposed.png", 
    			showCancelButton: false,   
    			confirmButtonColor: "#31333f",   
    			confirmButtonText: "OK",   
    			closeOnConfirm: false,
    			closeOnCancel: false, }, function(isConfirm){ location.reload(); }
    		);
		},
		error: function(){
    		$("#overlay").hide();
    		alert('mission fail');
		},
   	});
}

function uploadVideo()
{
	var token = $('input[name="_token"]').attr('value');
	var start_date = new Date(startdate);
	start_date.setHours(15);
	start_date.setMinutes(00);
	
	var form = new FormData();
    form.append('_token', token);
    form.append("_video_type", "normal");
    form.append("_video_title", $('#missiontitle').html());
    form.append("_video_description", $('#missiondescriptionraw').html());
    form.append("_video_creator", $('#missioncreatorid').attr('value'));
    form.append("_video_start_timedate","1");
    form.append("_video_start_date","1");
    form.append("_video_start_time","1");
    form.append("_video_country",$('#missioncountry').attr('value'));
    form.append("_parsed_time", start_date.getTime());

	$("#progressbar").show();
	$("#progresstext").show();
	$("#progresstext").html('Uploading...');

	doUpload(videofile,form,token,function(response){
	    if (response.status != "error") {
            $("#missionvideoid").attr('value',response.videoid);
            $("#progressbar").hide();
			$("#progresstext").hide();
            createMission();
	    } else {
        	alert('video fail');
            $("#progressbar").hide();
			$("#progresstext").hide();
            $("#overlay").hide();
        }
	});
}

function isUrl(url)
{
	if (url.length === 0) {
		return true;
	} 
	var result = false;
	
	var token = $('input[name="_token"]').attr('value');
	var f = new FormData();
	f.append('url',url);

    $.ajax({ 
            url: "/isUrl",
            type: "POST", 
            headers: {'X-CSRF-TOKEN': token},            
            data: f, 
            contentType: false,       
            cache: false,             
            processData:false,
            dataType: 'json', 
            async: false,	
    		success: function( response ) {
				result = response;
    		},
   	});
   	return result;

}
