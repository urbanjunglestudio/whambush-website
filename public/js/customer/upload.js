function doUpload(file, form, token, callBack) {
    $.post("/wb_admin/video/get_signature", 
        {   
            "_token": token,
        },
        function( response ) {
            var url = 'https://' + response.bucket + '.s3.amazonaws.com/';

            var data = new FormData();
            data.append('AWSAccessKeyId' ,response.accesskeyid);
            data.append('signature', response.signature); 
            data.append('acl', response.acl); 
            data.append('key', response.key); 
            data.append('bucket', response.bucket); 
            data.append('policy', response.policy); 
            data.append('success_action_status', '201'); 
            data.append('file', file, file.name);

            $.ajax({ // upload to s3
                url: url,
                headers: {'x-amz-acl': response.acl},   
                type: "POST",             
                data: data, 
                contentType: false,       
                cache: false,             
                processData:false,
                dataType: 'xml',  
                xhr: function() { // custom xhr (is the best)
                    var xhr = new XMLHttpRequest();
                    var total = 0;
                    total += file.size;
                    xhr.upload.addEventListener("progress", function(evt) {
                        var loaded = (evt.loaded / total).toFixed(2)*100; // percent
                        progressbar.progressbar({
                            'value' : loaded
                        });
                    }, false);
                    return xhr;
                },  
                success: function( response )
                {
                    var key = $(response).find('Key').text();
                    var splitkey = key.split('/');
                    var guid = splitkey[splitkey.length - 2];
                    
                    form.append('_guid',guid);

                    $.ajax({ // process video
                        url: "/wb_admin/video/process_video",
                        type: "POST",             
                        headers: {'X-CSRF-TOKEN': token},            
                        data: form, 
                        contentType: false,       
                        cache: false,             
                        processData:false,
                        dataType: 'json',  
                        success: function( response ) {
                            callBack(response);
                        },
                        error: function(evn) 
                        {   
                            alert("Something went wrong! Try again!!");
                            console.log(evn);
                            $("#overlay").hide();
                            $("#progressbar").hide();
                            $("#progresstext").hide();
                       }
                    });
        
                },
                error: function(evn) 
                {   
                    alert("Something went wrong! Try again!");
                    $("#overlay").hide();
                    $("#progressbar").hide();
                    $("#progresstext").hide();
               }
            });
        },                
        'json'
    );
}

