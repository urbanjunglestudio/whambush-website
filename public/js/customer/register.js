jQuery( document ).ready( function( $ ) {
	
	function pictureUpload() {
		var $uploadCrop;

		function readFile(input) {
 			if (input.files && input.files[0]) {
	            var reader = new FileReader();
	            
	            reader.onload = function (e) {
	            	$uploadCrop.croppie('bind', {
	            		url: e.target.result
	            	});
	            	$('.upload-profilepic').addClass('ready');
	            }
	            
	            reader.readAsDataURL(input.files[0]);

	            $( "#profilepic_dialog" ).dialog( "open" );
	        }
	        else {
		        $.get( '/translate/' + $('#locale').attr('value') + '/customer.ERRORNOTSUPPORTED', { token: $('input[name="_token"]').attr('value') }, function(result){
		        	swal(result);
		    	});
		    }
		}

		$uploadCrop = $('#upload-profilepic').croppie({
			viewport: {
				width: 640,
				height: 640,
				type: 'square'
			},
			boundary: {
				width: 640,
				height: 640
			}
		});

		$('#upload_profilepic').on('change', function () { 
			readFile(this); 
		});

		$('#crop_ok').on('click', function (ev) {
			$uploadCrop.croppie('result', {
				type: 'canvas',
				size: 'viewport'
			}).then(function (resp) {
				//$( "#profilepicbtn" ).hide();
				$( "#profilepicview" ).html('<img src="'+resp+'" style="width: 350px; height: 350px;" class="center">');
				$( "#profileimage" ).val(resp);
				$( "#profilepic_dialog" ).dialog( "close" );
			});
		});
	}

	function getCountries()
	{
		var token = $('input[name="_token"]').attr('value');
		$.get('/countries', { token: token },function(result){
			var def = $( "#countries" ).attr('loc');
			var msg = '<select name="_country_select" id="country_select" form="customer_register"><option value="">'+def+'</option>';
			$.each(result, function(index, value){
                var option = '<option name="country" value="' + value['country'] + '">' + value['name'] + '</option>';
                msg = msg.concat(option);
            });
            msg = msg.concat('</select>');
				
			$( "#countries" ).append(msg);
		},'json');
	}
	
	$("#description").on('update',function(){

	});

	getCountries();
	pictureUpload();

} );

function onStepChange(event, currentIndex, newIndex)
{
	if (currentIndex == 0) {
		var code = $( "body" ).find( 'input[name=keycode]' ).val();
		return checkKeyCode(code);
	} else if (currentIndex == 1) {
		return  checkFieldsAndRegister();
	} else if (currentIndex == 2) {
		return uploadPicture();
	} else if (currentIndex == 3) {
		$.get('/translate/' + $('#locale').attr('value') + '/customer.ACTIVATIONMESSAGE', { token: $('input[name="_token"]').attr('value') }, function(result){
			$('#activemessage').html(result.replace('%%EMAIL%%', "<b>" + $( "body" ).find( 'input[name=email]' ).val() + "</b>"));		
		});
		return uploadDescription();
	} 

	return true;
}

function onWizardFinishing(event, currentIndex, newIndex)
{
	location.reload();
	window.open(window.location.href + "/../logout", "_self");
	return true;
}

function isEmail(email) {
  	var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  	return regex.test(email);
}

function checkKeyCode(keycode)
{
	var result = false;
	var token = $('input[name="_token"]').attr('value');
    var f = new FormData();
    f.append('code',keycode);
    $.ajax({ 
            url: window.location.href + "/code",
            type: "POST", 
            headers: {'X-CSRF-TOKEN': token},            
            data: f, 
            contentType: false,       
            cache: false,             
            processData:false,
            dataType: 'json', 
            async: false,	
    		success: function( response ) {
    			$("#msg0").html(response.msg);
				result = response.result;
    		},
   	});
   	return result;
}

function checkFieldsAndRegister()
{
	var result = false;
	
	var token = $('input[name="_token"]').attr('value');
    
    var f = new FormData();
    f.append('username', $( "body" ).find( 'input[name=username]' ).val());
    f.append('email', $( "body" ).find( 'input[name=email]' ).val());
    f.append('country', $( "body" ).find( '#country_select' ).val());
    f.append('password', $( "body" ).find( 'input[name=password]' ).val());
    f.append('password2', $( "body" ).find( 'input[name=password2]' ).val());
    $.ajax({ 
            url: window.location.href + "/fields",
            type: "POST", 
            headers: {'X-CSRF-TOKEN': token},            
            data: f, 
            contentType: false,       
            cache: false,             
            processData:false,
            dataType: 'json', 
            async: false,	
    		success: function( response ) {
    			$("#msg1").html(response.msg);
				result = response.result;
    		},
   	});
   	return result;
}

function uploadPicture() {
	var result = false;
	var data = $( "body" ).find( '#profileimage' ).val();
	
	if (data.lenght == 0) {
		return true;
	}

	var token = $('input[name="_token"]').attr('value');
    
    var f = new FormData();
    f.append('data', data);
    f.append('id', $('#msg1').html());
    $.ajax({ 
            url: window.location.href + "/picture",
            type: "POST", 
            headers: {'X-CSRF-TOKEN': token},            
            data: f, 
            contentType: false,       
            cache: false,             
            processData:false,
            dataType: 'json', 
            async: false,	
    		success: function( response ) {
    			var msg = response.msg
    			if (msg.lenght > 0) {
    				swal(msg);
    			};
    			result = response.result;
    		},
   	});
   	return result;
}

function uploadDescription() {
	var result = false;
	var desc = $( "body" ).find( 'textarea[name=description]' ).val()
	
	if (desc.lenght == 0) {
		return true;
	}

	var token = $('input[name="_token"]').attr('value');
    
    var f = new FormData();
    f.append('desc', desc);
    f.append('id', $('#msg1').html());
    $.ajax({ 
            url: "/customer/register/description",
            type: "POST", 
            headers: {'X-CSRF-TOKEN': token},            
            data: f, 
            contentType: false,       
            cache: false,             
            processData:false,
            dataType: 'json', 
            async: false,	
    		success: function( response ) {
				var msg = response.msg
    			if (msg.lenght > 0) {
    				swal(msg);
    			};				
    			result = response.result;
    		},
   	});
   	return result;
}

