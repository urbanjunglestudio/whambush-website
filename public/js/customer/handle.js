var newOpen = false;
var activeOpen = false;
var oldOpen = false;
var dialog;
jQuery( document ).ready( function( $ ) {
	

	$('#missionnew').on('click', function (ev) {
		$('#missionnew').css('color', '');
		if ($('#missionnew').attr('value') > 0) {
			if ($("#missionnewcontent").html().length < 1) {
				$("#overlay").show();
				$.get( window.location.href.split('#')[0] + '/rows/new', { 
					token: $('input[name="_token"]').attr('value'), 
					size: $('#missionnew').attr('value'), 
				}, function(result){
	    			$("#missionnewcontent").html(result);
    			    $("#overlay").hide();
	    			toggleMainRow('new');		
		    	},'json');	
			} else {
				toggleMainRow('new');		
			}
		}
	});

	$('#missionactive').on('click', function (ev) {
		$('#missionactive').css('color', '');
		if ($('#missionactive').attr('value') > 0) {
			if ($("#missionactivecontent").html().length < 1) {
				$("#overlay").show();
				$.get( window.location.href.split('#')[0] + '/rows/active', { 
					token: $('input[name="_token"]').attr('value'), 
					size: $('#missionactive').attr('value'), 
				}, function(result){
	    			$("#missionactivecontent").html(result);
    			    $("#overlay").hide();
	    			toggleMainRow('active');		
		    	},'json');	
			} else {
				toggleMainRow('active');		
			}
		}
	});
	
	$('#missionold').on('click', function (ev) {
		$('#missionold').css('color', '');
		if ($('#missionold').attr('value') > 0) {
			if ($("#missionoldcontent").html().length < 1) {
				$("#overlay").show();
				$.get( window.location.href.split('#')[0] + '/rows/old', { 
					token: $('input[name="_token"]').attr('value'), 
					size: $('#missionold').attr('value'), 
				}, function(result){
	    			$("#missionoldcontent").html(result);
    			    $("#overlay").hide();
	    			toggleMainRow('old');		
		    	},'json');	
			} else {
				toggleMainRow('old');		
			}
		}
	});

	$('#missionnewcontent').on('click','.missionedit', function (ev) {
		$("#overlay").show();
		$.get( window.location.href.split('#')[0] + '/edit', { 
			token: $('input[name="_token"]').attr('value'), 
			id: $(this).attr('value'), 
		}, function(result){
			openDialog(result.title,result.msg);
		    $("#overlay").hide();
		},'json');	

		return false;
	});
	

	$('body').on('click','.missionstats', function (ev) {
		$("#overlay").show();
		$.get( window.location.href.split('#')[0] + '/stats', { 
			token: $('input[name="_token"]').attr('value'), 
			id: $(this).attr('value'), 
		}, function(result){
			openDialog(result.title,result.msg);
		    $("#overlay").hide();
		},'json');	

		return false;
	});

	$('#missionoldcontent').on('click','.missionwinner', function (ev) {
		$("#overlay").show();
		$.get( window.location.href.split('#')[0] + '/videos', { 
			token: $('input[name="_token"]').attr('value'), 
			id: $(this).attr('value'), 
			order: $(this).attr('order'),
		}, function(result){
			openDialog(result.title,result.msg,window.innerWidth-50,window.innerHeight-50);
		    $("#overlay").hide();
		},'json');	

		return false;
	});
		
	function openDialog(title,content,width,height) 
	{
		width = typeof width !== 'undefined' ? width : 800;
		height = typeof height !== 'undefined' ? height : 400;
		$( "#overlaybkg").show();
 		$( "#dialog" ).html(content)
		dialog = $( "#dialog" ).dialog({ 
			autoOpen: true, 
			width: width,
			height: height,
			resizeable: false,
			show: true,
			hide: true,
			title: title,
			close: function(event, ui) {
				$( "#dialog" ).dialog( "destroy" );
				$( "#overlaybkg").hide();
	  		}
 		});

	}
	
} );

function closeDialog()
{
	dialog.dialog( "close" );
	return false;
}
function saveMission(id)
{
	var title = $('#title').val(); ;
	var description = $('#description').val(); 
	$("#overlay").show();

	$.post( window.location.href.split('#')[0] + '/edit', { 
		_token: $('input[name="_token"]').attr('value'), 
		id: id, 
		title: title,
		description: description,
	}, function(response){
		dialog.dialog( "close" );
		swal({   
			title: response.title,   
			text: response.msg,   
			imageUrl: "/img/apple-touch-icon-144-precomposed.png", 
			showCancelButton: false,   
			confirmButtonColor: "#31333f",   
			confirmButtonText: "OK",   
			closeOnConfirm: true,
			closeOnCancel: false }
		);
		$("#overlay").hide();
	},'json');

	return false;
}

function confirmWinner()
{
	var id = "";
	var vid = "";
	$.each($('.selectedwinner:checked'), function(i, val ) {
		id += val.getAttribute('id') + ",";
		id += val.getAttribute('video') + ",";
		id += val.getAttribute('mission') + ":";
	});
	$("#overlay").show();
	$.post( window.location.href.split('#')[0] + '/videos/winner', { 
		_token: $('input[name="_token"]').attr('value'), 
		id: id, 
	}, function(response){
		dialog.dialog( "close" );
		swal({   
			title: response.title,   
			text: response.msg,   
			imageUrl: "/img/apple-touch-icon-144-precomposed.png", 
			showCancelButton: false,   
			confirmButtonColor: "#31333f",   
			confirmButtonText: "OK",   
			closeOnConfirm: true,
			closeOnCancel: false }
		);
	    $("#overlay").hide();
	},'json');	

	return false;
}

function toggleMainRow(type)
{
	$('#missionnewcontent').hide();		
	$('#missionactivecontent').hide();		
	$('#missionoldcontent').hide();		
	$('#missionnew').css('color', '');
	$('#missionactive').css('color', '');
	$('#missionold').css('color', '');
	$('#missionnew').find('.downarrow').removeClass('uparrow');
	$('#missionactive').find('.downarrow').removeClass('uparrow');
	$('#missionold').find('.downarrow').removeClass('uparrow');
	switch(type) {
		case 'new':
			if (!newOpen) {
				$('#missionnew').css('color', '#ADC234');
				$('#missionnewcontent').show();
				$('#missionnew').find('.downarrow').addClass('uparrow');
			}
			newOpen = !newOpen;
			activeOpen = false;
			oldOpen = false;
			break;
		case 'active':
			if (!activeOpen) {
				$('#missionactive').css('color', '#ADC234');
				$('#missionactivecontent').show();
				$('#missionactive').find('.downarrow').addClass('uparrow');
			}
			newOpen = false;
			activeOpen = !activeOpen;
			oldOpen = false;
			break;
		case 'old':
			if (!oldOpen) {
				$('#missionold').css('color', '#ADC234');
				$('#missionoldcontent').show();
				$('#missionold').find('.downarrow').addClass('uparrow');
			}
			newOpen = false;
			oldOpen = !oldOpen;
			activeOpen = false;
			break;
	}

}