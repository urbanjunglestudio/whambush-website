function doUpload(form, token, callBack) {
        $.ajax({ // check fields
            url: "/wb_admin/video/check_video_fields",
            type: "POST", 
            headers: {'X-CSRF-TOKEN': token},            
            data: form, 
            contentType: false,       
            cache: false,             
            processData:false,
            dataType: 'json', 
            xhr: function() { // custom xhr (is the best)

               var xhr = new XMLHttpRequest();
               var total = 0;

               // Get the total size of files
               $.each(document.getElementById('_file_').files, function(i, file) {
                      total += file.size;
               });

               // Called when upload progress changes. xhr2
               xhr.upload.addEventListener("progress", function(evt) {
                      // show progress like example
                      var loaded = (evt.loaded / total).toFixed(2)*100; // percent

                    //$('#progress').text('Uploading... ' + loaded + '%' );
                    $( "#progressbar" ).progressbar({
                        'value' : loaded
                    });
               }, false);

               return xhr;
            },  
 
            success: function( response ) {
                $( "#progressbar" ).progressbar({
                    'value' : 0
                });
                $('#progresstext').html('Uploading..');

                document.getElementById("_form_errors").value = "";
                if (response.status == 'error') {
                    document.getElementById("_rsp_msg").innerHTML = response.msg;
                    for (var i = 0; i < response.errors.length; i++) {
                        document.getElementById(response.errors[i]).style.border = "thin dotted #FF0000";
                    }
                    document.getElementById("_form_errors").value = response.errors;
                    document.getElementById("_rsp_msg").style.color = "#FF0000";
                    $("#overlay").hide();
                    $("#progressbar").hide();
                } else {
                    $.post("/wb_admin/video/get_signature", // signature from vzaar
                        {   
                            "_token": response.token,
                        },
                        function( response ) {
                            var file = document.getElementById('_file_').files[0];
                            
                            var url = 'https://' + response.bucket + '.s3.amazonaws.com/';

                            var data = new FormData();
                            data.append('AWSAccessKeyId' ,response.accesskeyid);
                            data.append('signature', response.signature); 
                            data.append('acl', response.acl); 
                            data.append('key', response.key); 
                            data.append('bucket', response.bucket); 
                            data.append('policy', response.policy); 
                            data.append('success_action_status', '201'); 
                            data.append('file', file, file.name);

                            $.ajax({ // upload to s3
                                url: url,
                                headers: {'x-amz-acl': response.acl},   
                                type: "POST",             
                                data: data, 
                                contentType: false,       
                                cache: false,             
                                processData:false,
                                dataType: 'xml',  
                                xhr: function() { // custom xhr (is the best)

                                   var xhr = new XMLHttpRequest();
                                   var total = 0;

                                   // Get the total size of files
                                   $.each(document.getElementById('_file_').files, function(i, file) {
                                          total += file.size;
                                   });

                                   // Called when upload progress changes. xhr2
                                   xhr.upload.addEventListener("progress", function(evt) {
                                          // show progress like example
                                          var loaded = (evt.loaded / total).toFixed(2)*100; // percent

                                        //$('#progress').text('Uploading... ' + loaded + '%' );
                                        $( "#progressbar" ).progressbar({
                                            'value' : loaded
                                        });
                                   }, false);

                                   return xhr;
                                },  
                                success: function( response )
                                {
                                    $('#progresstext').html('Prosessing..');
                                    var key = $(response).find('Key').text();
                                    var splitkey = key.split('/');
                                    var guid = splitkey[splitkey.length - 2];
                                    
                                    form.append('_guid',guid);

                                    $.ajax({ // process video
                                        url: "/wb_admin/video/process_video",
                                        type: "POST",             
                                        headers: {'X-CSRF-TOKEN': token},            
                                        data: form, 
                                        contentType: false,       
                                        cache: false,             
                                        processData:false,
                                        dataType: 'json',  
                                        success: function( response ) {
                                            callBack(response);
                                        },
                                        error: function(evn) 
                                        {   
                                            alert("Something went wrong! Try again!!");
                                            console.log(evn);
                                            $("#overlay").hide();
                                            $("#progressbar").hide();
                                            $("#progresstext").hide();
                                       }
                                    });
                        
                                },
                                error: function(evn) 
                                {   
                                    alert("Something went wrong! Try again!");
                                    $("#overlay").hide();
                                    $("#progressbar").hide();
                                    $("#progresstext").hide();
                               }
                            });
                        },                
                        'json'
                    );
                }
            }
        });
}