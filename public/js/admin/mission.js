jQuery( document ).ready( function( $ ) {
 
    
    $( '#new_mission' ).on( 'submit', function() {
        event.preventDefault();
        var errors = $( this ).find( 'input[name=_form_errors]' ).val();
        if (errors.length > 0) {
            var tmp = errors.split(",");
            for (var i = 0; i < tmp.length; i++) {
                document.getElementById(tmp[i]).style.border = "";
                document.getElementById("_rsp_msg").innerHTML = "";
            }
        }
        
        //show spinner 
        $("#overlay").show();

        var dur = $("input[type='radio'][name='_mission_duration']:checked").val();
        var country = $("input[type='radio'][name='_mission_country']:checked").val();
        var creator = $("input[type='radio'][name='_mission_creator']:checked").val();
        var mission_type = $("input[type='radio'][name='_mission_type']:checked").val();
        var icon_name = $( this ).find( 'input[name=_mission_icon_name]' ).val();
        
        if ($( this ).find( 'input[name=_mission_start_date]' ).val()) {
            var today = new Date();
            today.setHours(today.getHours()+(today.getTimezoneOffset()/60));
            var sdate = new Date($( this ).find( 'input[name=_mission_start_date]' ).val());
            if (sdate.getDate() == today.getDate() && sdate.getMonth() == today.getMonth() && sdate.getYear() == today.getYear() && today.getHours() >= 12) {
                sdate.setHours(today.getHours() + 1);
            } else {
                sdate.setHours(15);
            }
            sdate.setMinutes(00);
            var start_date = sdate.getTime();
        } else {
            var start_date = "";
        }

        //upload icon video
        if (mission_type == "icon" && parseInt($("#_mission_video_id").val()) < 1) {
            $("#progressbar").show();
            
            $('#progresstext').html('Checking..');

            var file = document.getElementById('_file_').files[0];
            
            var file_error = true;
            if (typeof(file) != "undefined") {
                var mimes = ["video/mp4", "video/quicktime", "video/avi", "video/mpeg"];
                for (var i = mimes.length - 1; i >= 0; i--) {
                    if (file.type == mimes[i]) {
                        file_error = false;
                        break;
                    }
                };
            };
            
            var token = $('input[name="_token"]').attr('value');
            
            form = new FormData();
            form.append('_token', token);
            form.append("_video_type", "normal");
            form.append("_video_title", $( this ).find( 'input[name=_mission_title]' ).val());
            form.append("_video_description", $( this ).find( 'textarea[name=_mission_description]' ).val());
            form.append("_video_creator", creator);
            if ($( this ).find( 'input[name=_mission_start_date]' ).val()) {
                form.append("_video_start_timedate","1");
                form.append("_video_start_date","1");
                form.append("_video_start_time","1");
            } else {
                form.append("_video_start_timedate","now");
            }
            form.append("_video_country",country);
            form.append("_parsed_time", start_date);

            if (file_error) {
                document.getElementById("_rsp_msg").style.color = "#FF0000";
                document.getElementById("_rsp_msg").innerHTML = "Error: Bad file!";
                document.getElementById("_mission_video_file").style.border = "thin dotted #FF0000";
                document.getElementById("_form_errors").value = "_mission_video_file";
                $("#progresstext").hide();
                $("#progressbar").hide();
                $("#overlay").hide();
            } else
                doUpload(form, token, function(response){
                    if (response.status != "error") {
                        $("#_mission_video_id").val(response.videoid);
                        $("#progressbar").hide();
                        $("#new_mission").submit();
                    } else {
                        document.getElementById("_rsp_msg").style.color = "#FF0000";                    
                        document.getElementById("_rsp_msg").innerHTML = response.ms                                    
                        $("#progressbar").hide();
                        $("#overlay").hide();
                    }
                });

        } else {  // create the mission
            $.post($( this ).prop( 'action' ),
                {   
                    "_token": $( this ).find( 'input[name=_token]' ).val(),
                    "_mission_type": mission_type,
                    "_mission_title":  $( this ).find( 'input[name=_mission_title]' ).val(),
                    "_mission_description":  $( this ).find( 'textarea[name=_mission_description]' ).val(),
                    "_mission_creator":  creator,
                    "_mission_country":  country,
                    "_mission_start_date":  start_date,//$( this ).find( 'input[name=_mission_start_date]' ).val(),
                    "_mission_duration":  dur,
                    "_mission_linked_file_id":  $("#_mission_video_id").val(),
                    "_mission_icon_name":  $( this ).find( 'input[name=_mission_icon_name]' ).val(),
                },
                function( response ) {
                    document.getElementById("_form_errors").value = "";

                    document.getElementById("_rsp_msg").innerHTML = response.msg;
                    if (response.status == 'error') {
                        for (var i = 0; i < response.errors.length; i++) {
                            document.getElementById(response.errors[i]).style.border = "thin dotted #FF0000";
                        }
                        document.getElementById("_form_errors").value = response.errors;
                        document.getElementById("_rsp_msg").style.color = "#FF0000";
                    } else {
                        document.getElementById("_rsp_msg").style.color = "#FFFFFF";

                        ///do something with data/response returned by server
                        document.getElementById("_mission_title").hidden = true;
                        document.getElementById("_mission_description").hidden = true;
                        document.getElementById("_mission_creator").hidden = true;
                        document.getElementById("_mission_country").hidden = true;
                        document.getElementById("_mission_start_date").hidden = true;
                        document.getElementById("_mission_duration").hidden = true;
                        //document.getElementById("_mission_create").hidden = true; 
                        $('#_mission_create').hide();     
                        document.getElementById("_mission_type").hidden = true;  
                        document.getElementById("_mission_video_file").hidden = true;
                        document.getElementById("_mission_icon_name").hidden = true;      
                        document.getElementById("new_mission").reset();
                        document.getElementById("_mission_reload").hidden = false;
                    }
                    
                    //hide spinner 
                    $("#overlay").hide();
                },
                'json'
            );
        }
        //prevent the form from actually submitting in browser
        return false;
    } );
    
    $( '#_mission_normal' ).on( 'click', function() {
        document.getElementById("_mission_title").hidden = false;
        document.getElementById("_mission_description").hidden = false;
        document.getElementById("_mission_creator").hidden = true;
        document.getElementById("_mission_country").hidden = false;
        document.getElementById("_mission_start_date").hidden = false;
        document.getElementById("_mission_duration").hidden = false;
        document.getElementById("_mission_create").hidden = false;      
        document.getElementById("_mission_type").hidden = true;  
        return true;
    } ); 

    $( '#_mission_icon' ).on( 'click', function() {
        document.getElementById("_mission_video_file").hidden = false;
        document.getElementById("_mission_title").hidden = false;
        document.getElementById("_mission_description").hidden = false;
        document.getElementById("_mission_creator").hidden = false;
        document.getElementById("_mission_country").hidden = false;
        document.getElementById("_mission_start_date").hidden = false;
        document.getElementById("_mission_duration").hidden = false;
        document.getElementById("_mission_create").hidden = false;              
        document.getElementById("_mission_icon_name").hidden = false;      
        document.getElementById("_mission_type").hidden = true;  
        return true;
    } ); 

    $( '#_mission_reload').on( 'click', function() {
        location.reload(true);
    });

////////////////////////
    $( '#_mission_creator_wb' ).on( 'click', function() {
        document.getElementById("_mission_creator_username").hidden = true;
        return true;
    } );

////////////////////////
    $( '#_mission_creator_other' ).on( 'click', function() {
        document.getElementById("_mission_creator_username").hidden = false;
        return true;
    } );

////////////////////////
    $( '#_mission_creator_username' ).on( 'input', function() {
        document.getElementById("_mission_creator_other").value = "other";
        var strg = $(this).val();
        if (strg.length > 2) {
            document.getElementById("overlay").hidden = false;
            $.get( "/wb_admin/user/search", { 
                "_username" : strg, 
            }, 
            function( response ) {
                document.getElementById("_mission_creator_username_list").innerHTML = response.msg;
                document.getElementById("overlay").hidden = true;
                $( '#_mission_creator_username_list' ).show();     
            }, "json");
        };
    } ); 
    
////////////////////////
    $( '#_mission_creator_username' ).on( 'click', function() {
        var value = $("#_other_user_id").val();
        if (value > 0) {
            document.getElementById("_mission_creator_username").value = "";
            document.getElementById("_other_user_id").value = 0;    
        };
    } );
    
////////////////////////
    $( "#_mission_creator_username_list" ).on( "click", "._other_username", function( event ) {
        document.getElementById("_other_user_id").value = $( this ).attr('value');    
        document.getElementById("_mission_creator_other").value = $( this ).attr('value');
        document.getElementById("_mission_creator_username").value = $( this ).attr('name') + " (" + $( this ).attr('value') + ")";   
        $( '#_mission_creator_username_list' ).hide();     
        return false;
    });

} );