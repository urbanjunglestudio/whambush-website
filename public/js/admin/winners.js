jQuery( document ).ready( function( $ ) {
 
    $( '#_get_winners' ).on( 'submit', function() {
        $( "#dialog" ).dialog( "close" );
        $( "#banana_pop_up" ).dialog( "close" );
        $( "#email_pop_up" ).dialog( "close" );
        
        //show spinner 
        document.getElementById("overlay").hidden = false;
        document.getElementById("_content").hidden = true;
        
        var type = $("input[type='radio'][name='_mission_type']:checked").val();
        var country = $("input[type='radio'][name='_mission_country']:checked").val();
        var print_mails = 1;//$("input[type='checkbox'][name='_print_emails']:checked").val();
        var download_links = $("input[type='checkbox'][name='_download_links']:checked").val();
        var icon_friendly = $("input[type='checkbox'][name='_icon_friendly']:checked").val();
        var mail_friendly = $("input[type='checkbox'][name='_mail_friendly']:checked").val();

        $.post($( this ).prop( 'action' ),
            {   
                "_token": $( this ).find( 'input[name=_token]' ).val(),
                "_mission_type":  type,
                "_mission_country":  country,
                "_number_of_missions" : $( this ).find( 'select[name=_number_of_missions]' ).val(),
                "_number_of_entries" : $( this ).find( 'select[name=_number_of_entries]' ).val(),
                "_print_emails" : print_mails,
                "_download_links" : download_links,
                "_icon_friendly" : icon_friendly,
                "_mail_friendly" : mail_friendly,
            },
            function( response ) {
                document.getElementById("_content").innerHTML = response.msg;
                //document.getElementById("_mail_subject").value = response.template.subject;
                $("#_mail_subject").val(response.template.subject);
                //document.getElementById("_mail_message_local").innerHTML = response.template.message;
                $("#_mail_message_local").html(response.template.message);
                //hide spinner 
                document.getElementById("overlay").hidden = true;
                document.getElementById("_content").hidden = false;
            },
            'json'
        );
 
        //prevent the form from actually submitting in browser
        return false;
    } );
    

    // mail stuff
    $( "#_content" ).on( "click", ".send_mail", function( event ) {
        $( "#email_pop_up" ).dialog({ autoOpen: false, width: 800 });

        document.getElementById("overlaybkg").hidden = false;
        var message = $("#_mail_message_local").html();
        var value = $( this ).attr('value').split('%%');
        //message = message.replace('\n','<br>\n');
        message = message.replace('%%MISSION%%',value[0]);
        message = message.replace('%%USERNAME%%',value[1]);
        message = message.replace('%%BANANAS%%',"20"); // should come from API
        message = message.replace('%%PRIZE%%',"********"); // should come from API

        //document.getElementById("_mail_message").innerHTML = message;
        //document.getElementById("_mail_form_id").innerHTML = value[2];
        $("#_mail_message").html(message);
        $("#_mail_form_id").html(value[2]);

        $( "#email_pop_up" ).dialog({
            close: function( event, ui ) { 
                document.getElementById("overlaybkg").hidden = true; 
                $('#email_pop_up').dialog('destroy');
            }
        });
        $( "#email_pop_up" ).dialog( "open" );
        
        return false;
    });
    
/*    $( "#email_pop_up" ).on( "click", "._mail_template_btn", function( event ) {
        if (parseInt($('#_mail_template').html()) === 1) {
            $('#_mail_template').html('2');
        } else {
            $('#_mail_template').html('1');
        }
    });*/

    $( '#_send_mail' ).on( 'submit', function() {

        document.getElementById("overlay").hidden = false;
        $( "#email_pop_up" ).dialog( "close" );
        
        var userid = $("#_mail_form_id").html();//document.getElementById("_mail_form_id").innerHTML;

        $.post($( this ).prop( 'action' ),
            {   
                "_token": $( this ).find( 'input[name=_token]' ).val(),
                "_subject" : $( this ).find( 'input[name=_mail_subject]' ).val(),
                "_message" : $( this ).find( 'textarea[name=_mail_message]' ).val(),
                "_user":  userid,
            },
            function( response ) {
                document.getElementById("overlay").hidden = true;
                
                $("#dialog").html(response.msg);
                $( "#dialog" ).dialog({
                    title: response.status
                });
                $( "#dialog" ).dialog( "open" );

                $('#email_pop_up').dialog('destroy');
                
            },
            'json'
        );
 
        return false;
    });


    //banana stuff
    $( "#_content" ).on( "click", ".send_bananas", function( event ) {
        document.getElementById("overlaybkg").hidden = false;

        document.getElementById("_banana_form_id").innerHTML = $(this).attr('value');
        document.getElementById("_number_of_bananas").value = "";
        $( "#banana_pop_up" ).dialog({
            close: function( event, ui ) { 
                document.getElementById("overlaybkg").hidden = true; 
            }
        });
        $( "#banana_pop_up" ).dialog( "open" );
        
        return false;
    });
    
    $( '#_give_bananas' ).on( 'submit', function() {

        document.getElementById("overlay").hidden = false;
        $( "#banana_pop_up" ).dialog( "close" );

        var userid = document.getElementById("_banana_form_id").innerHTML;

        $.post($( this ).prop( 'action' ),
            {   
                "_token": $( this ).find( 'input[name=_token]' ).val(),
                "_amount" : $( this ).find( 'input[name=_number_of_bananas]' ).val(),
                "_user":  userid,
            },
            function( response ) {
                document.getElementById("overlay").hidden = true;
                
                document.getElementById("dialog").innerHTML = response.msg;
                $( "#dialog" ).dialog({
                    title: response.status
                });
                $( "#dialog" ).dialog( "open" );
            },
            'json'
        );
 
        return false;
    });
    
    // general dialog
    $( "#_content" ).on( "click", ".mission_dialog", function( event ) {

        document.getElementById("dialog").innerHTML = $(this).attr('value');
        $( "#dialog" ).dialog({
            title: $(this).attr('name'),
        });
        $( "#dialog" ).dialog( "open" );
        
        return false;
    });
} );
