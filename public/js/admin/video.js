

jQuery( document ).ready( function( $ ) {
 
////////////////////////
    $( '#new_video' ).on( 'submit', function() {
        event.preventDefault();
        //show spinner 
        $("#progresstext").show();
        $("#progressbar").show();
        $("#overlay").show();

        var errors = $( this ).find( 'input[name=_form_errors]' ).val(); 
        if (errors.length > 0) {
            var tmp = errors.split(",");
            for (var i = 0; i < tmp.length; i++) {
                document.getElementById(tmp[i]).style.border = "";
                document.getElementById("_rsp_msg").innerHTML = "";
            }
        }
        document.getElementById("_rsp_msg").style.color = "#FFFFFF";


        var time  = $("#timepicker").val();
        var split = time.split(":");

        var date = $("#datepicker").val();
        var sdate = new Date(date);
        sdate.setHours(parseInt(split[0]));
        sdate.setMinutes(parseInt(split[1]));
        var start_date = sdate.getTime();
        $('#_parsed_time').val(sdate.getTime());
        
        var token = $('input[name="_token"]').attr('value');

        $('#progresstext').html('Checking..');

        var file = document.getElementById('_file_').files[0];
        
        var file_error = true;
        if (typeof(file) != "undefined") {
            var mimes = ["video/mp4", "video/quicktime", "video/avi", "video/mpeg"];
            for (var i = mimes.length - 1; i >= 0; i--) {
                if (file.type == mimes[i]) {
                    file_error = false;
                    break;
                }
        };};
        
        var video_type = $("input[type='radio'][name='_video_type']:checked").val();
        

        form = new FormData();
        form.append('_token', token);
        form.append("_video_type", video_type);
        form.append("_video_title", $( this ).find( 'input[name=_video_title]' ).val());
        form.append("_video_description", $( this ).find( 'textarea[name=_video_description]' ).val());
        form.append("_video_tags", $( this ).find( 'input[name=_video_tags]' ).val());
        form.append("_video_creator", $("input[type='radio'][name='_video_creator']:checked").val());
        form.append("_video_start_timedate", $("input[type='checkbox'][name='_video_start_timedate']:checked").val());
        form.append("_video_start_date", $( this ).find( 'input[name=_video_start_date]' ).val());
        form.append("_video_start_time", $( this ).find( 'input[name=_video_start_time]' ).val());        
        if (video_type !== "wbtv") {
            var country = $("input[type='radio'][name='_video_country']:checked").val();
        } else {
            var channel = $("#_video_tv_channel_select :selected").val().split('%%');
            form.append("_video_tv_channel_select",channel[0]);
            var country = channel[1];
        }
        form.append("_video_country",country);
        form.append("_parsed_time", sdate.getTime());//$('#_parsed_time').val());

        if (file_error) {
            document.getElementById("_rsp_msg").style.color = "#FF0000";
            document.getElementById("_rsp_msg").innerHTML = "Error: Bad file!";
            document.getElementById("_video_file").style.border = "thin dotted #FF0000";
            document.getElementById("_form_errors").value = "_video_file";
            $("#progresstext").hide();
            $("#progressbar").hide();
            $("#overlay").hide();
        } else
            doUpload(form, token, function(response) {
                $("#_video_type").hide();
                $("#_video_file").hide();
                $("#_video_title").hide();
                $("#_video_description").hide();
                $("#_video_tags").hide();
                $("#_video_creator").hide();
                $("#_video_country").hide();
                $("#_video_start_timedate").hide();
                $("#_video_create").hide();
                $("#_video_tv_channel").hide();
                $("#_video_reload").show();
                document.getElementById("new_video").reset();
                
                document.getElementById("_rsp_msg").innerHTML = response.msg;
                
                $("#overlay").hide();
                $("#progressbar").hide();
                $("#progresstext").hide();
            });
        /*
        $.ajax({ // check fields
            url: "/wb_admin/video/check_video_fields",
            type: "POST", 
            headers: {'X-CSRF-TOKEN': token},            
            data: form, 
            contentType: false,       
            cache: false,             
            processData:false,
            dataType: 'json', 
            xhr: function() { // custom xhr (is the best)

               var xhr = new XMLHttpRequest();
               var total = 0;

               // Get the total size of files
               $.each(document.getElementById('_file_').files, function(i, file) {
                      total += file.size;
               });

               // Called when upload progress changes. xhr2
               xhr.upload.addEventListener("progress", function(evt) {
                      // show progress like example
                      var loaded = (evt.loaded / total).toFixed(2)*100; // percent

                    //$('#progress').text('Uploading... ' + loaded + '%' );
                    $( "#progressbar" ).progressbar({
                        'value' : loaded
                    });
               }, false);

               return xhr;
            },  
 
            success: function( response ) {
                $( "#progressbar" ).progressbar({
                    'value' : 0
                });
                $('#progresstext').html('Uploading..');

                document.getElementById("_form_errors").value = "";
                if (response.status == 'error') {
                    document.getElementById("_video_rsp_msg").innerHTML = response.msg;
                    for (var i = 0; i < response.errors.length; i++) {
                        document.getElementById(response.errors[i]).style.border = "thin dotted #FF0000";
                    }
                    document.getElementById("_form_errors").value = response.errors;
                    document.getElementById("_video_rsp_msg").style.color = "#FF0000";
                    $("#overlay").hide();
                    $("#progressbar").hide();
                } else {
                    $.post("/wb_admin/video/get_signature", // signature from vzaar
                        {   
                            "_token": response.token,
                        },
                        function( response ) {
                            console.log(response);

                            var file = document.getElementById('_file_').files[0];
                            
                            var url = 'https://' + response.bucket + '.s3.amazonaws.com/';

                            var data = new FormData();
                            data.append('AWSAccessKeyId' ,response.accesskeyid);
                            data.append('signature', response.signature); 
                            data.append('acl', response.acl); 
                            data.append('key', response.key); 
                            data.append('bucket', response.bucket); 
                            data.append('policy', response.policy); 
                            data.append('success_action_status', '201'); 
                            data.append('file', file, file.name);

                            $.ajax({ // upload to s3
                                url: url,
                                headers: {'x-amz-acl': response.acl},   
                                type: "POST",             
                                data: data, 
                                contentType: false,       
                                cache: false,             
                                processData:false,
                                dataType: 'xml',  
                                xhr: function() { // custom xhr (is the best)

                                   var xhr = new XMLHttpRequest();
                                   var total = 0;

                                   // Get the total size of files
                                   $.each(document.getElementById('_file_').files, function(i, file) {
                                          total += file.size;
                                   });

                                   // Called when upload progress changes. xhr2
                                   xhr.upload.addEventListener("progress", function(evt) {
                                          // show progress like example
                                          var loaded = (evt.loaded / total).toFixed(2)*100; // percent

                                        //$('#progress').text('Uploading... ' + loaded + '%' );
                                        $( "#progressbar" ).progressbar({
                                            'value' : loaded
                                        });
                                   }, false);

                                   return xhr;
                                },  
                                success: function( response )
                                {
                                    $('#progresstext').html('Prosessing..');
                                    var key = $(response).find('Key').text();
                                    var splitkey = key.split('/');
                                    var guid = splitkey[splitkey.length - 2];
                                    
                                    form.append('_guid',guid);

                                    $.ajax({ // process video
                                        url: "/wb_admin/video/process_video",
                                        type: "POST",             
                                        headers: {'X-CSRF-TOKEN': token},            
                                        data: form, 
                                        contentType: false,       
                                        cache: false,             
                                        processData:false,
                                        dataType: 'json',  
                                        success: function( response ) {
                                            $("#_video_type").hide();
                                            $("#_video_file").hide();
                                            $("#_video_title").hide();
                                            $("#_video_description").hide();
                                            $("#_video_tags").hide();
                                            $("#_video_creator").hide();
                                            $("#_video_country").hide();
                                            $("#_video_start_timedate").hide();
                                            $("#_video_create").hide();
                                            $("#_video_tv_channel").hide();
                                            $("#_video_reload").show();
                                            document.getElementById("new_video").reset();
                                            
                                            document.getElementById("_video_rsp_msg").innerHTML = response.msg;
                                            
                                            $("#overlay").hide();
                                            $("#progressbar").hide();
                                            $("#progresstext").hide();
                                        },
                                        error: function(evn) 
                                        {   
                                            alert("Something went wrong! Try again!!");
                                            console.log(evn);
                                            $("#overlay").hide();
                                            $("#progressbar").hide();
                                            $("#progresstext").hide();
                                       }
                                    });
                        
                                },
                                error: function(evn) 
                                {   
                                    alert("Something went wrong! Try again!");
                                    $("#overlay").hide();
                                    $("#progressbar").hide();
                                    $("#progresstext").hide();
                               }
                            });
                        },                
                        'json'
                    );
                }
            }
        });
        */
        //prevent the form from actually submitting in browser
        return false;
    } );
    
////////////////////////
   $( '#_video_publish' ).on( 'change', function() {
        document.getElementById("timepicker").hidden = !document.getElementById("timepicker").hidden;
        document.getElementById("datepicker").hidden = !document.getElementById("datepicker").hidden;
        return true;
    });

////////////////////////
    $( '#_video_normal' ).on( 'click', function() {
        document.getElementById("_video_file").hidden = false;
        document.getElementById("_video_title").hidden = false;
        document.getElementById("_video_description").hidden = false;
        document.getElementById("_video_tags").hidden = false;
        document.getElementById("_video_creator").hidden = false;
        document.getElementById("_video_country").hidden = false;
        document.getElementById("_video_start_timedate").hidden = false; 
        document.getElementById("_video_create").hidden = false;  
        document.getElementById("_video_type").hidden = true;  
        return true;
    } ); 

////////////////////////
    $( '#_video_tv' ).on( 'click', function() {
        document.getElementById("overlay").hidden = false;

        $.get( "/wb_admin/tv/channels", {
            'artist' : 'true',
        }, 
        function( response ) {
            if (response.status != "error") {
                var artists = response.msg;
                var msg = '<select name="_video_tv_channel_select" id="_video_tv_channel_select" form="new_video"><option value="false%%0">Select channel!</option>';
                $.each(artists, function(index,value){
                    var option = '<option name="_video_tv_channel_select" value="' + value['userid'] + '%%' + value['country'] + '">' + value['country'] + ': ' + value['name'] + '</option>';
                    msg = msg.concat(option);
                });
                msg = msg.concat('</select>');
            };
            document.getElementById("_video_tv_channel_div").innerHTML = msg;
            document.getElementById("overlay").hidden = true;
        }, "json");

        document.getElementById("_video_file").hidden = false;
        document.getElementById("_video_tv_channel").hidden = false;
        document.getElementById("_video_title").hidden = false;
        document.getElementById("_video_description").hidden = false;
        document.getElementById("_video_tags").hidden = false;
        document.getElementById("_video_creator").hidden = true;
        document.getElementById("_video_country").hidden = true;
        document.getElementById("_video_start_timedate").hidden = false;  
        document.getElementById("_video_create").hidden = false;  
        document.getElementById("_video_type").hidden = true; 
         $("#_video_publish").prop("checked", false);
        document.getElementById("timepicker").hidden = false;
        document.getElementById("datepicker").hidden = false;

        return false;
    } ); 

////////////////////////
    $( '#_video_creator_wb' ).on( 'click', function() {
        document.getElementById("_video_creator_username").hidden = true;
        return true;
    } );

////////////////////////
    $( '#_video_creator_other' ).on( 'click', function() {
        document.getElementById("_video_creator_username").hidden = false;
        return true;
    } );

////////////////////////
    $( '#_video_creator_username' ).on( 'input', function() {
        document.getElementById("_video_creator_other").value = "other";
        var strg = $(this).val();
        if (strg.length > 2) {
            document.getElementById("overlay").hidden = false;
            $.get( "/wb_admin/user/search", { 
                "_username" : strg, 
            }, 
            function( response ) {
                document.getElementById("_video_creator_username_list").innerHTML = response.msg;
                document.getElementById("overlay").hidden = true;
                $( '#_video_creator_username_list' ).show();     
            }, "json");
        } else {
            document.getElementById("_video_creator_username_list").innerHTML = "";
        }
    } ); 
    
////////////////////////
    $( '#_video_creator_username' ).on( 'click', function() {
        var value = $("#_other_user_id").val();
        if (value > 0) {
            document.getElementById("_video_creator_username").value = "";
            document.getElementById("_other_user_id").value = 0;    
        };
    } );
    
////////////////////////
    $( "#_video_creator_username_list" ).on( "click", "._other_username", function( event ) {
        document.getElementById("_other_user_id").value = $( this ).attr('value');    
        document.getElementById("_video_creator_other").value = $( this ).attr('value');
        document.getElementById("_video_creator_username").value = $( this ).attr('name') + " (" + $( this ).attr('value') + ")";   
        $( '#_video_creator_username_list' ).hide();     
        return false;
    });

////////////////////////
    $( '#_video_reload').on( 'click', function() {
        location.reload(true);
    });

} );