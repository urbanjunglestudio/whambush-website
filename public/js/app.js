$.getScript("/js/browser-deeplink.js", function(){
    deeplink.setup({
        iOS: {
            appName: "whambush",
            appId: "876370933",
        },
        android: {
            appId: "com.whambush.android.client"
        },
    }); 
});

jQuery( document ).ready( function( $ ) {

  
    if (/Mobi/.test(navigator.userAgent)) {
        $('#applink').show();
    }

    $('#applink').on('click',function(){
        var url = $(this).attr('url');
        if(!deeplink.open("whambush://"+url)){
            alert("Whambush is available for iOS and Android");
        }
        return false;
    });

} );