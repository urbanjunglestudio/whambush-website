/*jslint browser: true, nomen: true, unparam: true, sloppy: true*/
/*global $, jQuery*/

$(document).ready(function () {

    var header = $('.clearHeader');
    $(window).scroll(function () {
        var scroll = $(window).scrollTop();

        if (scroll >= 100) {
            header.removeClass('clearHeader').addClass('darkHeader');
        } else {
            header.removeClass('darkHeader').addClass('clearHeader');
        }
    });

});