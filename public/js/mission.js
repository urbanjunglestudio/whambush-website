var timer;

function updateEndTime() {
    
    // var seconds = document.getElementById("mission_ends_at").getAttribute('value');
    // var local = document.getElementById("mission_ends_at").getAttribute('local');
    // var notsoon = document.getElementById("mission_ends_at").getAttribute('notsoon');
    var seconds = $("#mission_ends_at").attr('value');
    var local = $("#mission_ends_at").attr('local');
    var notsoon = $("#mission_ends_at").attr('notsoon');
    var now = new Date();
    var date = new Date(null);
    date.setSeconds(seconds);
    
    var t = Date.parse(date) - Date.parse(now);
    var sec = Math.floor( (t/1000) % 60 );
    var minutes = Math.floor( (t/1000/60) % 60 );
    var hours = Math.floor( (t/(1000*60*60)) % 24 );
    var days = Math.floor( t/(1000*60*60*24) );
    if (days < 0 || hours < 0 || minutes < 0 || seconds < 0) {
        var str = "";
        clearInterval(timer);
        $(".get-app").hide();
        $("#timerrow").hide();
    } else if (days > 31) {
        var str = notsoon ;
        clearInterval(timer);
    } else {
        var str = local + " " + days + "d " + hours +"h "+ minutes +"m "+ sec + "s";
    }
   
    document.getElementById("mission_ends_at").innerHTML = str;
    if (days == 0 && hours == 0) {
        document.getElementById("mission_ends_at").style.color = "#CD554A"; 
    };
}

var page = 1;

jQuery( document ).ready( function( $ ) {

    if ($('#mission_ends_at').length) {
        updateEndTime();
    };
    timer = setInterval(updateEndTime,1000);

    
    var waypoint = new Waypoint({
        element: document.getElementById('load_more_block'),
        handler: function(direction) {
            var videos = parseInt(document.getElementById("mission_videos").getAttribute('value'));
            if (direction === 'down' &&  videos > 0) { 
                $("#spinner").show();
                if (videos < 30) {
                    waypoint.disable();
                };
                var url = window.location.href.split('#')[0] + "/videos/";
                $.get( url, { 
                    "mission_id" : document.getElementById("mission_videos").getAttribute('missionid'), 
                    "order" : "rank",
                    "page" : page,
                }, 
                function( response ) {
                    $("#spinner").hide();
                    $('#mission_videos').append(response.data);
                    document.getElementById("mission_videos").setAttribute('value', parseInt(videos) - parseInt(response.number));
                    page = parseInt(response.next_page);
                    Waypoint.refreshAll();
                }, "json");
            }
        },
        offset: 'bottom-in-view',
    });

   
} );