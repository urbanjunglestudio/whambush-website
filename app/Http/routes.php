<?php

// ------------------------------------------------------------------------ //
// --------------------------- Authentication filter ---------------------- //
// ------------------------------------------------------------------------ //
Route::filter('auth.verybasic', function()
{
    //no authentication for production or local_dev
    if(App::environment() === 'prod' | App::environment() === 'local') return; 
    
    if(Request::getUser() != 'whambush' || Request::getPassword() != 'noaccess'){ //username and password could also come form db :)
        $headers = array('WWW-Authenticate' => 'Basic');
        return Response::make('Unauthorised access denied!', 401, $headers);
    }
});

// ------------------------------------------------------------------------ //
// --------------------------- LOCALIZED ROUTES --------------------------- //
// ------------------------------------------------------------------------ //

Route::group([
    'before' => 'auth.verybasic',
    'prefix' => LaravelLocalization::setLocale(),
], function() {
    // --------------------------- flat pages --------------------------- //
    Route::get('/', [
        'as' => 'home',
        'uses' => 'FlatController@home'
    ]);

    Route::get('/faq', [
        'as' => 'faq',
        'uses' => 'FlatController@faq'
    ]);

    Route::get('/advertise', [
        'as' => 'advertise',
        'uses' => 'FlatController@advertise'
    ]);

    Route::get('/terms-of-service', [
        'as' => 'tos',
        'uses' => 'FlatController@tos'
    ]);

    // --------------------------- videos --------------------------- //
    Route::get('/v/{slug}', [
        'as' => 'video_detail',
        'uses' => 'VideoController@detail'
    ]);
    
    Route::get('/v/{slug}/download', [ 
        'as' => 'video_download',
        'uses' => 'VideoController@download_video'
    ]);
    
    Route::get('/v/{slug}/image.jpg', [ 
        'as' => 'video_download',
        'uses' => 'VideoController@video_thumb'
    ]);
    
    // --------------------------- users --------------------------- //
    Route::get('/u/{username}', [
        'as' => 'user_detail',
        'uses' => 'UserController@detail'
    ]);
    
    // --------------------------- missions --------------------------- //
    Route::get('/singlemission', [
        'as' => 'singlemission',
        'uses' => 'FlatController@singlemission'
    ]);
    Route::get('/m', [
        'as' => 'missions_all',
        'uses' => 'MissionController@allMissions'
    ]);
    Route::get('/m/{slug}', [
        'as' => 'mission_single',
        'uses' => 'MissionController@singleMission'
    ]);
    Route::get('/m/{slug}/image.jpg', [
        'as' => 'mission_single',
        'uses' => 'MissionController@missionImage'
    ]);
    Route::get('/m/{slug}/rules', [
        'as' => 'mission_rules',
        'uses' => 'MissionController@showRules'
    ]);
    Route::get('/m/{slug}/videos', [
        'as' => 'mission_videos',
        'uses' => 'MissionController@getMissionVideos'
    ]);
    // --------------------------- customer portal --------------------------- //
    Route::get('/customer', [
        'as' => 'customer_landing',
        'uses' => 'CustomerController@checkLogin'
    ]);
    Route::get('/customer/register', [
        'as' => 'customer_register',
        function () {
            return view('customer.'.'register');
        }
    ]);
    Route::post('/customer/register/{what}', [
        'as' => 'customer_register',
        'uses' => 'CustomerController@register'
    ]);
    Route::get('/customer/login', [
        'as' => 'customer_login',
        function () {
            return redirect()->away(LaravelLocalization::getLocalizedURL(null,'customer/'));
        }    
    ]);
    Route::post('/customer/login', [
        'as' => 'customer_login',
        'uses' => 'CustomerController@login'
    ]);
    Route::get('/customer/logout', [
        'as' => 'customer_logout',
        'uses' => 'CustomerController@logout'
    ]);
    Route::get('/customer/account', [
        'as' => 'customer_account',
        'uses' => 'CustomerController@account'
    ]);
    Route::get('/customer/missions/new', [
        'as' => 'customer_new_mission',
        'uses' => 'CustomerController@newMission'
    ]);
    Route::get('/customer/missions/new/description', [
        'as' => 'customer_new_mission_description',
        'uses' => 'CustomerController@getDescription'
    ]);
    Route::post('/customer/missions/new', [
        'as' => 'customer_create_mission',
        'uses' => 'CustomerController@createCustomerMission'
    ]);
    Route::get('/customer/missions/current', [
        'as' => 'customer_current_missions',
        'uses' => 'CustomerController@currentCustomerMissions'
    ]);
    Route::get('/customer/missions/handle', [
        'as' => 'customer_missions',
        'uses' => 'CustomerController@customerMissions'
    ]);
    Route::get('/customer/missions/handle/rows/{type}', [
        'as' => 'customer_missions_rows',
        'uses' => 'CustomerController@getMissionRows'
    ]);
    Route::get('/customer/missions/handle/stats', [
        'as' => 'customer_missions_stats',
        'uses' => 'CustomerController@getMissionStats'
    ]);
    Route::get('/customer/missions/handle/edit', [
        'as' => 'customer_missions_get_edit',
        'uses' => 'CustomerController@getEditMission'
    ]);    
    Route::post('/customer/missions/handle/edit', [
        'as' => 'customer_missions_update',
        'uses' => 'CustomerController@updateMission'
    ]);
    Route::get('/customer/missions/handle/videos', [
        'as' => 'customer_missions_videos',
        'uses' => 'CustomerController@getVideoRows'
    ]);
    Route::post('/customer/missions/handle/videos/winner', [
        'as' => 'customer_missions_winner',
        'uses' => 'CustomerController@missionWinner'
    ]);
    Route::get('/customer/missions/handle/videos/winner', [
        'as' => 'customer_missions_winner',
        'uses' => 'CustomerController@missionWinner'
    ]);
});

//redirects to api
Route::any('account/{all}', 
    function ($all) {
        return redirect()->away('https://api.whambush.com/account/'.$all);
    }
)->where(['all' => '.*']);

Route::any('api/{all}', 
    function ($all) {
        return redirect()->away('https://api.whambush.com/'.$all);
    }
)->where(['all' => '.*']);

Route::any('media/{all}', 
    function ($all) {
        return redirect()->away('https://api.whambush.com/media/'.$all);
    }
)->where(['all' => '.*']);

Route::group(['before' => 'auth.verybasic'], function () {
// ------------------------------------------------------------------------ //
// ------------------------- NON-LOCALIZED ROUTES ------------------------- //
// ------------------------------------------------------------------------ //
// --------------------------- flat pages --------------------------- //
/*Route::get('/styleguide', [
    'as' => 'styleguide',
    'uses' => 'FlatController@styleguide'
]);*/

Route::get('/countries', [
    'as' => 'countries',
    'uses' => 'CommonController@getCountries'
]);
Route::post('/isUrl', [
    'as' => 'validate_url',
    'uses' => 'CommonController@isUrl'
]);
Route::get('/translate/{lang}/{key}', [
    'as' => 'translate',
    'uses' => 'CommonController@getTranslation'
]);
Route::get('/hyvis-k15', [
    'as' => 'hyvis',
    'uses' => 'FlatController@hyvis'
]);

Route::get('/hyvis', [
    'as' => 'hyvis2',
    'uses' => 'FlatController@hyvis2'
]);
// --------------------------- app redirection --------------------------- //
Route::get('/android', [
    'as' => 'redirect_android',
    function () {
        return redirect()->away('https://play.google.com/store/apps/details?id=com.whambush.android.client&hl=en');
    }
]);
Route::get('/ios', [
    'as' => 'redirect_ios',
    function () {
        return redirect()->away('https://itunes.apple.com/fi/app/whambush/id876370933?mt=8');
    }
]);

// --------------------------- facebook --------------------------- //
Route::get('/facebook', [
    'as' => 'redirect_facebook',
    function () {
        return redirect()->away('/en/facebook');
    }
]);Route::get('/en/facebook', [
    'as' => 'redirect_facebook_en',
    function () {
        return redirect()->away('https://facebook.com/Whambush');
    }
]);
Route::get('/fi/facebook', [
    'as' => 'redirect_facebook_fi',
    function () {
        return redirect()->away('https://facebook.com/Whambush');
    }
]);
Route::get('/et/facebook', [
    'as' => 'redirect_facebook_et',
    function () {
        return redirect()->away('https://facebook.com/whambushest');
    }
]);

// --------------------------- instagram --------------------------- //
Route::get('/instagram', [
    'as' => 'redirect_instagram',
    function () {
        return redirect()->away('/en/instagram');
    }
]);Route::get('/en/instagram', [
    'as' => 'redirect_instagram_en',
    function () {
        return redirect()->away('https://instagram.com/Whambush');
    }
]);
Route::get('/fi/instagram', [
    'as' => 'redirect_instagram_fi',
    function () {
        return redirect()->away('https://instagram.com/Whambush');
    }
]);
Route::get('/et/instagram', [
    'as' => 'redirect_instagram_et',
    function () {
        return redirect()->away('https://instagram.com/whambusheesti');
    }
]);

// --------------------------- twitter --------------------------- //
Route::get('/twitter', [
    'as' => 'redirect_twitter',
    function () {
        return redirect()->away('/en/twitter');
    }
]);Route::get('/en/twitter', [
    'as' => 'redirect_twitter_en',
    function () {
        return redirect()->away('https://twitter.com/Whambush');
    }
]);
Route::get('/fi/twitter', [
    'as' => 'redirect_twitter_fi',
    function () {
        return redirect()->away('https://twitter.com/Whambush');
    }
]);
Route::get('/et/twitter', [
    'as' => 'redirect_twitter_et',
    function () {
        return redirect()->away('https://twitter.com/WhambushEesti');
    }
]);

// --------------------------- youtube --------------------------- //
Route::get('/youtube', [
    'as' => 'redirect_youtube',
    function () {
        return redirect()->away('/en/youtube');
    }
]);
Route::get('/en/youtube', [
    'as' => 'redirect_youtube_en',
    function () {
        return redirect()->away('https://www.youtube.com/user/whambush/');
    }
]);
Route::get('/fi/youtube', [
    'as' => 'redirect_youtube_fi',
    function () {
        return redirect()->away('https://www.youtube.com/user/whambush/');
    }
]);
Route::get('/et/youtube', [
    'as' => 'redirect_youtube_et',
    function () {
        return redirect()->away('https://www.youtube.com/user/whambush/');
    }
]);


// ------------------------------------------------------------------------ //
// ------------------------- Admin stuff ---------------------------------- //
// ------------------------------------------------------------------------ //

Route::get('/wb_admin', [
    'as' => 'admin_landing',
    'uses' => 'AdminController@checkLogin'
]);


Route::post('/wb_admin/login', [
    'as' => 'admin_login',
    'uses' => 'AdminController@login'
]);
Route::get('/wb_admin/logout', [
    'as' => 'admin_logout',
    'uses' => 'AdminController@logout'
]);


// ------------------------------------------------------------------------ //
Route::get('/wb_admin/mission/new', [
    'as' => 'admin_mission_new',
    'uses' => 'AdminController@mission'
]);
Route::post( '/wb_admin/mission', [
    'as' => 'admin_mission_create',
    'uses' => 'AdminController@create_mission'
]);
Route::get( '/wb_admin/mission', array(
    'as' => 'admin_mission_get',
    function () {
        return redirect()->away('/wb_admin/mission/new');
    }
) );
// ------------------------------------------------------------------------ //
Route::get('/wb_admin/winners', [
    'as' => 'admin_winners_get',
    'uses' => 'AdminController@winners'
]);
Route::post('/wb_admin/winners', [
    'as' => 'admin_winners_post',
    'uses' => 'AdminController@get_winners'
]);
Route::post('/wb_admin/bananas', [
    'as' => 'admin_bananas_post',
    'uses' => 'AdminController@give_bananas'
]);
Route::post('/wb_admin/sendmail', [
    'as' => 'admin_mail_post',
    'uses' => 'AdminController@send_mail'
]);
// ------------------------------------------------------------------------ //
Route::get('/wb_admin/video/new', [
    'as' => 'admin_video_new',
    'uses' => 'AdminController@video'
]);
Route::post('/wb_admin/video', [
    'as' => 'admin_video_post',
    'uses' => 'AdminController@create_video'
]);
Route::get( '/wb_admin/video', array(
    'as' => 'admin_video_get',
    function () {
        return redirect()->away('/wb_admin/video/new');
    }
) );
Route::post( '/wb_admin/video/get_signature', array(
    'as' => 'video_signature_post',
    'uses' => 'AdminController@get_signature'
) );
Route::post( '/wb_admin/video/process_video', array(
    'as' => 'video_process_post',
    'uses' => 'AdminController@process_video'
) );
Route::post( '/wb_admin/video/check_video_fields', array(
    'as' => 'video_check_fields_post',
    'uses' => 'AdminController@check_video_fields'
) );
// ------------------------------------------------------------------------ //
Route::get('/wb_admin/user/search', [
    'as' => 'admin_user_search_get',
    'uses' => 'AdminController@search_user'
]);
Route::get('/wb_admin/tv/channels', [
    'as' => 'admin_tv_channels_get',
    'uses' => 'AdminController@get_tv_channels'
]);

}); //group

