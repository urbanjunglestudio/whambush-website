<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use LaravelLocalization;
use Mandrill;
use Vzaar;

class AdminController extends BaseApiController {

    public function __call($func, $params)
    {        
        session(['progress' => 0]);
        if ( session('is_admin') ) {//$this->authenticate( session('username'), session('password') ) ) {
            return view('wb_admin.'.$func);
        } else {
            return redirect()->away('/wb_admin');
        }
    }

    public function checkLogin() {
        if ( $this->authenticate( session('username'), session('password') ) ) {
            if (session('is_admin')) {
                return view( 'wb_admin.admin', [
                    'logged' => 1,
                    'msg' => 'Welcome!'
                ]);
            }
        } 
        parent::logout();
        return view( 'wb_admin.admin', [
            'logged' => 0,
            'msg' => 'Please login!'
        ]);
     }
    
    public function logout() {
        parent::logout();
        return redirect()->away('/wb_admin');
    }
    
    public function login(Request $request) {
        if ($request->input('_token') == session('_token')){
            if ( $this->authenticate( $request->input('username'), $request->input('password'), true) ) {
                session([
                    'username' => $request->input('username'), 
                    'password' => $request->input('password'),
                ]);
                if (session('is_admin')) {
                    return redirect()->away('/wb_admin');
                }
            } 
        }
        parent::logout();
        return view( 'wb_admin.admin', [
            'logged' => false,
            'msg' => 'Username and/or password incorrect. Try again!'
        ]);
     }
    
 
/*
 *
 *  new mission code
 *
 */    
    public function create_mission(Request $request) {
        if (!session('is_admin')) {
            abort(404);
        }
        if ($request->input('_token') != session('_token')){
            $response = array(
                'status' => 'error',
                'msg' => 'Error: Old form, reload page!',
                'errors' => array() , 
            );
            return json_encode($response);        
        }
        
        date_default_timezone_set("UTC");

        $fields['normal'] = array(
            "_mission_title",
            "_mission_description",
            "_mission_creator",
            "_mission_country",
            "_mission_start_date",
            "_mission_duration");
        
        $fields['icon'] = array(
            "_mission_icon_name",
            "_mission_title",
            "_mission_description",
            "_mission_creator",
            "_mission_country",
            "_mission_start_date",
            "_mission_duration");

        $type = $request->input('_mission_type');
        $errors = array();

        foreach ($fields[$type] as $field) {
            if (!(strlen($request->input($field)) > 0)) {
                $errors[] = $field; 
            }        
        }    
        
        if (count($errors) > 0) {
            $response = array(
                'status' => 'error',
                'msg' => 'Error: Empty fields',
                'errors' => $errors , 
            );
            return json_encode($response);
        }
        $country = $request->input('_mission_country');
        
        /*//set the start and end times
        $time_in_seconds = $request->input('_mission_start_date')/1000;
        if ($country == "FI" || $country == "EE") {
            $localdate = date("15:00:00 d.m.Y ",$time_in_seconds).$this->getTimeZone($country);
        } else { //global
            $localdate = date("12:00:00 d.m.Y ",$time_in_seconds).$this->getTimeZone($country);
        }
        $i = 0;
        if (strtotime($localdate) < time()) {
            $i = 1;
            $newtime = strtotime($localdate);
            while ($newtime < time()) {
                $newtime = strtotime($localdate) + 60*60*$i;
                $i++;
            }
            $i--;
        } else {
            $newtime = strtotime($localdate);
        }
*/
        $start_at = date("Y-m-d\TH:i:s",$request->input('_mission_start_date')/1000);//$newtime);
        $end_at = date("Y-m-d\T12:00:00",strtotime($start_at."+".$request->input('_mission_duration')."days"));
        
        $rsp_msg = "";
        
        if ($type == "icon") {
            // call API
            $rsp = $this->doPOST('missions', [], [
                'json' => [
                    'mission_type' => 1,         
                    'icon_name' => $request->input('_mission_icon_name'),         
                    'name' => $request->input('_mission_title'), 
                    'description' => $request->input('_mission_description'), 
                    'country' => $request->input('_mission_country'),
                    'start_at' => $start_at, 
                    'end_at' => $end_at, 
                    'added_by' => $request->input('_mission_creator'),
                    'linked_video' => $request->input('_mission_linked_file_id'),
                ]
            ]);
            $rsp_msg .= '<br>Video link: <a href="'.$rsp['linked_video']['web_url'].'" target="_blank">'.$rsp['linked_video']['web_url'].'</a>';
            $rsp_msg .= '<br>- Video download link: <a href="'.$rsp['linked_video']['web_url'].'/download/?id='.$rsp['linked_video']['external_id'].'" target="_blank">'.$rsp['linked_video']['web_url'].'/download/?id='.$rsp['linked_video']['external_id'].'</a>';
        } else {
            // call API
            $rsp = $this->doPOST('missions', [], [
                'json' => [
                    'mission_type' => 0,
                    'name' => $request->input('_mission_title'), 
                    'description' => $request->input('_mission_description'), 
                    'country' => $request->input('_mission_country'),
                    'start_at' => $start_at, 
                    'end_at' => $end_at, 
                    'added_by' => $request->input('_mission_creator'),
                ]
            ]);
        }
        $rsp_msg .= '<br>Mission link: <a href="'.$rsp['mission_url'].'" target="_blank">'.$rsp['mission_url'].'</a>';

        $response = array(
            'status' => 'ok',
            'msg' => 'Mission created succesfully!'.$rsp_msg,
        );
        
        return json_encode($response);
    }

/*
 *
 *  Winners code
 *
 */    

    public function get_winners(Request $request) {
        if (!session('is_admin')) {
            abort(404);
        }
        if ($request->input('_token') != session('_token')){
            $response = array(
                'status' => 'error',
                'msg' => 'Error: Old form, reload page!',
            );
            return json_encode($response);        
        }
        $defaults['normal'] = array (
            'default' => 1,
            'all' => 0,
            'type' => 0,
        );
        $defaults['icon'] = array (
            'default' => 10,
            'all' => 0,
            'type' => 1,
        );

        $mission_type = $request->input('_mission_type');
        $number_of_entries = $request->input('_number_of_entries');
        if (!is_numeric($number_of_entries)) {
            $number_of_entries = $defaults[$mission_type][$number_of_entries];
        }
        $number_of_missions = $request->input('_number_of_missions');
        $mission_country = $request->input('_mission_country');
        if ($mission_country == "ZZ") {
            LaravelLocalization::setLocale(getLanguageWithCountry("EN"));
        } else {
            LaravelLocalization::setLocale(getLanguageWithCountry($mission_country));
        }

        $print_emails = $request->input('_print_emails');
        $download_links = $request->input('_download_links');
        $icon_friendly = $request->input('_icon_friendly');
        $mail_friendly = $request->input('_mail_friendly');

        $rsp = $this->doGET('missions',[], ['type' => 'old', 'country' => $mission_country, 'page_size' => 50]);
        $msg = "";
        $num_missions = 0;
        for ($i = 0; $i < count($rsp['results']); $i++) {
            if ($rsp['results'][$i]['mission_type'] === $defaults[$mission_type]['type'] && $number_of_missions > $num_missions) {
                $msg .= "<div class='mission_line'>";
                $msg .= $this->get_mission_string($rsp['results'][$i], $number_of_entries, $print_emails, $download_links, $icon_friendly, $mail_friendly);
                $msg .= "</div>";
                $num_missions++;
            } 
        }
        if ($mission_type == 'normal') {
            $mailt = 'admin.WINNER_MAIL_TMPL_1';
        } else {
            $mailt = 'admin.WINNER_MAIL_TMPL_2';
        }
        $template = array(
            'subject' => trans('admin.WINNER_MAIL_SUBJECT'), 
            'message' => trans($mailt),
        );
        
        $response = array(
            'status' => 'ok',
            'msg' => $msg,
            'template' => $template,
        );
        
        return json_encode($response);
    }

    private function get_mission_string($data,$entries,$mails,$dl,$friendly,$mailfriendly)
    {
        if (!$data['is_videos_ranked']) {
            return "<a href='#' class='mission_dialog' name='".htmlentities($data['name'],ENT_QUOTES)."' value='".htmlentities($data['description'])."'>".$data['name']." - ".$data['id']."</a> - Not yet ranked. Try again later.<br>";
        }
        $page_size = ($entries > 0) ? $entries : 100;
        $order = ($data['mission_type'] === 1) ? "random" : "rank";
        $rsp = $this->doGET('search',['what' => 'videos'], ['mission' => $data['id'], 'order' => 'rank' , 'page_size' => $page_size]);
        if ($rsp['count'] < 1) {
            return "<a href='#' class='mission_dialog' name='".htmlentities($data['name'],ENT_QUOTES)."' value='".htmlentities($data['description'])."'>".$data['name']." - ".$data['id']."</a> - No entries<br>";
        }
        $results = $rsp['results'];
        if ($entries == 0 && (count($rsp['results']) < $rsp['count'])) {
            $page = 2;
            while (count($rsp['next']) > 0) {
                $rsp = $this->doGET('search',['what' => 'videos'], ['mission' => $data['id'], 'order' => 'rank' , 'page' => $page, 'page_size' => $page_size]);
                $results = array_merge($results, $rsp['results']);
                $page++;
            }
        }
        $rank = 1;
        
        if ($data['mission_type'] === 1) {
            shuffle($results);
        }

        $msg = "<a href='#' class='mission_dialog' name='".htmlentities($data['name'],ENT_QUOTES)."' value='".htmlentities($data['description'])."'>".$data['name']." - ".$data['id']."</a><br>";
        foreach ($results as $value) {
            $bananas = (($rank === 1 || $data['mission_type'] === 1) && !$friendly && !$mailfriendly) ? ", <a href='#' class='send_bananas' id='banana_".$value['id']."' value='".$value['added_by']['id']."'> Give bananas</a>" : "";
            $winner = (($rank === 1 || $data['mission_type'] === 1) && !$friendly && !$mailfriendly) ? ", <a href='#' class='send_mail' id='mission_".$data['id']."_".$value['id']."' value='".htmlentities($data['name'],ENT_QUOTES)."%%".$value['added_by']['username']."%%".$value['added_by']['id']."'> Send mail to winner</a>" : "";
            $mail = ($mails && !$friendly) ? ", ".$value['added_by']['email'] : "";
            $dlurl = parse_url($value['web_url'], PHP_URL_PATH);
            $dlink = ($dl && !$friendly && !$mailfriendly) ? ", <a href='".asset($dlurl)."/download?id=".$value['external_id']."''>Download video</a>" : "";
            $vlink = ($friendly || $mailfriendly) ? $value['web_url'] : "View video";
            if ($value['added_by']['activation_state'] != "ACTIVATED") {
                $msg .= "<del>";
            }
            if ($data['mission_type'] !== 1) {
                $msg .= $rank.". ";
            }

            $msg .= $value['added_by']['username'].$mail.", <a href='".$value['web_url']."' target='_blank'>".$vlink."</a>".$dlink.$winner.$bananas."<br>";
            if ($value['added_by']['activation_state'] != "ACTIVATED") {
                $msg .= "</del>";
            }
            $rank++;
        }
        return $msg;
    }
    
/*
 *
 *  banana code
 *
 */    
    public function give_bananas(Request $request) {
        if (!session('is_admin')) {
            abort(404);
        }

        $user = $request->input('_user');
        $amount = $request->input('_amount');
        
        if (!is_numeric($amount) || $amount < 1) {
            $response = array(
                'status' => 'Fail!',
                'msg' => "Amount needs to be a positive number!",
            );
            return json_encode($response);
        }

        $rsp = $this->doPOST('bananas', [], [
            'json' => [
                'user' => $user,
                'amount' => $amount, 
            ]
        ]);
        $amount = $rsp['amount'];

        $msg = ($amount > 1) ? "User got ".$amount." bananas!" : "User got single banana!";
        $response = array(
            'status' => 'Success!',
            'msg' => $msg,
        );
        
        return json_encode($response);
    }
    
    public function send_mail(Request $request) {
        if (!session('is_admin')) {
            abort(404);
        }

        $userid = $request->input('_user');
        $subject = $request->input('_subject');
        $message = nl2br($request->input('_message'));
        
        $rsp = $this->doGET('user',['id' => $userid], []);
        
        $email = $rsp['email'];
        $username = $rsp['username'];

        $mandrill = new Mandrill('4r9gM2uJxPtYygEbgw8AaQ');

        $template_name = 'MISSIONWINNER_TMPL';

        $message = nl2br($request->input('_message'));

        $from = 'reward@whambush.com';

        try {
            $message = array(
                'subject' => $subject,
                'from_email' => $from,
                'from_name' => 'Whambush',
                'to' => array(
                    array(
                        'email' => $email,
                        'type' => 'to'
                    )
                ),
                'important' => false,
                'track_opens' => true,
                'track_clicks' => true,
                'auto_text' => true,
                'auto_html' => false,
                'inline_css' => false,
                'url_strip_qs' => false,
                'preserve_recipients' => true,
                'view_content_link' => true,
                'bcc_address' => 'jari@whambush.com',
                'merge' => true,
                'merge_language' => 'mailchimp',
                'global_merge_vars' => array(
                    array(
                        'name' => 'SUBJECT',
                        'content' => $subject
                    ),
                    array(
                        'name' => 'WINNER_CONTENT',
                        'content' => $message
                    ),
                ),
                'tags' => array('mission_winner'),
                'google_analytics_domains' => array('whambush.com'),
                'recipient_metadata' => array(
                    array(
                        'rcpt' => $email,
                        'values' => array('username' => $username)
                    )
                ),
            );
            $async = false;
            $ip_pool = 'Main Pool';
            $send_at = '';

            $result = $mandrill->messages->sendTemplate($template_name, [], $message, $async, $ip_pool, $send_at);

            if (isset($result['reject_reason'])) {
                $response = array(
                    'status' => 'FAIL!',
                    'msg' => $result['reject_reason'],
                );
            } else { 
                $response = array(
                    'status' => 'Success!',
                    'msg' => "Mail sent!",
                    //'msg' => json_encode($result),
                );
            }

            return json_encode($response);

        } catch(Mandrill_Error $e) {
            // Mandrill errors are thrown as exceptions
            $msg = 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
            // A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
            //throw $e;
            $response = array(
                'status' => 'FAIL!',
                'msg' => $msg,
            );
            
            return json_encode($response);
        }
    }

/*
 *
 *  video code
 *
 */    
    public function create_video(Request $request) {
        return;
    }

    public function check_video_fields(Request $request) {
        if ($request->input('_token') === null) {
            $response = array(
                'status' => 'error',
                'msg' => json_encode($request->all()),
                'errors' => array(),
            );
            return json_encode($response);
        }


        if (!session('is_admin')) {
            abort(404);
        }

        //$file = $request->file('_video_file');

        $fields['normal'] = array(
            "_video_title",
            "_video_creator",
            "_video_country",
        );
        
        $fields['wbtv'] = array(
            "_video_title",
            "_video_tv_channel_select",
            "_video_country",
        );
        
        $type = $request->input('_video_type');
        $video_type = ($type === "wbtv") ? 2 : 0;

        $errors = array();

        foreach ($fields[$type] as $field) {
            if (!(strlen($request->input($field)) > 0)) {
                $errors[] = $field; 
            }        
        } 
        if ($request->input('_video_creator') === "other") {
            $errors[] = '_video_creator'; 
        }
        if ($request->input('_video_tv_channel_select') === "false" && $type === "wbtv") {
            $errors[] = '_video_tv_channel'; 
        }

        if ($request->input('_video_start_timedate') !== "now") {
            if (!( strlen($request->input('_video_start_date')) > 0  && strlen($request->input('_video_start_time')) > 0 )) {
                $errors[] = '_video_start_timedate'; 
            }
        } 

        if (count($errors) > 0) {
            $response = array(
                'status' => 'error',
                'msg' => 'Error: Empty/errors in fields',
                'errors' => $errors , 
            );
            return json_encode($response);
        }
        
        $response = array(
            'status' => 'Success!',
            'token' => $request->input('_token'),
        );

        return json_encode($response);
    }
    
    public function get_signature(Request $request) {
        Vzaar::$token = session('vzaar_key');
        Vzaar::$secret = session('vzaar_secret');
        $signature = Vzaar::getUploadSignature();
    
        return json_encode($signature['vzaar-api']);
    }

    public function process_video(Request $request) {
        $guid = $request->input('_guid');

        Vzaar::$token = session('vzaar_key');
        Vzaar::$secret = session('vzaar_secret');
        
        $type = $request->input('_video_type');
        $video_type = ($type === "wbtv") ? 2 : 0;

        $apiarray = array(
            'name' => html_entity_decode($request->input('_video_title')),
            'description' => html_entity_decode($request->input('_video_description')), 
            'tags' => $request->input('_video_tags'), 
            'video_type' => $video_type, 
        );

        if ($request->input('_video_country') !== "default") {
            $apiarray['country'] = $request->input('_video_country');
            /*$country = $apiarray['country'];
        } else {
            $country = session('user')['country']['country'];*/
        }

        if ($type === "wbtv") {
            $apiarray['added_by'] = $request->input('_video_tv_channel_select');
        } else {
            if ($request->input('_video_creator') !== "current") {
                $apiarray['added_by'] = $request->input('_video_creator');
            }
        }

        date_default_timezone_set("UTC");

        if ($request->input('_video_start_timedate') !== "now") {
            $published_at = date("Y-m-d H:i:s",($request->input('_parsed_time')/1000));
            $apiarray['published_at'] = $published_at; 
        }
        
        $vzaar_video_title = "w(".session('username').") ".htmlentities($request->input('_video_title'),ENT_QUOTES)."-WHAMBUSH";

        $apiarray['external_id'] = Vzaar::processVideo($guid, $vzaar_video_title, "www.WHAMBUSH.com", "", 6);
        
        $rsp = $this->doPOSTasADMIN('videos', [], ['json' => $apiarray]);

        $response = array(
            'status' => 'Success!',
            'msg' => /*json_encode($apiarray).json_encode($rsp),*/'Video created with id: '.$rsp['id'].'<br>Link: '.$rsp['web_url'],
            'videoid' => $rsp['id'],
        );
        return json_encode($response);
   }
    
 /*
 *
 *  search user
 *
 */    
    public function search_user(Request $request) {
        if (!session('is_admin')) {
            abort(404);
        }
        $username = $request->input('_username');
        if (count($username) < 1) {
            abort(404);   
        }
        $rsp = $this->doGET('search', ['what' => 'users'], ['query' => $username]);
        $msg = "";
        foreach ($rsp['results'] as $value) {
            if (!$value['is_guest']) {
                $msg .= '<li><a href="#" class="_other_username" value="'.$value['id'].'" name="'.$value['username'].'">'.$value['username'].'</a></li>';
            }
        }
        
        $response = array(
            'status' => 'Success!',
            'msg' => $msg,
        );
        return json_encode($response);
    }
 
 /*
 *
 *  tv channels
 *
 */    
    public function  get_tv_channels(Request $request) {
        if (!session('is_admin')) {
            abort(404);
        }
        if ($request->input('artist') !== null) {
            $artists_only = true;
        } else {
            $artists_only = false;
        }

        $rsp = $this->doGET('channels', [], ['page_size' => 50]);

        $msg = $this->parseChannels($rsp['results'],$result);

        if ($artists_only) {
            foreach ($result as $value) {
                if ($value['channel_type'] === 1) {
                    parse_str(parse_url($value['videos_endpoint'],PHP_URL_QUERY));
                    $value['userid'] = $user;
                    $value['country'] = $country;
                    $msg[] = $value;
                }
            }
        } else {
            $msg = $result;
        }
        

        $response = array(
            'status' => 'Success!',
            'msg' => $msg,
        );
        //print_r($msg);
        return json_encode($response);
    }

    private function parseChannels($data,&$r = null) {
        if(is_array($data)) {
            foreach ($data as $value) {
                if (count($value['children']) > 0) {
                    $newvalue = $this->parseChannels($value['children'],$r);
                    if (count($newvalue) > 0) {
                        $r[] = $newvalue;
                    }
                } 
                unset($value['children']);
                $r[] = $value;
            }
        } 
        //print_r($data);
        //return $result;
    }

/*
 *
 *  timezone
 *
 */    
   private function getTimeZone($country) {
        if ($country == "FI" || $country == "EE") {
            $timezone = "Europe/Helsinki"; //"2013-10-30T17:26:00"
        } else { //global
            $timezone = "UTC";
        }
        return $timezone;
    }

//end of class
}
