<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use LaravelLocalization;
use Config;

class MissionController extends BaseApiController {

    public function showRules(Request $request, $slug) {
        $mission = $this->doGET('mission', ['slug' => $slug]);
        
        $paths = explode("/",$_SERVER['REQUEST_URI']);
        if (strcmp($paths[1],"m") == 0) {
            LaravelLocalization::setLocale(getLanguageWithCountry($mission['country']));
        }
        
        $isIOS = $request->has('iOS') ? true : false;
        $content = "<h1>".strtoupper(trans('rules.PAGE_TITLE'))."</h1>";

        $mission_name = $mission['name'];
        $username = $mission['added_by']['username'];
        
        $sections = 1;
        while(strcmp(trans('rules.SECTION_'.$sections.'_TITLE'), 'rules.SECTION_'.$sections.'_TITLE') != 0) {
            $content .= "<p>";
            $content .= "<h2>".strtoupper(trans('rules.SECTION_'.$sections.'_TITLE'))."</h2>";
            $subsec = 1;
            while(strcmp(trans('rules.SECTION_'.$sections.'_'.$subsec), 'rules.SECTION_'.$sections.'_'.$subsec) != 0) {
                if ($isIOS && strcmp(trans('rules.SECTION_'.$sections.'_'.$subsec.'_IOS'), 'rules.SECTION_'.$sections.'_'.$subsec.'_IOS') != 0){
                    $orgtext = trans('rules.SECTION_'.$sections.'_'.$subsec.'_IOS');
                } else {
                    $orgtext = trans('rules.SECTION_'.$sections.'_'.$subsec);
                }
                $text = str_replace('%%MISSIONPAGE%%', '<a href="'.$mission['mission_url'].'">'.trans('rules.MISSIONPAGE').'</a>', $orgtext);
                $text = str_replace('%%USERNAME%%', '<a href="/u/'.$username.'">'.$username.'</a>', $text);
                $text = str_replace('%%TERMS%%', '<a href="/terms-of-service">'.trans('common.terms').'</a>', $text);
                $text = str_replace('%%SUPPORT%%', '<a href="mailto:'.trans('rules.SUPPORT').'">'.trans('rules.SUPPORT').'</a>', $text);
                $text = str_replace('%%FAQ%%', '<a href="/faq">'.trans('common.faq').'</a>', $text);
                $content .= $text."<br>";
                $subsec++;
            }
            $content .= "</p>".PHP_EOL;
            $sections++;
        }
        
        if ($isIOS) {
            return view('mission.apprules',[
                'mission_name' => $mission_name,
                'content' => $content,
            ]);
          } else {
            return view('mission.rules',[
                'mission_name' => $mission_name,
                'content' => $content,
            ]);
        }
    }

    public function missionImage(Request $request, $slug)
    {

        $mission = $this->doGET('mission', ['slug' => $slug]);

        if (strlen($mission['added_by']['profile_picture']) > 0 && array_key_exists('share',$request->all())) {
            $image = imagecreatetruecolor(1138, 640);
            $first = imagecreatefrompng(asset('/img/wb_grad.png'));
            if(exif_imagetype($mission['added_by']['profile_picture']) == IMAGETYPE_PNG) {
                $second  = imagecreatefrompng($mission['added_by']['profile_picture']);
            } else {
                $second  = imagecreatefromjpeg($mission['added_by']['profile_picture']);
            }
            
            $size = getimagesize($mission['added_by']['profile_picture']);
            
            imagealphablending($image, true);
            imagesavealpha($image, true);
           
            imagecopyresized($image, $second, 498, 0, 0, 0, 640, 640, $size[0], $size[1]); 
            imagecopy($image, $first, 0, 0, 0, 0, 1138, 640); 

            header('Content-Type: image/png');
            imagepng($image);

            imagedestroy($first);
            imagedestroy($second);
            imagedestroy($image);
        } else {
            if (count($mission['linked_video']) > 1 && $mission['linked_video']['is_processed'] == 0) {
                $file = asset('/img/video_not_ready.jpg');
            } else if (strlen($mission['mission_image_1']) > 1) {
                $file = $mission['mission_image_1'];
            } else if(count($mission['linked_video']) > 1) {
                $file = $mission['linked_video']['thumbnail_url'];
            } else {
                if ($mission['added_by']['id'] === 1) {
                    $file = asset('/img/fb.jpg');
                } else {
                    $file = asset('/img/mission_img.jpg');
                }
            }
            header("Content-type: " . image_type_to_mime_type(exif_imagetype($file)));

            readfile($file);
        }
    }

    public function singleMission($slug)
    {
        $mission = $this->doGET('mission', ['slug' => $slug]);
        $description = nl2br(urlize($mission['description']));

        if ($mission['added_by']['profile_picture'] == "") {
            $added_by_profile_picture =  asset('/img/minion.png');
        } else {
            $added_by_profile_picture = $mission['added_by']['profile_picture']; 
        }  
       
        $banana = $mission['like_count'] - $mission['dislike_count'];
        
        if (is_null($mission['linked_video'])) {
        //    $image = 'http://whambush.com/img/fb.jpg';
            $video = false;
        //    $banana = $mission['like_count'] - $mission['dislike_count'];
        } else {
            $video = $mission['linked_video'];
        //    if ($mission['mission_image_1'] == "") {
        //        $image = $mission['linked_video']['thumbnail_url']; 
         //       $banana = $video['like_count'] - $video['dislike_count'];               
            //} else {
         //       $image = $mission['mission_image_1'];
         //       $banana = $mission['like_count'] - $mission['dislike_count'];
        //    }
        }
        $image = $mission['mission_url']."/image.jpg";
        $mission_end_time = strtotime($mission['end_at']);
        
        if ($banana < 0) {
            $banana_css = "icon-shit";     
        } else {
            $banana_css = "icon-banana";
        }
        
        $paths = explode("/",$_SERVER['REQUEST_URI']);
        if (strcmp($paths[1],"m") == 0) {
            LaravelLocalization::setLocale(getLanguageWithCountry($mission['country']));
        }

        return view('mission.detail', [
            'mission' => $mission,
            'mission_title' => $mission['name'],
            'mission_description' => $description,
            'mission_image' => $image,
            'mission_end_time' => $mission_end_time,
            'number_of_videos' => $mission['number_of_submissions'],
            'banana' => $banana,
            'banana_css' => $banana_css,
            'added_by_profile_picture' => $added_by_profile_picture,
            'video' => $video,
            'slug' => $slug,
        ]);
    }
    
    public function getMissionVideos(Request $request, $slug)
    {
        $mission = $this->doGET('mission', ['slug' => $slug]);
        $id = $request->input('mission_id');
        if ($mission['id'] != $id) {
            abort(404);
        }
        if ($request->has('order')) {
            $order = $request->input('order');
        } else {
            $order = 'rank';    
        }
        $page = (array_key_exists('page',$request->all())) ? $request->input('page') : 1 ;
        $page_size = (array_key_exists('page_size',$request->all())) ? $request->input('page_size') : 30 ;
        
        $all = $this->doGET('search', ['what' => 'videos'], ['mission' => $id, 'order' => $order, 'page' => $page, 'page_size' => $page_size]);
        
        if (strlen($all['next']) > 5) {
            $page = $page + 1;
        }
        $data = "";
        foreach ($all['results'] as $value) {
            $bananas = $value['like_count'];
            $shit = $value['dislike_count'];
            $banana_icon = "icon-banana";
            $shit_icon = "icon-shit";

            $data .= "<div class='col-sm-6 col-md-4'>";
            $data .= "    <div class='video-thumb'>";
            $data .= "        <div class='video-thumb-content' style='background-image: url(\"".$value['thumbnail_url']."\");'>"; 
            $data .= "                <a href='".$value['web_url']."'><div class='container-fluid video-thumb-overlay'>";
            $data .= "                    <div class='col-xs-3'>";
            $data .= "                        <span class='".$banana_icon."'><span style=\"font-size: 28px;\">".abs($bananas)."</span></span>";
            $data .= "                        <span class='".$shit_icon."'><span style=\"font-size: 28px;\">".abs($shit)."</span></span>";
            $data .= "                    </div>";
            $data .= "                    <div class='col-xs-6'>";
            $data .= "                       <span class='icon-play_video'></span>";
            $data .= "                    </div>";
            $data .= "                    <div class='col-xs-3'>";
            $data .= "                        <span class='icon-comment'></span>";
            $data .= "                        <p>0</p>";
            $data .= "                    </div>";
            $data .= "                </div></a>";
            $data .= "            </div>";
            $data .= "        </div>";
            $data .= "        <div class='video-grid-desc'><a href='/u/".$value['added_by']['username']."'>".$value['added_by']['username']."</a></div>";
            $data .= "    </div>";
            $data .= "</div>";
        }
       
        return json_encode([
            'videos' => $all['results'],
            'next_page' => $page,
            'total' => $all['count'],
            'number' => count($all['results']),
            'data' => $data,
        ]);
    }

    public function allMissions(Request $request)
    {
        $active = (array_key_exists('old',$request->all())) ? false : true;
        
        if ($active) {
            $missions = $this->doGET('missions', [], ['page_size' => 50]);
        } else {
            $country = (array_key_exists('country',$request->all())) ? explode(",",$request->input('country')) : array();
            if (count($country) > 0) {
                $missions = array('results' => array());
                foreach ($country as $key => $value) {
                    $all = $this->doGET('missions', [], ['type' => 'old','country' => strtoupper($value),'page_size' => 30]);
                    //print_r($all);
                    foreach ($all['results'] as $value) {
                        $missions['results'][] = $value;
                    }
                }
            } else {
                $missions = $this->doGET('missions', [], ['type' => 'old','country' => '','page_size' => 20]);
            }
        }
        //print_r($missions);
        foreach ($missions['results'] as $mission) {
            $results[$mission['country']][] = $mission;
        }
        

        $countries = $this->doGET('countries', [], ['page_size' => 200]);
        foreach ($countries['results'] as $country) {
            if ($country['supported'] && isset($results[$country['country']])) {
               $supported_countries[$country['country']] = $country; 
            }
        }
        
        if (array_key_exists('latest',$request->all())) {
            $country = (array_key_exists('c',$request->all())) ? strtoupper($request->input('c')) : strtoupper(LaravelLocalization::getCurrentLocale());
            if (!array_key_exists($country, $supported_countries)) {
                $country = "ZZ";
            }
            $amount = (array_key_exists('n',$request->all())) ? $request->input('n') : 1;
            $array = $results[$country];
            if (array_key_exists('order', $request->all())) {
                if ($request->input('order') == "random") {
                    if (count($array) > 1) {
                        $null = array_pop($array);
                    }
                    shuffle($array);
                }
            }
            return json_encode($this->get_mission_html(array_slice($array,0,$amount), $country));
        }

        if (isset($results['ZZ'])) {
            $supported_countries['ZZ'] = array(
                'name' => 'Global',
                'country' => 'ZZ',
                'flag_url' => Config::get('api.base_url').'/static/img/country_flags/ZZ.png',
                'flag_banner_url' => Config::get('api.base_url').'/static/img/country_flags/ZZ_banner.png',
            );
        }

        return view('missions.detail', [
            'missions' => $results,
            'countries' => $supported_countries,
        ]);
    }
    
    public function  get_mission_html($result, $country) {
        if ($country == "FI" || $country == "EE") {
            date_default_timezone_set("Europe/Helsinki");
        } else {
            date_default_timezone_set("UTC");
        }
        $html = "";
        foreach ($result as $value) {
            $description = substr($value['description'],0,150)."...";
            $html .= '<div class="mission_box_details" style="padding-top: 25px;background:url('.Config::get('api.base_url').'/static/img/country_flags/'.$value['country'].'_banner.png'.'), linear-gradient(to top, rgba(0,0,0,0) , rgba(0,0,0,0.8)); ">';  
            $html .= '<a href="'.asset('/m').'" style="color : white"><div class="mission_box_name"><h2><a href="'.asset('/m').'" style="color : white">'.$value['name'].'</h2></div></a>';  
            $html .= '<div class="mission_box_description">'.nl2br(urlize($description)).'</div>';  
            $html .= '<div class="mission_box_ends small">'.trans('common.ends').' '.date("H:i \a\\t d.n.Y T",strtotime($value['end_at']."UTC")).'</div>';  
            $html .= '<img class="mission_box_flag" src="'.Config::get('api.base_url').'/static/img/country_flags/'.$value['country'].'.png">';
            $html .= '</div>';  
        }
        return $html;
    }

}
