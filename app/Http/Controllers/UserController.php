<?php namespace App\Http\Controllers;

use LaravelLocalization;

class UserController extends BaseApiController {

    public function detail($username)
    {
        $result = $this->doGET('search', ['what' => 'users'], ['query' => $username, 'fields' => 'username']);
        if (isset($result['results'][0])) {
            $user = $result['results'][0];
        } else {
            abort(404);
        }
        
        if ($user['activation_state'] == 'DEACTIVATED') {
            abort(404);
        }

        // turn URLs into HTML hyperlinks
        $user['description_html'] = nl2br(urlize($user['description']));

        // get videos
        if ($user['num_videos'] > 0) {
            $response = $this->doGET('search',['what' => 'videos'],['user' => $user['id']]);
            $videos = $response['results'];
        } else {
            $videos = array();
        }

 
        //users profile picture
        if ($user['profile_picture'] == "") {
            $profile_picture =  asset('/img/minion.png');
        } else {
            $profile_picture = $user['profile_picture']; 
        }   
        
        $paths = explode("/",$_SERVER['REQUEST_URI']);
        if (strcmp($paths[1],"u") == 0) {
            LaravelLocalization::setLocale(getLanguageWithCountry($user['country']['country']));
        }

        return view('users.detail', [
            'user' => $user,
            'videos' => $videos,
            'profile_picture' => $profile_picture,
        ]);
    }
}
