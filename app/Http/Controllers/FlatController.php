<?php namespace App\Http\Controllers;

class FlatController extends Controller {

    public function __construct()
    {
        $this->middleware('guest');
    }

    public function __call($func, $params)
    {
        return view('flats.'.$func);
    }
}
