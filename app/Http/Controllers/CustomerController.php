<?php namespace App\Http\Controllers;

use Config;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use LaravelLocalization;
use Mandrill;
use SendGrid;
use App;
use Form;

class CustomerController extends BaseApiController {

    public function __call($func, $params)
    {        
        $this->logout();
        return redirect()->away(LaravelLocalization::getLocalizedURL(null,'customer/'));
    }

    public function checkLogin() {
        if ( $this->authenticate( session('username'), session('password') ) ) {
            if (session('is_customer')) {
                return view( 'customer.customer', [
                    'logged' => true,
                    'msg' => trans('customer.WELCOME')." ".session('username'),
                ]);
            } elseif (session('is_admin')) {
                return view( 'customer.customer', [
                    'logged' => true,
                    'msg' => trans('customer.WELCOME')." ".session('username'),
                    'admin' => true,
                ]);
            }
        } 
        return view( 'customer.customer', [
            'logged' => false,
            'msg' => trans('customer.PLEASE_LOGIN'),
        ]);
    }
    
    public function logout() {
        parent::logout();
        return redirect()->away(LaravelLocalization::getLocalizedURL(null,'customer/'));
     }
    
    public function login(Request $request) {
        if ($request->input('_token') == session('_token')){
            if ( $this->authenticate( $request->input('username'), $request->input('password'), true) ) {
                if (session('is_customer') || session('is_admin')) {
                    session([
                        'username' => $request->input('username'), 
                        'password' => $request->input('password'),
                    ]);
                    return redirect()->away(LaravelLocalization::getLocalizedURL(null,'customer/'));
                }
            } 
        }
        parent::logout();
        return view( 'customer.customer', [
            'logged' => false,
            'failed' => true,
            'msg' => trans('customer.LOGIN_FAILED'),
        ]);
    }
    
    public function newMission()
    {
        if (session('is_customer')) {
            $user = session('user'); 
            return view( 'customer.newmission', [
                'logged' => true,
                'username' => session('username'),
                'userid' => $user['id'],
                'country' => session('country'),
             ]);        
        } else {
            $this->logout();
            return redirect()->away(LaravelLocalization::getLocalizedURL(null,'customer/'));
        }
    }

    public function customerMissions()
    {
        if (session('is_customer') || session('is_admin')) {
            $user = session('user'); 
            if (session('is_admin')) {
                $rsp = $this->doGET('missions',[],['type' => 'new']);
                $number_of_new_missions = $rsp['count'];
                $rsp = $this->doGET('missions',[],['type' => 'active']); 
                $number_of_active_missions = $rsp['count'];
                $rsp = $this->doGET('missions',[],['type' => 'old']); 
                $number_of_old_missions = $rsp['count'];                
            } else {
                $rsp = $this->doGET('missions',[],['type' => 'new', 'user' => $user['id']]);
                $number_of_new_missions = $rsp['count'];
                $rsp = $this->doGET('missions',[],['type' => 'active', 'user' => $user['id']]); 
                $number_of_active_missions = $rsp['count'];
                $rsp = $this->doGET('missions',[],['type' => 'old', 'user' => $user['id']]); 
                $number_of_old_missions = $rsp['count'];          
            }
            if ($number_of_old_missions > 100){
                $number_of_old_missions = 100;
            }
            return view( 'customer.handle', [
                'logged' => true,
                'username' => session('username'),
                'userid' => $user['id'],
                'country' => session('country'),
                'number_of_new_missions' => $number_of_new_missions,
                'number_of_active_missions' => $number_of_active_missions,
                'number_of_old_missions' => $number_of_old_missions,
             ]);        
        } else {
            $this->logout();
            return redirect()->away(LaravelLocalization::getLocalizedURL(null,'customer/'));
        }
    }
    
    public function getMissionRows(Request $request, $type) {
        if ($request->input('token') != session('_token')){
            //$this->logout();
            abort(404);
        }
        $user = session('user'); 

        if (session('is_admin')) {
            $rsp = $this->doGET('missions',[],['type' => $type, 'page_size' => $request->input('size') ]); 
        } else {
            $rsp = $this->doGET('missions',[],['type' => $type, 'user' => $user['id'], 'page_size' => $request->input('size') ]); 
        }

        if ($type == "new") {
           $row = $this->newMissionRows($rsp);
        } else if ($type == "active") {
           $row = $this->activeMissionRows($rsp);
        } else if ($type == "old") {
           $row = $this->oldMissionRows($rsp);
        } else {
            $row = "Nothing here, carry on...";
        }

        return json_encode($row);
    }

    public function register(Request $request, $what) 
    {
        $msg = trans('customer.ERRORGENERAL');
        $result = false;
        
        if ($what == "code") {
            return $this->checkCode($request->input('code'));
        } else if ($what == "fields") {
            return $this->checkFieldsAndRegister($request);
        } else if ($what == "picture") {
            return $this->uploadPicture($request);
        } else if ($what == "description") {
            return $this->changeDescription($request);
        }

        return json_encode([
            'result' => $result,
            'msg' => $msg,
        ]);
    }

    public function getDescription(Request $request)
    {
        if ($request->input('token') != session('_token')){
            $this->logout();
            abort(404);
        }

        $title = $request->input('title');
        $description = trim($request->input('description')).PHP_EOL.PHP_EOL;
        switch ($request->input('winner')) {
            case 'rank':
                $description .= str_replace('%%PRIZE%%', "'".$request->input('prize')."'", trans('customer.MISSIONPRIZETXTRANK'));
                break;
            case 'rand':
                $description .= str_replace('%%PRIZE%%', "'".$request->input('prize')."'", trans('customer.MISSIONPRIZETXTRAND'));
                break;
            case 'select':
                $descriptiontmp = str_replace('%%PRIZE%%', "'".$request->input('prize')."'", trans('customer.MISSIONPRIZETXTSELECT'));
                $description .= str_replace('%%USERNAME%%', session('username'), $descriptiontmp);
                break;
        }
        $end = date("d.m.",$request->input('end')/1000);
        $description .= " ".str_replace('%%DATE%%', $end, trans('customer.MISSIONDURATIONTXT')).PHP_EOL;
        
        if (strlen($request->input('link1')) > 0) {
            $description .= PHP_EOL.$request->input('link1');
        }
        if (strlen($request->input('link2')) > 0) {
            $description .= PHP_EOL.$request->input('link2');
        }
        if (strlen($request->input('link3')) > 0) {
            $description .= PHP_EOL.$request->input('link3');
        }

        return json_encode([
            'title' => $title,
            'description' => nl2br(urlize($description)),
            'description_raw' => $description,

        ]);
    }

    public function createCustomerMission(Request $request)
    {
        $user = session('user');

        $start_at = date("Y-m-d\TH:i:s",$request->input('mission_start_date')/1000);
        $end_at = date("Y-m-d\TH:i:s",$request->input('mission_end_date')/1000);
        
        $json = [   'mission_type' => 1,         
                    'icon_name' => $user['username'],         
                    'name' => html_entity_decode($request->input('mission_title')), 
                    'description' => html_entity_decode($request->input('mission_description')), 
                    'country' => $user['country']['country'],
                    'start_at' => $start_at, 
                    'end_at' => $end_at, 
                    'added_by' => $user['id'],
                    'linked_video' => $request->input('mission_linked_file_id'),
                ];

        $imagedata = $request->input('mission_image_filedata');
        if (strlen($imagedata) > 0) {
            $filepath = $this->base64_to_png($imagedata, sys_get_temp_dir()."/".$user['id']."_".time().".png");
            $file = fopen($filepath, "r");
            $json['mission_image_1'] = $file;
        }        

        $rsp = $this->doPOSTasADMIN('missions', [], [
                'body' => $json,
            ],true);
        
        $mailmsg = trans('customer.NEWMISSIONMAILMSG');
        $mailmsg = str_replace('%%MISSIONLINK%%',$rsp['mission_url'], $mailmsg);
        if (strlen($rsp['linked_video']['web_url']) > 0)  {            
            $mailmsg = str_replace('%%VIDEODL%%',$rsp['linked_video']['web_url'].'/download/?id='.$rsp['linked_video']['external_id'], $mailmsg);
        }

        $mailrsp = $this->sendSingleMail([
            'from' => 'support@whambush.com',
            'to' => $user['email'],
            'bcc' => Config::get('api.info_email'),
            'subject' => trans('customer.NEWMISSIONMAILSUBJECT'),
            'message' =>  $mailmsg,
            'template' => 'dd3c3eb9-094a-48f0-a67b-4b6581dc4c47',
        ]);
        
        $msg = str_replace('%%EMAIL%%', $user['email'], trans('customer.SUCCESSMESSAGE'));

        return json_encode([
            'title' => trans('customer.SUCCESSTITLE'),
            'msg' => $msg,
        ]);
    }

    public function getMissionStats(Request $request) 
    {
        if ($request->input('token') != session('_token')){
            $this->logout();
            abort(404);
        }
        $msg = "";
        $userviews = 0;
        $userlikes = 0;
        $userdislikes = 0;
        $usercomments = 0;

        $rsp = $this->doGET('missionWithId',['mission_id' => $request->input('id')]); 
    
        $missionvideospage = $this->doGET('search',['what' => 'videos'], ['mission' => $request->input('id')]);

        $numofvideos = $rsp['number_of_submissions'];

        $next = parse_url($missionvideospage['next'], PHP_URL_QUERY);
        parse_str($next, $queryarray);

        $missionvideos =  $missionvideospage['results'];
        
        while (array_key_exists ('page',$queryarray)) {
            $missionvideospage = $this->doGET('search',['what' => 'videos'], ['mission' => $request->input('id'), 'page' => $queryarray['page']]);
            $missionvideos = array_merge($missionvideos, $missionvideospage['results']);
            $next = parse_url($missionvideospage['next'], PHP_URL_QUERY);
            parse_str($next, $queryarray);
        }
        foreach ($missionvideos as $singlevideo) {
            if (intval($singlevideo['rank']) > 0) {
                $userviews += $singlevideo['view_count'];
                $userlikes += $singlevideo['like_count'];
                $userdislikes += $singlevideo['dislike_count'];
                $usercomments += $singlevideo['comment_count'];
            }  else {
                $numofvideos -= 1;
            }
        }
        
        $msg .= trans('customer.STARTDATE').': '.date("d.m.Y",strtotime($rsp['start_at'])); 
        $msg .= '<br>'.trans('customer.ENDDATE').': '.date("d.m.Y",strtotime($rsp['end_at'])); 

        $msg .= '<div class="row missiondetailrow">'; 
        $msg .= '<div class="col-md-7"><b>'.''.'</b></div>'; 
        $msg .= '<div class="col-md-1"><b>'.'<span class="icon-camera" data-toggle="tooltip" title="Videos"></span>'.'</b></div>'; 
        $msg .= '<div class="col-md-1"><b>'.'<span class="icon-play" data-toggle="tooltip" title="Views"></span>'.'</b></div>'; 
        $msg .= '<div class="col-md-1"><b>'.'<span class="icon-banana" data-toggle="tooltip" title="Likes"></span>'.'</b></div>'; 
        $msg .= '<div class="col-md-1"><b>'.'<span class="icon-shit" data-toggle="tooltip" title="Dislikes"></span>'.'</b></div>'; 
        $msg .= '<div class="col-md-1"><b>'.'<span class="icon-comments" data-toggle="tooltip" title="Comments"></span>'.'</b></div>'; 
        $msg .= '</div>';
        $msg .= '<div class="row missiondetailrow">'; 
        $msg .= '<div class="col-md-7">'.trans('customer.MISSION').'</a></div>'; 
        $msg .= '<div class="col-md-1"></div>'; 
        $msg .= '<div class="col-md-1"></div>'; 
        $msg .= '<div class="col-md-1">'.$rsp['like_count'].'</div>'; 
        $msg .= '<div class="col-md-1">'.$rsp['dislike_count'].'</div>'; 
        $msg .= '<div class="col-md-1"></div>'; 
        $msg .= '</div>';
        if (count($rsp['linked_video']) > 0) {
            $msg .= '<div class="row missiondetailrow">'; 
            $msg .= '<div class="col-md-7">'.trans('customer.MISSIONVIDEO').'</a></div>'; 
            $msg .= '<div class="col-md-1"></div>'; 
            $msg .= '<div class="col-md-1">'.$rsp['linked_video']['view_count'].'</div>'; 
            $msg .= '<div class="col-md-1">'.$rsp['linked_video']['like_count'].'</div>'; 
            $msg .= '<div class="col-md-1">'.$rsp['linked_video']['dislike_count'].'</div>'; 
            $msg .= '<div class="col-md-1">'.$rsp['linked_video']['comment_count'].'</div>'; 
            $msg .= '</div>';
        }
        if ($numofvideos > 0) {
            $msg .= '<div class="row missiondetailrow">'; 
            $msg .= '<div class="col-md-7">'.trans('customer.MISSIONUSERVIDEOS').'</a></div>'; 
            $msg .= '<div class="col-md-1">'.$numofvideos.'</div>'; 
            $msg .= '<div class="col-md-1">'.$userviews.'</div>'; 
            $msg .= '<div class="col-md-1">'.$userlikes.'</div>'; 
            $msg .= '<div class="col-md-1">'.$userdislikes.'</div>'; 
            $msg .= '<div class="col-md-1">'.$usercomments.'</div>'; 
            $msg .= '</div>';
        }
        $msg .= '<br><a href="#" class="btn btn-default" onClick="return closeDialog()">'.trans('customer.CLOSE').'</a>';
        
        return json_encode([
            'title' => $rsp['name']." - ".trans('customer.MISSIONSTATS'),
            'msg' => $msg,
        ]);
    }
    
    public function getVideoRows(Request $request) {
        if ($request->input('token') != session('_token')){
            $this->logout();
            abort(404);
        }
        $msg = "";
        $mission_id = $request->input('id');
        $rsp = $this->doGET('missionWithId',['mission_id' => $mission_id]); 
        $has_winner = $rsp['mission_type'] == 10 ? true : false;
        $winner = $rsp['prize'];
        $order = $request->input('order') ? $request->input('order') : 'random';
        $missionvideospage = $this->doGET('search',['what' => 'videos'], ['mission' => $mission_id, 'order' => 'rank', 'page_size' => '200']);

        $next = parse_url($missionvideospage['next'], PHP_URL_QUERY);
        parse_str($next, $queryarray);

        $missionvideos =  $missionvideospage['results'];
        
        while (array_key_exists ('page',$queryarray)) {
            $missionvideospage = $this->doGET('search',['what' => 'videos'], ['mission' => $mission_id, 'order' => 'rank', 'page' => $queryarray['page']]);
            $missionvideos = array_merge($missionvideos, $missionvideospage['results']);
            $next = parse_url($missionvideospage['next'], PHP_URL_QUERY);
            parse_str($next, $queryarray);
        }
        if ($order == 'rank') {
            $rank = 1;
            $msg .= trans('customer.ORDER').': '.'<b>'.trans('customer.RANK').'</b>'.'<br>'; 
        } else {
            shuffle($missionvideos);
            $rank = "-";
            $msg .= trans('customer.ORDER').': '.'<b>'.trans('customer.RANDOM').'</b>'.'<br>'; 
        }
        $msg .= '<div class="row missiondetailrow">'; 
        $msg .= '<div class="col-md-1"><b>'.'<span class="icon-trophy" data-toggle="tooltip" title="Rank"></span>'.'</b></div>'; 
        $msg .= '<div class="col-md-1"><b>'.'<span class="icon-star" data-toggle="tooltip" title="Rank"></span>'.'</b></div>'; 
        if (session('is_admin')) {
            $msg .= '<div class="col-md-2"><b>'.trans('customer.USERNAME').'</b></div>'; 
            $msg .= '<div class="col-md-2"><b>'.trans('customer.EMAIL').'</b></div>'; 
            $msg .= '<div class="col-md-1"><b>'.trans('customer.VIDEOLINK').'</b></div>'; 
        } else {
            $msg .= '<div class="col-md-3"><b>'.trans('customer.USERNAME').'</b></div>'; 
            $msg .= '<div class="col-md-2"><center><b>'.trans('customer.VIDEOLINK').'</b></center></div>';      
        }
        $msg .= '<div class="col-md-1"><b>'.'<span class="icon-play" data-toggle="tooltip" title="Views"></span>'.'</b></div>'; 
        $msg .= '<div class="col-md-1"><b>'.'<span class="icon-banana" data-toggle="tooltip" title="Likes"></span>'.'</b></div>'; 
        $msg .= '<div class="col-md-1"><b>'.'<span class="icon-shit" data-toggle="tooltip" title="Dislikes"></span>'.'</b></div>'; 
        $msg .= '<div class="col-md-1"><b>'.'<span class="icon-comments" data-toggle="tooltip" title="Comments"></span>'.'</b></div>'; 
        $msg .= '<div class="col-md-1"><b>'.'<span class="icon-globe" data-toggle="tooltip" title="Country"></span>'.'</b></div>'; 
        $msg .= '</div>';
        foreach ($missionvideos as $singlevideo) {
            if (intval($singlevideo['rank']) > 0) {
                $msg .= '<div class="row missiondetailrow">'; 
                if ($has_winner || session('is_admin')) {
                    $msg .= '<div class="col-md-1"></div>'; 
                } else {
                    $msg .= '<div class="col-md-1"><input type="checkbox" class="selectedwinner" id="'.$singlevideo['added_by']['id'].'" video="'.$singlevideo['id'].'" mission="'.$mission_id.'"></div>'; 
                }
                $msg .= '<div class="col-md-1">'.$rank.'</div>'; 

                if (session('is_admin')) {
                    $msg .= '<div class="col-md-2" style="word-break:break-all;overflow:hidden;">'.$singlevideo['added_by']['username'].'</div>'; 
                    if ($singlevideo['added_by']['activation_state'] == 'ACTIVATED') {
                        $msg .= '<div class="col-md-2" style="word-break:break-all;overflow:hidden;">'.$singlevideo['added_by']['email'].'</div>'; 
                    } else {
                        $msg .= '<div class="col-md-2" style="word-break:break-all;overflow:hidden;"><strike>'.$singlevideo['added_by']['email'].'</strike></div>'; 
                    }
                    $msg .= '<div class="col-md-1"><center><a href="'.$singlevideo['web_url'].'" target="_blank"><span class="icon-globe"></span></a>|'; 
                } else {
                    $msg .= '<div class="col-md-3" style="word-break:break-all;overflow:hidden;">'.$singlevideo['added_by']['username'].'</div>'; 
                    $msg .= '<div class="col-md-2"><center><a href="'.$singlevideo['web_url'].'" target="_blank"><span class="icon-globe"></span></a>|'; 
                }
                $msg .= '<a href="'.$singlevideo['web_url'].'/download/?id='.$singlevideo['external_id'].'" target="_blank"><img src="/img/dl_ico.png" width="15"></a></center></div>'; 
                $msg .= '<div class="col-md-1">'.$singlevideo['view_count'].'</div>'; 
                $msg .= '<div class="col-md-1">'.$singlevideo['like_count'].'</div>'; 
                $msg .= '<div class="col-md-1">'.$singlevideo['dislike_count'].'</div>'; 
                $msg .= '<div class="col-md-1">'.$singlevideo['comment_count'].'</div>'; 
                $country = strlen($singlevideo['added_by']['country']['flag_url']) > 0 ? '<img src="'.$singlevideo['added_by']['country']['flag_url'].'" width="20" data-toggle="tooltip" title="'.$singlevideo['added_by']['country']['name'].'">' : $singlevideo['added_by']['country']['country'];
                $msg .= '<div class="col-md-1">'.$country.'</div>'; 
                $msg .= '</div>';
                $rank = $rank > 0 ? $rank + 1 : $rank;
            }
        }
        if (count($missionvideos) > 0 && !$has_winner && !session('is_admin')) {
            $msg .= '<br><a href="#" class="btn btn-default" onClick="return confirmWinner()">'.trans('customer.SELECTWINNER').'</a>';
        }
        if ($has_winner && strlen($winner) > 0) {
            $msg .= "<p>".trans("customer.MISSIONHASWINNER")." ".$winner."</p>";
        }       
        $msg .= '<a href="#" class="btn btn-default" onClick="return closeDialog()">'.trans('customer.CLOSE').'</a><br><br>';

        return json_encode([
            'title' => trans('customer.MISSIONUSERVIDEOS'),//.' - '.$rsp['name'].' - '.$rsp['mission_type'],
            'msg' => $msg,
        ]);
    }

    public function missionWinner(Request $request) {
        $all = explode(':',$request->input('id'));
        array_pop($all);
        foreach ($all as $one) {
            list($ids[],$vids[],$missions[]) = explode(',',$one);
        }
        
        for ($i=0; $i < count($ids); $i++) { 
            $mission = $this->doGET('missionWithId',['mission_id' => $missions[$i]]);
            $user = $this->doGET('user',['id' => $ids[$i]]);
            $winner = $mission['prize'];
            if (strlen($winner) > 0) {
                $winner .= ", ".$user['username'];
            } else {
                $winner .= $user['username'];
            }
            //mission text + mission type    
            $winnerText = $user['username'];
            if ($i === 0) {
                $new_txt = $mission['description'].PHP_EOL.PHP_EOL.trans('customer.MISSIONWINNERDESCRIPTION').PHP_EOL.$winnerText;
            } else {
                $new_txt = $mission['description'].PHP_EOL.$winnerText;
            }
            $rsp = $this->doPUTasADMIN('missionWithId',['mission_id' => $missions[$i]], ['body' => [ 'description' => $new_txt, 'mission_type' => '10','prize' => $winner]], true);

            //comment
            $rspc = $this->doPOST('comment',['what' => 'video', 'id' => $vids[$i]], ['body' => ['comment' => trans('customer.MISSIONWINNERCOMMENT')]],true);

            //mail
            $current_user = session('user');
            $mailmsg = trans('admin.WINNER_MAIL_TMPL_3');
            $mailmsg = str_replace('%%USERNAME%%',$user['username'], $mailmsg);
            $mailmsg = str_replace('%%MISSION%%',$mission['name'], $mailmsg);
            $mailmsg = str_replace('%%CREATOR%%',$current_user['username'], $mailmsg);
            $mailmsg = str_replace('%%LINK%%',$mission['mission_url'], $mailmsg);
            $mailrsp ="";
            $mailrsp = $this->sendSingleMail([
                'from' => $current_user['email'],
                'from_name' => $current_user['username'].' - '.$current_user['email'],
                'to' => $user['email'],
                'bcc' => 'reward@whambush.com',
                'subject' => trans('customer.MISSIONWINNERMAILTITLE'),
                'message' =>  $mailmsg,
                'template' => 'dd3c3eb9-094a-48f0-a67b-4b6581dc4c47',
            ]);
          }
        
        return json_encode([
            'title' => trans('customer.MISSIONWINNERTITLE'),
            'msg' => trans('customer.MISSIONWINNERDONE')//.json_encode($rsp).json_encode($rspc).json_encode($mailrsp),
        ]);
    }

    public function getEditMission(Request $request) {
        if ($request->input('token') != session('_token')){
            $this->logout();
            abort(404);
        }
        
        $rsp = $this->doGET('missionWithId',['mission_id' => $request->input('id')]); 

        $msg  = '<div class="row">';
        $msg .= '<div class="col-md-4">'.Form::label('title',trans('customer.MISSIONTITLE')).'</div>'; 
        $msg .= '<div class="col-md-8">'.Form::text('title',$rsp['name']).'</div>'; 
        $msg .= '</div>';
        $msg .= '<div class="row" style="height:200px">';
        $msg .= '<div class="col-md-12">'.Form::label('description',trans('customer.MISSIONDESCRIPTION')).'<br>'.Form::textarea('description',$rsp['description'],array('style' => 'height: 180px')).'</div>'; 
        $msg .= '</div>';
        $msg .= '<div class="row">';
        $msg .= '<div class="col-md-12"><a href="#" class="btn btn-default" onClick="return closeDialog()">'.trans('customer.CANCEL').'</a><a href="#" class="btn btn-default" onClick="return saveMission('.$rsp['id'].')">'.trans('customer.SAVE').'</a>    <input type="hidden" name="_token" value="{{ csrf_token() }}">
</div>'; 
        $msg .= '</div>';

        return json_encode([
            'title' => trans('customer.EDITMISSION'),
            'msg' => $msg,
        ]);
    }
    public function updateMission(Request $request) {
        if ($request->input('_token') != session('_token')){
         //   $this->logout();
            abort(404);
        }
        $id = $request->input('id');
        $title = html_entity_decode($request->input('title'));
        $description = html_entity_decode($request->input('description'));

        $rsp = $this->doPUTasADMIN('missionWithId',['mission_id' => $id], ['body' => ['name' => $title, 'description' => $description]], true);
        
        return json_encode([
            'title' => trans('customer.FINISH'),
            'msg' => trans('customer.MISSIONEDITDONE')//.json_encode($rsp),
        ]);

    }
   //// privates

    private function newMissionRows($rsp) {
        $row = "";
        $row .= '<div class="row missiondetailrow">'; 
        $row .= '<div class="col-md-2"><span class="icon-latest"></span></div>'; 
        $row .= '<div class="col-md-3"><b>'.trans('customer.MISSIONTITLE').'</b></div>'; 
        $row .= '<div class="col-md-6"><b>'.trans('customer.MISSIONSHARELINK').'</b></div>';
        $row .= '<div class="col-md-1"></div>'; 
        $row .= '</div>'; 

        foreach ($rsp['results'] as $value) {
            $date = strtotime($value['start_at']);
            $row .= '<div class="row missiondetailrow">'; 
            $row .= '<div class="col-md-2">'.date("d.m.Y",$date).'</div>'; 
            $row .= '<div class="col-md-3">'.$value['name'].'</div>'; 
            $row .= '<div class="col-md-6"><a href="'.$value['mission_url'].'" target="_blank">'.$value['mission_url'].'</a>'.'</div>';
            $row .= '<div class="col-md-1"><a href="#" class="missionedit" value="'.$value['id'].'">'.trans('customer.EDITMISSION').'</a></div>'; 
            $row .= '</div>'; 
        }
        return $row;
    }
    
    private function activeMissionRows($rsp) {
        $row = "";
        $row .= '<div class="row missiondetailrow">'; 
        $row .= '<div class="col-md-2"><span class="icon-latest"></span></div>'; 
        $row .= '<div class="col-md-3"><b>'.trans('customer.MISSIONTITLE').'</b></div>'; 
        $row .= '<div class="col-md-5"><b>'.trans('customer.MISSIONSHARELINK').'</b></div>';
        $row .= '<div class="col-md-1"><span class="icon-camera"></span></div>';
        $row .= '<div class="col-md-1"></div>'; 
        $row .= '</div>'; 

        foreach ($rsp['results'] as $value) {
            $date = strtotime($value['end_at']);
            $row .= '<div class="row missiondetailrow">'; 
            $row .= '<div class="col-md-2">'.date("d.m.Y",$date).'</div>'; 
            $row .= '<div class="col-md-3">'.$value['name'].'</div>'; 
            $row .= '<div class="col-md-5"><a href="'.$value['mission_url'].'" target="_blank">'.$value['mission_url'].'</a>'.'</div>';
            $row .= '<div class="col-md-1">'.$value['number_of_submissions'].'</div>'; 
            $row .= '<div class="col-md-1"><a href="#" class="missionstats" value="'.$value['id'].'">'.trans('customer.MISSIONSTATS').'</a></div>'; 
            $row .= '</div>'; 
        }
        return $row;
    }
    
    private function oldMissionRows($rsp) {
        $row = "";
        $row .= '<div class="row missiondetailrow">'; 
        $row .= '<div class="col-md-2 missiondetailcol"><span class="icon-latest"></span></div>'; 
        $row .= '<div class="col-md-3 missiondetailcol"><b>'.trans('customer.MISSIONTITLE').'</b></div>'; 
        $row .= '<div class="col-md-4 missiondetailcol"><b>'.trans('customer.MISSIONSHARELINK').'</b></div>';
        if (session('is_admin')) {
            $row .= '<div class="col-md-1 missiondetailcol"><b>'.trans('customer.VIDEOLINK').'</b></span></div>'; 
        } else {
            $row .= '<div class="col-md-1 missiondetailcol"><b>'.trans('customer.MISSIONWINNER').'</b></span></div>'; 
        }
        $row .= '<div class="col-md-1 missiondetailcol"><span class="icon-camera"></span></div>'; 
        $row .= '<div class="col-md-1 missiondetailcol"></div>'; 
        $row .= '</div>'; 

        foreach ($rsp['results'] as $value) {
            $date = strtotime($value['end_at']);
            $style = "";
            if ($value['mission_type'] == 10) {
                $style = 'style="color:#CD554A;"';
            }
            $row .= '<div class="row missiondetailrow">'; 
            $row .= '<div class="col-md-2 missiondetailcol">'.date("d.m.Y",$date).'</div>'; 
            $row .= '<div class="col-md-3 missiondetailcol" style="word-break:break-all;overflow:hidden;">'.$value['name'].'</div>'; 
            $row .= '<div class="col-md-4 missiondetailcol" style="word-break:break-all;overflow:hidden;"><a href="'.$value['mission_url'].'" target="_blank">'.$value['mission_url'].'</a>'.'</div>';
            if (session('is_admin')) {
               $row .=  '<div class="col-md-1 missiondetailcol"><a href="#" class="missionwinner" value="'.$value['id'].'" order="rank"><span class="icon-camera"></span></a></div>';
            } else {
                $row .= '<div class="col-md-1 missiondetailcol"><a href="#" class="missionwinner" value="'.$value['id'].'" order="rank"><span class="icon-trophy" '.$style.'></span></a>/<a href="#" class="missionwinner" value="'.$value['id'].'" order="random"><span class="icon-random" '.$style.'></span></a></div>'; 
            }
            $row .= '<div class="col-md-1 missiondetailcol">'.$value['number_of_submissions'].'</div>'; 
            $row .= '<div class="col-md-1 missiondetailcol"><a href="#" class="missionstats" value="'.$value['id'].'">'.trans('customer.MISSIONSTATS').'</a></div>'; 
            $row .= '</div>'; 
        }
        return $row;
    }

    private function sendSingleMail($data)
    {
        $subject = $data['subject'];
        $message = nl2br($data['message']);
           
        $email = $data['to'];
        
        $template_name = $data['template'];
        $template_id = $data['template'];

        $from = $data['from'];
        $from_name = array_key_exists('from_name', $data) ? $data['from_name'] : 'Whambush';
        
        $sendgrid = new SendGrid(Config::get('api.sg_api_key'));
        $sgEmail = new SendGrid\Email();
        $sgEmail
            ->addTo($email)
            ->setFrom($from)
            ->setFromName($from_name)
            ->addBcc($data['bcc'])
            ->setSubject($subject)
            ->setText($message)
            ->setHtml($message)
            ->setTemplateId($template_id);

        try {
            $sendgrid->send($sgEmail);
            $response = array(
                'status' => 'Success!',
                'msg' => 'Mail sent!'
            );
            return json_encode($response);
        } catch(\SendGrid\Exception $e) {
            $msg = $e->getCode() . "\n";
            foreach($e->getErrors() as $er) {
                $msg .= $er . "\n";
            }
            $response = array(
                'status' => 'FAIL!',
                'msg' => $msg
            );
            return json_encode($response);
        }
    }

    private function checkCode($code) 
    {
        if ($code == "whambushCustomer" || App::environment() != 'prod') {
            $msg = "";
            $result = true;
        }  else {
            $msg = trans('customer.WRONGCODE');
            $result = false;
        }     
        return json_encode([
            'result' => $result,
            'msg' => $msg,
        ]);
    }

    private function checkFieldsAndRegister(Request $data) 
    {
        $result = false;
        $msg = "";
        if (strlen($data->input('username')) < 3) {
            $msg = trans('customer.ERRORUSERNAME1');
        } else if (!filter_var($data->input('email'), FILTER_VALIDATE_EMAIL)) {
            $msg = trans('customer.ERROREMAIL1');
        } else if (strlen($data->input('country')) < 1 ) {
            $msg = trans('customer.ERRORCOUNTRY');
        } else if (strlen($data->input('password')) < 4) {
            $msg = trans('customer.ERRORPASS1');
        } else if ($data->input('password') != $data->input('password2')) {
            $msg = trans('customer.ERRORPASS2');
        } else {
            $params = ['json' => [
                    'username' => $data->input('username'),
                    'password1' => $data->input('password'),
                    'password2' => $data->input('password'),
                    'email' => $data->input('email'),
                    'country' => $data->input('country'),
                    'user_type' => '3', // customer == 3
                ]
            ];
            $response = $this->doPOST('register',[],$params,true);
            if (array_key_exists('error',$response)) { 
                if (array_key_exists('username',$response['error'])) {
                    $msg = trans('customer.ERRORUSERNAME2');
                } else if (array_key_exists('email',$response['error'])) {
                    $msg = trans('customer.ERROREMAIL2');
                } else {
                    $msg = trans('customer.ERRORGENERAL');                
                }
            } else {
                if ($this->authenticate($data->input('username'), $data->input('password'), true, false)) {
                    session()->forget('is_customer');
                    $msg = $response['id'];
                } 
                $result = true;
            }
        }

        return json_encode([
            'result' => $result,
            'msg' => $msg,
        ]);
    }
    
    private function changeDescription(Request $request) 
    {
        $id = $request->input('id');
        $desc = $request->input('desc');
        
        $msg = "";
        
        $response = $this->doPUT('user',['id' => $id],[ 'json' => ['description' => $desc] ]);
        
        if (array_key_exists('error',$response)) { 
            $msg = trans('customer.ERRORDESCRIPTION');
        }
        
        return json_encode([
            'result' => true,
            'msg' => $msg,
        ]);
    }
    
    private function uploadPicture(Request $request) 
    {
        $id = $request->input('id');
        $data = $request->input('data');
        
        $msg = "";
        if (strlen($data) > 0) {
            $filepath = $this->base64_to_png($data, sys_get_temp_dir()."/".$id."_".time().".png");
            
            $file = fopen($filepath, "r");
            $response = $this->doPUT('user',['id' => $id],[ 'body' => ['profile_picture' => $file] ]);
            
            if (array_key_exists('error',$response)) { 
                $msg = trans('customer.ERRORPROFILEPIC');
            }
        }
        return json_encode([
            'result' => true,
            'msg' => $msg,
        ]);
    }



    private function base64_to_png($base64_string, $output_file) {
        $ifp = fopen($output_file, "wb"); 

        $data = explode(',', $base64_string);

        fwrite($ifp, base64_decode($data[1])); 
        fclose($ifp); 

        return $output_file; 
    }

//end of class
}
