<?php namespace App\Http\Controllers;

use Config;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Log;

class BaseApiController extends Controller {

    private $client;

    public function __construct()
    {
        $clientOptions = [
            'base_url' => Config::get('api.base_url').Config::get('api.version').'/',
            'defaults' => [
                'verify' => false, // TODO: enable checking SSL after testing
            ]
        ];

        $this->client = new Client($clientOptions);
        if (!(session()->has('api_token'))) {
            session( ['api_token' => $this->_getAuthToken()] );
        }
        $this->client->setDefaultOption('headers', [
            'Authorization' => 'Token '.session('api_token')
        ]);
    }

    private function _getAuthToken()
    {
        $response = $this->doPOST('token', [], [
            'json' => [
                'username' => Config::get('api.username'),
                'password' => Config::get('api.password'),
                'guest_id' => Config::get('api.guest_id'),
            ]
        ]);

        return $response['token'];
    }
    
    protected function authenticate($username, $password, $time = false, $require_active = true)
    {
        if (!$time) {
            if (session('login_time') > 0) {
                $seconds = time() - session('login_time'); 
                if ($seconds > 60*60) {
                    return false;
                }       
            }    
        }
        if (count($username) > 0 && count($password) > 0) {
            $this->client->setDefaultOption('headers', []); //clear headers for old token (if such exists)
            $url = $this->_getPath('token', []);
            try {
                $response = $this->client->post($url,[
                    'json' => [
                        'username' => $username,
                        'password' => $password,
                        'device'=>'customer_web',
                    ]
                ]);
            } catch (RequestException $e) {
                return false;
            }
            
            if (isset($response->json()['user'])) {
                if (($response->json()['user']['is_admin'] || $response->json()['user']['user_type'] == 3) && ($response->json()['user']['activation_state'] == "ACTIVATED" || !$require_active)) {   
                    session( [
                        'is_admin' => $response->json()['user']['is_admin'],
                        'is_customer' => $response->json()['user']['user_type'] == 3 ? true : false ,
                        'user' => $response->json()['user'],
                        'country' => $response->json()['user']['country']['country'],
                        'api_token' => $response->json()['token'],
                        'vzaar_secret' => $response->json()['settings']['vzaar_secret'],
                        'vzaar_key' => $response->json()['settings']['vzaar_key'],
                        'login_time' => time(),
                    ]);
                    $this->client->setDefaultOption('headers', [
                        'Authorization' => 'Token '.session('api_token')
                    ]);
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    
    protected function logout() {
        session()->forget('user');
        session()->forget('country');
        session()->forget('usertype');
        session()->forget('username');
        session()->forget('password');
        session()->forget('login_time');
        session()->forget('api_token');
        session()->forget('is_admin');
        session()->forget('is_customer');
        session()->forget('vzaar_key');
        session()->forget('vzaar_secret');
    }

    protected function _getPath($path, $params = [])
    {
        $url = Config::get('api.path.'.$path);
        foreach ($params as $key => $value) {
            $url = str_replace('{'.$key.'}', $value, $url);
        }

        return $url;
    }

    protected function doGET($path, $pathParams = [], $params = []) {
        $url = $this->_getPath($path, $pathParams);
        try {
            $response = $this->client->get($url, ['query' => $params]);
        } catch (RequestException $e) {
            $errormsg = "";
            if ($e->hasResponse() && env('DEBUG',false)) {
               $errormsg = json_encode($e->getResponse());
               Log::error($errormsg);
            }
            if (!(session()->has('is_admin'))) {
                session()->forget('api_token'); // clear token, just in case (not for admin)
            }
            if($e->hasResponse()) {
                abort($e->getResponse()->getStatusCode());
            } else {
                abort(500, $errormsg);
            }
        }

        return $response->json();
    }
    
    protected function doPOSTasADMIN($path, $pathParams = [], $params = [], $return_error_response = false) {
        
        $response = $this->doPOST('token', [], [
            'json' => [
                'username' => Config::get('api.admin_username'),
                'password' => Config::get('api.admin_password'),
                'guest_id' => Config::get('api.guest_id'),
            ]
        ]);

        $this->client->setDefaultOption('headers', [
            'Authorization' => 'Token '.$response['token']
        ]);

        $response = $this->doPOST($path, $pathParams, $params, $return_error_response);
        
        $this->authenticate(session('username'),session('password'),true);

        return $response;//->json();
    }
    
    protected function doPOST($path, $pathParams = [], $params = [], $return_error_response = false) {
        $url = $this->_getPath($path, $pathParams);
        try {
            $response = $this->client->post($url, $params);
        } catch (RequestException $e) {
            if ($return_error_response) {
               if ($e->hasResponse()) {
                    return ['error' => $e->getResponse()->json()];
                } 
            } else {
                $errormsg = "";
                if ($e->hasResponse() && env('DEBUG',false)) {
                    $errormsg = json_encode($e->getResponse());
                    Log::error($errormsg);
                }
                if (!(session()->has('is_admin'))) {
                    session()->forget('api_token'); // clear token, just in case (not for admin)
                }
                if($e->hasResponse()) {
                    abort($e->getResponse()->getStatusCode());
                } else {
                    abort(500, $errormsg);
                }
            }
        }

        return $response->json();
    }

    protected function doPUTasADMIN($path, $pathParams = [], $params = [], $return_error_response = false) {
        $response = $this->doPOST('token', [], [
            'json' => [
                'username' => Config::get('api.admin_username'),
                'password' => Config::get('api.admin_password'),
                'guest_id' => Config::get('api.guest_id'),
            ]
        ]);

        $this->client->setDefaultOption('headers', [
            'Authorization' => 'Token '.$response['token']
        ]);

        $response = $this->doPUT($path, $pathParams, $params, $return_error_response);
        
        $this->authenticate(session('username'),session('password'),true);

        return $response;
    }

    protected function doPUT($path, $pathParams = [], $params = [], $return_error_response = false) {
        $url = $this->_getPath($path, $pathParams);
        try {
            $response = $this->client->put($url, $params);
        } catch (RequestException $e) {
            if ($return_error_response) {
               if ($e->hasResponse()) {
                    return ['error' => $e->getResponse()->json()];
                } 
            } else {
                $errormsg = "";
                if ($e->hasResponse() && env('DEBUG',false)) {
                    $errormsg = json_encode($e->getResponse());
                    Log::error($errormsg);
                }
                if (!(session()->has('is_admin'))) {
                    session()->forget('api_token'); // clear token, just in case (not for admin)
                }
                if($e->hasResponse()) {
                    abort($e->getResponse()->getStatusCode());
                } else {
                    abort(500, $errormsg);
                }
            }
        }

        return $response->json();
    }

}
