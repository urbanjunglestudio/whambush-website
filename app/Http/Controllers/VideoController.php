<?php namespace App\Http\Controllers;

use LaravelLocalization;
use Request;

class VideoController extends BaseApiController {

    public function detail($slug)
    {
        $video = $this->doGET('video', ['slug' => $slug]);
     
        //if user is deactivated throw 404
        if ($video['added_by']['activation_state'] == 'DEACTIVATED') {
            abort(404);
        }

        if (count($video['linked_to_mission']) > 0 && $video['video_type'] !== 2) {
            return redirect()->away($video['linked_to_mission'][0]['mission_url']);
        }
        // turn URLs into HTML hyperlinks
        $video['description_html'] = nl2br(urlize($video['description']));

        // get 3 recent comments
        if ($video['comment_count'] > 0) {
            $response = $this->doGET('comments', ['video_id' => $video['id']]);
            $comments = $response['results'];
        } else {
            $comments = array();
        }

        // calculate banana and shit count
        $banana = $video['like_count']; 
        $shit = $video['dislike_count'];
        $shit_css = "shit-count";     
        $banana_css = "banana-count";
    
        //users profile picture
        if ($video['added_by']['profile_picture'] == "") {
            $added_by_profile_picture =  asset('/img/minion.png');
        } else {
            $added_by_profile_picture = $video['added_by']['profile_picture']; 
        }  

        $paths = explode("/",$_SERVER['REQUEST_URI']);
        if (strcmp($paths[1],"v") == 0) {
            LaravelLocalization::setLocale(getLanguageWithCountry($video['country']));
        }


        return view('videos.detail', [
            'video' => $video,
            'comments' => $comments,
            'banana' => $banana,
            'banana_css' => $banana_css,
            'shit' => $shit,
            'shit_css' => $shit_css,
            'added_by_profile_picture' => $added_by_profile_picture,
            'comments' => $comments,
            'slug' => $slug,
        ]);
    }

    public function download_video($slug) 
    {

        $video = $this->doGET('video', ['slug' => $slug]);
        
        if (Request::input('id') !== $video['external_id']) {
            abort(403);
        }
        $file = "http://view.vzaar.com/".$video['external_id']."/video";
        
        $filename = $slug.".mp4";
        
        header('Content-Type: mime/video');
        header('Content-Disposition: attachment; filename='.$filename);

        readfile($file);
    }
    
    public function video_thumb($slug) 
    {

        $video = $this->doGET('video', ['slug' => $slug]);
        if ($video['is_processed'] == 0) {
            $file = asset('/img/video_not_ready.jpg');
        } else if (count($video['linked_to_mission']) > 0 && strlen($video['linked_to_mission'][0]['mission_image_1']) > 0) {
            $file = $video['linked_to_mission'][0]['mission_image_1'];
        } else {
            $file = $video['thumbnail_url'];
        }
        header("Content-type: " . image_type_to_mime_type(exif_imagetype($file)));
        
        readfile($file);
  
    }

}
