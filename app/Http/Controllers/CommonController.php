<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use LaravelLocalization;

class CommonController extends BaseApiController {

    public function getCountries(Request $request)
    {
        if ($request->input('token') != session('_token')){
            abort(404);
        }
        $rsp = $this->doGET('countries', [], ['page_size' => 200]);
        return json_encode($rsp['results']);
    }
	
	public function getTranslation(Request $request, $lang, $key)
    {
        if ($request->input('token') != session('_token') || strlen($key) < 1){
            abort(404);
        }

        LaravelLocalization::setLocale($lang);
        
        return trans($key);
    }
    
    public function isUrl(Request $request)
    {
        /*if (!filter_var($request->input('url'), FILTER_VALIDATE_URL) === false) {
            return json_encode(true);
        } else {
            return json_encode(false);
        }*/
        $website = $request->input('url');
        if (!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",$website)) {
            return json_encode(false);
        } else {
            return json_encode(true);
        }
    }

//end of class
}
