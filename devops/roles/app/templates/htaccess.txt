{%- if env == 'stag' %}
<IfModule mod_rewrite.c>
    <IfModule mod_negotiation.c>
        Options -MultiViews
    </IfModule>

    RewriteEngine On

    # Redirect requests to API
    RewriteRule ^api/(.*)$ http://api.whambush.net/$1 [P,L]
    RewriteRule ^account/(.*)$ http://api.whambush.net/account/$1 [L,R=301]
    RewriteRule ^media/(.*)$ http://api.whambush.net/media/$1 [L,R=301]

    # Redirect Trailing Slashes...
    RewriteRule ^(.*)/$ /$1 [L,R=301]

    # Handle Front Controller...
    RewriteCond %{REQUEST_FILENAME} !-d
    RewriteCond %{REQUEST_FILENAME} !-f
    RewriteRule ^ index.php [L]
</IfModule>
{% elif env == 'prod' %}
<IfModule mod_rewrite.c>
    <IfModule mod_negotiation.c>
        Options -MultiViews
    </IfModule>

    RewriteEngine On

    # Redirect requests to API
    RewriteRule ^api/(.*)$ https://api.whambush.com/$1 [P,L]
    RewriteRule ^account/(.*)$ https://api.whambush.com/account/$1 [L,R=301]
    RewriteRule ^media/(.*)$ https://api.whambush.com/media/$1 [L,R=301]

    # Redirect Trailing Slashes...
    RewriteRule ^(.*)/$ /$1 [L,R=301]

    # Handle Front Controller...
    RewriteCond %{REQUEST_FILENAME} !-d
    RewriteCond %{REQUEST_FILENAME} !-f
    RewriteRule ^ index.php [L]
</IfModule>
{% endif -%}
