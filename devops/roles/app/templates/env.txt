APP_ENV={{ env }}
SECRET_KEY={{ secret_key }}
DEBUG=false

API_USERNAME=wbapi

{% if env == 'stag' %}

HEAD_SCRIPT="<script>(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');ga('create', 'UA-49596640-2', 'whambush.net');ga('require', 'displayfeatures');ga('send', 'pageview');</script>"
GOOGLE_ANALYTICS_ID=UA-49596640-2
API_URL=http://api.whambush.net/
API_PASSWORD={{ api_password }}

{% elif env == 'prod' %}

HEAD_SCRIPT="<script>(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');ga('create', 'UA-49596640-1', 'whambush.com');ga('require', 'displayfeatures');ga('send', 'pageview');</script>"
GOOGLE_ANALYTICS_ID=UA-49596640-1
API_URL=https://api.whambush.com/
API_PASSWORD={{ api_password }}

{% endif %}
